package my.utm.acad.ac.core;

import junit.framework.TestCase;
import my.utm.acad.ac.core.dao.*;
import my.utm.acad.ac.core.model.*;
import my.utm.acad.ac.core.model.impl.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static my.utm.acad.ac.core.model.AcAcademicClassification.LEVEL_100;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class DomainTest extends TestCase {

    private AcFaculty faculty;
    private AcProgram program;
    private AcCourse course;
    private AcAcademicYear year;
    private AcAcademicSemester semester;
    private AcAcademicSession session;

    public void setup() {
        year = new AcAcademicYearImpl();
        year.setCode("2014/2015");
        year.setDescription("2014/2015");

        semester = new AcAcademicSemesterImpl();
        semester.setCode("1");
        semester.setDescription("Semester 1");

        session = new AcAcademicSessionImpl();
        session.setCode("2014/2015/1");
        session.setDescription("2014/2015/1");
        session.setYear(year);
        session.setSemester(semester);

        faculty = new AcFacultyImpl();
        faculty.setCode("FKM");
        faculty.setPrefixCode("FKM");
        faculty.setDescription("Fakulti Kejuruteraan Mekanikal");

        program = new AcProgramImpl();
        program.setCode("MECH");
        program.setPrefixCode("M");
        program.setDescription("Mechanical Engineering");
        program.setFaculty(faculty);

        course = new AcCourseImpl();
        course.setCode("EMM212");
        course.setTitle("Thermo and Design");
        course.setClassification(LEVEL_100);
        course.setCreditHour(4);
        course.setFaculty(faculty);
    }

    @Test
    public void createOffering() {
        AcOffering offering = new AcOfferingImpl();
        offering.setCode(course.getCode());
        offering.setTitle(course.getTitle());
        offering.setProgram(program);
        offering.setCourse(course);

        AcSection section = new AcSectionImpl();
        section.setCode(session.getCode() + "." + offering.getCode());
        section.setSession(session);
        section.setOffering(offering);
    }

    @Autowired
    private AcSponsorDao sponsorDao;

    @Autowired
    private AcAcademicSessionDao academicSessionDao;

    @Autowired
    private AcEnrollmentDao enrollmentDao;

    @Autowired
    private AcPaymentBatchDao paymentBatchDao;

    @Autowired
    private AcCpaDao cpaDao;

    public void firstBatchProcess() {
        AcAcademicSession current = academicSessionDao.findCurrent(); // 201420151
        List<AcSponsor> sponsors = sponsorDao.find(0, 100);

        for (AcSponsor sponsor : sponsors) {
            AcPaymentBatch batch = new AcPaymentBatchImpl();
            batch.setSession(current); //201420141
            batch.setSponsor(sponsor);

            AcPaymentBatchItem item = new AcPaymentBatchItemImpl();
            List<AcEnrollment> enrollments = enrollmentDao.find(current, sponsor);
            for (AcEnrollment enrollment : enrollments) {

                // dia enrolled + dia tak habis
                if (enrollment.getEnrollmentStatus().equals(AcEnrollmentStatus.ENROLLED)
                        || enrollment.getEnrollmentStatus().equals(AcEnrollmentStatus.INCOMPLETE))

                    item.setEligible(true);
                item.setEnrollment(enrollment);
            }
        }
    }

    public void secondBatchProcess() {
        AcAcademicSession current = academicSessionDao.findCurrent();
        List<AcPaymentBatch> batches = paymentBatchDao.find(current);
        for (AcPaymentBatch batch : batches) {
            AcSponsor sponsor = batch.getSponsor();
            List<AcPaymentBatchItem> items = paymentBatchDao.findItems(batch);
            for (AcPaymentBatchItem item : items) {
                AcEnrollment enrollment = item.getEnrollment();
                AcCpa cpa = cpaDao.find(current, enrollment.getStudent());
                if (sponsor.getCpaRequirement().compareTo(cpa.getValue()) > 0) {
                    item.setEligible(true);
                }
            }
        }
    }
}