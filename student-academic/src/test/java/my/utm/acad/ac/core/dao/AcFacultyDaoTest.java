package my.utm.acad.ac.core.dao.impl;

import my.utm.acad.ac.core.dao.AcFacultyDao;
import my.utm.acad.ac.core.dao.AcUserDao;
import my.utm.acad.ac.core.model.*;
import my.utm.acad.ac.core.model.impl.AcFacultyImpl;
import my.utm.acad.ac.core.model.impl.AcUserImpl;
import my.utm.acad.configuration.AbstractDaoContextBaseTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class AcFacultyDaoTest extends AbstractDaoContextBaseTest {

    private static final Logger LOG = LoggerFactory.getLogger(AcFacultyDaoTest.class);

    @Autowired
    private AcFacultyDao facultyDao;

    @Autowired
    private AcUserDao userDao;
    private AcUser root;

    @Before
    public void setUp() {
        root = new AcUserImpl();
        root.setName("root");
        root.setRealName("ROOT");
        root.setPassword("abc123");
        root.setPrincipalType(AcPrincipalType.USER);
        userDao.save(root);
    }

    @After
    public void tearDown() {
    }

    @Test
    @Rollback(true)
    public void list() {
        AcFaculty faculty = new AcFacultyImpl();
        faculty.setCode("FKM");
        faculty.setDescription("Fakulti Kejuruteraan Mekanikal");
        facultyDao.save(faculty, root);

        AcFaculty fkm = facultyDao.findByCode("FKM");
        Assert.assertNotNull(fkm);
        LOG.debug("faculty {}", fkm.getDescription());
    }
}
