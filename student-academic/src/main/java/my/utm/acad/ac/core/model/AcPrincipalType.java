package my.utm.acad.ac.core.model;

public enum AcPrincipalType {
    USER,
    GROUP,
}
