package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcAcademicClassification;
import my.utm.acad.ac.core.model.AcAcademicYear;
import my.utm.acad.ac.core.model.AcCohort;
import my.utm.acad.ac.core.model.AcProgram;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class AcCohortImpl implements AcCohort {

    private Long id;
    private String code;
    private String prefixCode;
    private String description;

    private AcAcademicClassification academicClassification;
    private AcAcademicYear academicYear;
    private AcProgram program;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getPrefixCode() {
        return prefixCode;
    }

    @Override
    public void setPrefixCode(String prefixCode) {
        this.prefixCode = prefixCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public AcAcademicClassification getAcademicClassification() {
        return academicClassification;
    }

    @Override
    public void setAcademicClassification(AcAcademicClassification academicClassification) {
        this.academicClassification = academicClassification;
    }

    @Override
    public AcAcademicYear getAcademicYear() {
        return academicYear;
    }

    @Override
    public void setAcademicYear(AcAcademicYear academicYear) {
        this.academicYear = academicYear;
    }

    @Override
    public AcProgram getProgram() {
        return program;
    }

    @Override
    public void setProgram(AcProgram program) {
        this.program = program;
    }
}
