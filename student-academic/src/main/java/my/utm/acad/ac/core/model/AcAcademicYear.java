package my.utm.acad.ac.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcAcademicYear {
    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);
}
