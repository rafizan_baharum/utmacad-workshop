package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcGroup;
import my.utm.acad.ac.core.model.AcGroupMember;
import my.utm.acad.ac.core.model.AcMetadata;
import my.utm.acad.ac.core.model.AcPrincipal;

import javax.persistence.*;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "AcGroupMember")
@Table(name = "AC_GROP_MMBR")
public class AcGroupMemberImpl implements AcGroupMember {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_AC_GROP_MMBR")
    @SequenceGenerator(name = "SEQ_AC_GROP_MMBR", sequenceName = "SEQ_AC_GROP_MMBR", allocationSize = 1)
    private Long id;

    @OneToOne(targetEntity = AcGroupImpl.class)
    @JoinColumn(name = "GROUP_ID")
    private AcGroup group;

    @OneToOne(targetEntity = AcPrincipalImpl.class)
    @JoinColumn(name = "PRINCIPAL_ID")
    private AcPrincipal principal;

    @Embedded
    private AcMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public AcGroup getGroup() {
        return group;
    }

    @Override
    public void setGroup(AcGroup group) {
        this.group = group;
    }

    @Override
    public AcPrincipal getPrincipal() {
        return principal;
    }

    @Override
    public void setPrincipal(AcPrincipal principal) {
        this.principal = principal;
    }

    @Override
    public AcMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(AcMetadata metadata) {
        this.metadata = metadata;
    }
}
