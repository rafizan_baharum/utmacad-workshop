package my.utm.acad.ac.core.model;

import static my.utm.acad.ac.core.model.AcAssessmentCategoryType.COURSE_WORK;
import static my.utm.acad.ac.core.model.AcAssessmentCategoryType.EXAM_WORK;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public enum AcAssessmentType {

    QUIZ(COURSE_WORK),               // 0   // cw
    TEST(COURSE_WORK),               // 1
    REPORT(COURSE_WORK),             // 2
    ASSIGNMENT(COURSE_WORK),         // 3
    PROJECT(COURSE_WORK),            // 4
    PRESENTATION(COURSE_WORK),       // 5
    VIVA(COURSE_WORK),               // 6
    LAB_REPORT(COURSE_WORK),         // 7
    LAB_ASSIGNMENT(COURSE_WORK),     // 8
    FINAL_EXAM(COURSE_WORK),         // 9  // ew
    THESIS(COURSE_WORK),             // 10
    ATTENDANCE(COURSE_WORK),         // 11
    SURVEY(COURSE_WORK),             // 12
    EVALUATION(COURSE_WORK),         // 13
    TUTORIAL(COURSE_WORK),           // 14
    CASE_STUDIES(COURSE_WORK),       // 15
    FINAL_YEAR_PROJECT(EXAM_WORK), // 16
    IN_CLASS_EXERCISE(COURSE_WORK),  // 17
    REFLECTION_JOURNAL(COURSE_WORK), // 18
    PAIR_TEST(COURSE_WORK),          // 19
    PEER_GRADING(COURSE_WORK),       // 20
    PEER_EVALUATION(COURSE_WORK),    // 21
    JOURNAL(COURSE_WORK),            // 22
    OTHER(COURSE_WORK);              // 23

    private String label = "";

    private AcAssessmentCategoryType category;

    private AcAssessmentType() {
    }

    private AcAssessmentType(AcAssessmentCategoryType category) {
        this.category = category;
    }

    private AcAssessmentType(String label) {
        this.label = label;
    }

    public static AcAssessmentType get(int index) {
        return values()[index];
    }

}
