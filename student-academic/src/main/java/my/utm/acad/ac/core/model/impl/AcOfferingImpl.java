package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.*;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class AcOfferingImpl implements AcOffering {

    private Long id;
    private String code;
    private String title;
    private AcOfferingStatus status;
    private AcProgram program;
    private AcCourse course;

    private List<AcSection> sections;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public AcProgram getProgram() {
        return program;
    }

    @Override
    public void setProgram(AcProgram program) {
        this.program = program;
    }

    @Override
    public AcCourse getCourse() {
        return course;
    }

    @Override
    public void setCourse(AcCourse course) {
        this.course = course;
    }

    @Override
    public List<AcSection> getSections() {
        return sections;
    }

    @Override
    public void setSections(List<AcSection> sections) {
        this.sections = sections;
    }
}
