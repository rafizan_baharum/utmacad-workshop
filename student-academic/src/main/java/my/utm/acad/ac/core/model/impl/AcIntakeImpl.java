package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcAcademicYear;
import my.utm.acad.ac.core.model.AcIntake;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class AcIntakeImpl implements AcIntake {

    private Long id;
    private AcAcademicYear year;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public AcAcademicYear getYear() {
        return year;
    }

    @Override
    public void setYear(AcAcademicYear year) {
        this.year = year;
    }
}
