package my.utm.acad.ac.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public enum AcStudentType {
    UNDER_GRAD,
    POST_GRAD;
}
