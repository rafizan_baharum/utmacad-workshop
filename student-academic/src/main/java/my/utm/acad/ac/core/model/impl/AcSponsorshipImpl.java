package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.*;

import java.util.Date;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 19/2/2015.
 */
public class AcSponsorshipImpl implements AcSponsorship {

    private Long id;
    private Date startDate;
    private Date endDate;
    private boolean current;
    private AcSponsorshipTier tier;
    private AcSponsor sponsor;
    private AcStudent student;
    private AcMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @Override
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean isCurrent() {
        return current;
    }

    @Override
    public void setCurrent(boolean current) {
        this.current = current;
    }

    @Override
    public AcSponsorshipTier getTier() {
        return tier;
    }

    @Override
    public void setTier(AcSponsorshipTier tier) {
        this.tier = tier;
    }

    @Override
    public AcSponsor getSponsor() {
        return sponsor;
    }

    @Override
    public void setSponsor(AcSponsor sponsor) {
        this.sponsor = sponsor;
    }

    @Override
    public AcStudent getStudent() {
        return student;
    }

    @Override
    public void setStudent(AcStudent student) {
        this.student = student;
    }

    @Override
    public AcMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(AcMetadata metadata) {
        this.metadata = metadata;
    }
}
