package my.utm.acad.ac.biz.manager;

import my.utm.acad.ac.core.model.AcCourse;
import my.utm.acad.ac.core.model.AcOffering;
import my.utm.acad.ac.core.model.AcProgram;
import my.utm.acad.ac.core.model.AcSection;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface ProgramManager {

    void createOffering(AcProgram program, AcCourse course, AcOffering offering);

    void createSection(AcSection section, AcOffering offering);
}
