package my.utm.acad.ac.core.model;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface AcProgram extends AcMetaObject {

    String getCode();

    void setCode(String code);

    String getPrefixCode();

    void setPrefixCode(String prefixCode);

    String getDescription();

    void setDescription(String description);

    AcFaculty getFaculty();

    void setFaculty(AcFaculty faculty);
}
