package my.utm.acad.ac.biz.manager;

import my.utm.acad.ac.core.model.AcAcademicSession;
import my.utm.acad.ac.core.model.AcSection;
import my.utm.acad.ac.core.model.AcStudent;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface EnrollmentManager {

    void enroll(AcSection section, AcStudent student);

    void withdraw(AcSection section, AcStudent student);

    void processPayment(AcAcademicSession session);
}
