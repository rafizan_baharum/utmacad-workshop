package my.utm.acad.ac.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public enum AcSectionStatus {

    NEW,    // 0
    STARTED, // 1
    ENDED,   // 2
    CANCELLED; // 3

    public static AcSectionStatus get(int index) {
        return values()[index];
    }

}
