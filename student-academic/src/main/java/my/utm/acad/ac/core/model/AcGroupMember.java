package my.utm.acad.ac.core.model;

public interface AcGroupMember extends AcMetaObject {

    AcPrincipal getPrincipal();

    void setPrincipal(AcPrincipal principal);

    AcGroup getGroup();

    void setGroup(AcGroup group);
}
