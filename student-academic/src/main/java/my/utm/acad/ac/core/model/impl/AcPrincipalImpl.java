package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcMetadata;
import my.utm.acad.ac.core.model.AcPrincipal;
import my.utm.acad.ac.core.model.AcPrincipalType;

import javax.persistence.*;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "AcPrincipal")
@Table(name = "AC_PCPL")
@Inheritance(strategy = InheritanceType.JOINED)
public class AcPrincipalImpl implements AcPrincipal {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_AC_PCPL")
    @SequenceGenerator(name = "SEQ_AC_PCPL", sequenceName = "SEQ_AC_PCPL", allocationSize = 1)
    private Long id;

    @Column(name = "NAME", unique = true, nullable = false)
    private String name;

    @Column(name = "PRINCIPAL_TYPE")
    private AcPrincipalType principalType;

    @Embedded
    private AcMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public AcPrincipalType getPrincipalType() {
        return principalType;
    }

    @Override
    public void setPrincipalType(AcPrincipalType principalType) {
        this.principalType = principalType;
    }

    @Override
    public AcMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(AcMetadata metadata) {
        this.metadata = metadata;
    }
}
