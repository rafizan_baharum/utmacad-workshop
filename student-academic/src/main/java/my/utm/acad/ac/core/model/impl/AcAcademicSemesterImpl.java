package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcAcademicSemester;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class AcAcademicSemesterImpl implements AcAcademicSemester {

    private Long id;
    private String code;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
