package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcEnrollment;
import my.utm.acad.ac.core.model.AcPaymentBatchItem;
import my.utm.acad.ac.core.model.AcMetadata;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 19/2/2015.
 */
public class AcPaymentBatchItemImpl implements AcPaymentBatchItem {

    private Long id;
    private boolean eligible;
    private AcEnrollment enrollment;
    private AcMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean isEligible() {
        return eligible;
    }

    @Override
    public void setEligible(boolean eligible) {
        this.eligible = eligible;
    }

    @Override
    public AcEnrollment getEnrollment() {
        return enrollment;
    }

    @Override
    public void setEnrollment(AcEnrollment enrollment) {
        this.enrollment = enrollment;
    }

    @Override
    public AcMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(AcMetadata metadata) {
        this.metadata = metadata;
    }
}
