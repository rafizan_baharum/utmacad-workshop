package my.utm.acad.ac.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcActor extends AcObject {

    String getNricNo();

    void setNricNo(String nricNo);

    String getPassportNo();

    void setPassportNo(String passportNo);

    String getName();

    void setName(String name);

    String getPrimaryEmail();

    void setPrimaryEmail(String email);

    String getSecondaryEmail();

    void setSecondaryEmail(String email);

    AcCampus getCampus();

    void setCampus(AcCampus campus);

}
