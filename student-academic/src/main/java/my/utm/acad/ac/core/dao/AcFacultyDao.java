package my.utm.acad.ac.core.dao;

import my.utm.acad.ac.core.model.AcCourse;
import my.utm.acad.ac.core.model.AcFaculty;
import my.utm.acad.ac.core.model.AcProgram;
import my.utm.acad.ac.core.model.AcUser;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcFacultyDao extends GenericDao<Long, AcFaculty> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    AcFaculty findByCode(String code);

    List<AcFaculty> find();

    List<AcFaculty> find(String filter, Integer offset, Integer limit);

    List<AcProgram> findPrograms(AcFaculty faculty);

    List<AcProgram> findPrograms(AcFaculty faculty, Integer offset, Integer limit);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(String filter);

    Integer countProgram(AcFaculty faculty);

    boolean isExists(String code);

    // ====================================================================================================
    // CRUD
    // ====================================================================================================

    void addProgram(AcFaculty faculty, AcProgram program, AcUser user);

    void updateProgram(AcFaculty faculty, AcProgram program, AcUser user);

    void removeProgram(AcFaculty faculty, AcProgram program, AcUser user);

    void addCourse(AcFaculty faculty, AcCourse course, AcUser user);

    void updateCourse(AcFaculty faculty, AcCourse course, AcUser user);

    void removeCourse(AcFaculty faculty, AcCourse course, AcUser user);


    //    NOTE: available after workshop Spring Security
//    List<AcFaculty> findAuthorized(Set<String> sids, String filter, Integer offset, Integer limit);
//
//    Integer countAuthorized(Set<String> sids, String filter);

}
