package my.utm.acad.ac.core.model;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcCurriculum extends AcObject {

    String getCode(); // SCK01 SCK02

    void setCode(String code);

    String getDescription();

    void setDescription(String description);

    AcProgram getProgram();

    void setProgram(AcProgram program);

    List<AcSubject> getSubjects();

    void setSubjects(List<AcSubject> subjects);
}
