package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcFaculty;
import my.utm.acad.ac.core.model.AcMetadata;

import javax.persistence.*;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "AcFaculty")
@Table(name = "AC_FCTY")
public class AcFacultyImpl implements AcFaculty {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_AC_FCTY")
    @SequenceGenerator(name = "SEQ_AC_FCTY", sequenceName = "SEQ_AC_FCTY", allocationSize = 1)
    private Long id;

    @Column(name = "CODE", nullable = false, unique = true)
    private String code;

    @Column(name = "PREFIX_CODE")
    private String prefixCode;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Embedded
    private AcMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getPrefixCode() {
        return prefixCode;
    }

    @Override
    public void setPrefixCode(String prefixCode) {
        this.prefixCode = prefixCode;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public AcMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(AcMetadata metadata) {
        this.metadata = metadata;
    }
}
