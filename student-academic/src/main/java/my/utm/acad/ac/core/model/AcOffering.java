package my.utm.acad.ac.core.model;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcOffering extends AcObject{

    String getCode();

    void setCode(String code);

    String getTitle();

    void setTitle(String title);

    AcProgram getProgram();

    void setProgram(AcProgram program);

    AcCourse getCourse();

    void setCourse(AcCourse course);

    List<AcSection> getSections();

    void setSections(List<AcSection> sections);

}
