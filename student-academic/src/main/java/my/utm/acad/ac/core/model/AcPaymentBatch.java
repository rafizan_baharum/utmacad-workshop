package my.utm.acad.ac.core.model;

import java.util.List;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 18/2/2015.
 */
public interface AcPaymentBatch extends AcMetaObject{

    AcSponsor getSponsor();

    void setSponsor(AcSponsor sponsor);

    AcAcademicSession getSession();

    void setSession(AcAcademicSession session);

    List<AcPaymentBatchItem> getItems();

    void setItems(List<AcPaymentBatchItem> items);
}
