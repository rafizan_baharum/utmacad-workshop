package my.utm.acad.ac.biz.manager;

import my.utm.acad.ac.core.dao.AcEnrollmentDao;
import my.utm.acad.ac.core.model.AcAcademicSession;
import my.utm.acad.ac.core.model.AcEnrollment;
import my.utm.acad.ac.core.model.AcSection;
import my.utm.acad.ac.core.model.AcStudent;
import my.utm.acad.ac.core.model.impl.AcEnrollmentImpl;
import org.springframework.beans.factory.annotation.Autowired;

import static my.utm.acad.ac.core.model.AcEnrollmentStatus.CONFIRMED;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class EnrollmentManagerImpl implements EnrollmentManager {

    @Autowired
    private AcEnrollmentDao enrollmentDao;

    @Override
    public void enroll(AcSection section, AcStudent student) {
        AcEnrollment enrollment = new AcEnrollmentImpl();
        enrollment.setEnrollmentStatus(CONFIRMED);
        enrollment.setSection(section);
        enrollment.setStudent(student);
    }

    @Override
    public void withdraw(AcSection section, AcStudent student) {
        // find enrollment for this student
        // if there's one, remove enrollment
    }

    @Override
    public void processPayment(AcAcademicSession session) {

    }
}
