package my.utm.acad.ac.core.dao;

import my.utm.acad.ac.core.model.AcPrincipal;
import my.utm.acad.ac.core.model.AcPrincipalType;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcPrincipalDao extends GenericDao<Long, AcPrincipal> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    AcPrincipal findByName(String name);

    List<AcPrincipal> findAllPrincipals();

    List<AcPrincipal> find(String filter);

    List<AcPrincipal> find(String filter, AcPrincipalType type);

    List<AcPrincipal> find(String filter, Integer offset, Integer limit);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(String filter);
}
