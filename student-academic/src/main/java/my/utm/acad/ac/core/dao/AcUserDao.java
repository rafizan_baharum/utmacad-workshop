package my.utm.acad.ac.core.dao;

import my.utm.acad.ac.core.model.AcActor;
import my.utm.acad.ac.core.model.AcGroup;
import my.utm.acad.ac.core.model.AcUser;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcUserDao extends GenericDao<Long, AcUser> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    AcUser findByEmail(String email);

    AcUser findByUsername(String username);

    AcUser findByActor(AcActor actor);

    List<AcUser> find(String filter, Integer offset, Integer limit);

    List<AcGroup> findGroups(AcUser user);

    Integer count(String filter);

    // ====================================================================================================
    // CRUD
    // ====================================================================================================
    void save(AcUser user);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    boolean isExists(String username);

    boolean hasUser(AcActor actor);
}