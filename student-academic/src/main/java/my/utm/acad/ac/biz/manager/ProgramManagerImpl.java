package my.utm.acad.ac.biz.manager;

import my.utm.acad.ac.core.model.AcCourse;
import my.utm.acad.ac.core.model.AcOffering;
import my.utm.acad.ac.core.model.AcProgram;
import my.utm.acad.ac.core.model.AcSection;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class ProgramManagerImpl implements ProgramManager {

    @Override
    public void createOffering(AcProgram program, AcCourse course, AcOffering offering) {
        // save offering
        offering.setCode(program.getCode() + "." + course.getCode());
        offering.setTitle(course.getTitle());
        offering.setProgram(program);
        offering.setCourse(course);

        // create other stuff
    }

    @Override
    public void createSection(AcSection section, AcOffering offering) {
        // save section
        section.setOffering(offering);

        // create other stuff
    }
}
