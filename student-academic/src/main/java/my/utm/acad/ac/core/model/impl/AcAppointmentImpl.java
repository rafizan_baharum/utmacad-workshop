package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcAppointment;
import my.utm.acad.ac.core.model.AcAppointmentStatus;
import my.utm.acad.ac.core.model.AcSection;
import my.utm.acad.ac.core.model.AcStaff;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class AcAppointmentImpl implements AcAppointment {

    private Long id;
    private AcAppointmentStatus status;
    private AcSection section;
    private AcStaff staff;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AcAppointmentStatus getStatus() {
        return status;
    }

    public void setStatus(AcAppointmentStatus status) {
        this.status = status;
    }

    @Override
    public AcSection getSection() {
        return section;
    }

    @Override
    public void setSection(AcSection section) {
        this.section = section;
    }

    @Override
    public AcStaff getStaff() {
        return staff;
    }

    @Override
    public void setStaff(AcStaff staff) {
        this.staff = staff;
    }
}
