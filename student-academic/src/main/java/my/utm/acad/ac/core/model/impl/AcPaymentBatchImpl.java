package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.*;

import java.util.List;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 19/2/2015.
 */
public class AcPaymentBatchImpl implements AcPaymentBatch {

    private Long id;
    private AcSponsor sponsor;
    private AcAcademicSession session;
    private List<AcPaymentBatchItem> items;
    private AcMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public AcSponsor getSponsor() {
        return sponsor;
    }

    @Override
    public void setSponsor(AcSponsor sponsor) {
        this.sponsor = sponsor;
    }

    @Override
    public AcAcademicSession getSession() {
        return session;
    }

    @Override
    public void setSession(AcAcademicSession session) {
        this.session = session;
    }

    @Override
    public List<AcPaymentBatchItem> getItems() {
        return items;
    }

    @Override
    public void setItems(List<AcPaymentBatchItem> items) {
        this.items = items;
    }

    @Override
    public AcMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(AcMetadata metadata) {
        this.metadata = metadata;
    }
}
