package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.*;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class AcSectionImpl implements AcSection {

    private Long id;
    private String code;
    private AcAcademicSession session;
    private AcOffering offering;
    private List<AcEnrollment> enrollments;
    private List<AcAppointment> appointments;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public AcAcademicSession getSession() {
        return session;
    }

    @Override
    public void setSession(AcAcademicSession session) {
        this.session = session;
    }

    @Override
    public AcOffering getOffering() {
        return offering;
    }

    @Override
    public void setOffering(AcOffering offering) {
        this.offering = offering;
    }

    @Override
    public List<AcEnrollment> getEnrollments() {
        return enrollments;
    }

    @Override
    public void setEnrollments(List<AcEnrollment> enrollments) {
        this.enrollments = enrollments;
    }

    @Override
    public List<AcAppointment> getAppointments() {
        return appointments;
    }

    @Override
    public void setAppointments(List<AcAppointment> appointments) {
        this.appointments = appointments;
    }
}
