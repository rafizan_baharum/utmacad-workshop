package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcAcademicSemester;
import my.utm.acad.ac.core.model.AcAcademicSession;
import my.utm.acad.ac.core.model.AcAcademicYear;

import javax.persistence.*;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "AcAcademicSessionImpl")
@Table(name = "AC_ACDM_SESN")
public class AcAcademicSessionImpl implements AcAcademicSession {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_AC_ACDM_SESN")
    @SequenceGenerator(name = "SEQ_AC_ACDM_SESN", sequenceName = "SEQ_AC_ACDM_SESN", allocationSize = 1)
    private Long id;

    @Column(name = "CODE", nullable = false)
    private String code;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    private AcAcademicSession previous;
    private AcAcademicSemester semester;
    private AcAcademicYear year;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    public AcAcademicSession getPrevious() {
        return previous;
    }

    public void setPrevious(AcAcademicSession previous) {
        this.previous = previous;
    }

    @Override
    public AcAcademicSemester getSemester() {
        return semester;
    }

    @Override
    public void setSemester(AcAcademicSemester semester) {
        this.semester = semester;
    }

    @Override
    public AcAcademicYear getYear() {
        return year;
    }

    @Override
    public void setYear(AcAcademicYear year) {
        this.year = year;
    }
}
