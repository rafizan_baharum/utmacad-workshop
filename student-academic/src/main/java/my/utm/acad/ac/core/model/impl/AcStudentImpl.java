package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.*;

import java.util.Date;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public class AcStudentImpl extends AcActorImpl implements AcStudent {

    private Long id;
    private String idName;
    private Date birthDate;
    private AcProgram program;
    private AcCohort cohort;
    private AcSponsor sponsor;
    private AcIntake intake;
    private AcCampus campus;
    private AcAdmissionStatus admissionStatus;
    private AcStudentType studentType;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getMatricNo() {
        return getIdentityNo();
    }

    @Override
    public void setMatricNo(String matricNo) {
        setIdentityNo(matricNo);
    }

    @Override
    public String getIdName() {
        return idName;
    }

    public void setIdName(String idName) {
        this.idName = idName;
    }

    @Override
    public Date getBirthDate() {
        return birthDate;
    }

    @Override
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public AcStudentType getStudentType() {
        return studentType;
    }

    public void setStudentType(AcStudentType studentType) {
        this.studentType = studentType;
    }

    @Override
    public AcProgram getProgram() {
        return program;
    }

    @Override
    public void setProgram(AcProgram program) {
        this.program = program;
    }


    @Override
    public AcCohort getCohort() {
        return cohort;
    }

    @Override
    public void setCohort(AcCohort cohort) {
        this.cohort = cohort;
    }

    @Override
    public AcIntake getIntake() {
        return intake;
    }

    @Override
    public void setIntake(AcIntake intake) {
        this.intake = intake;
    }

    @Override
    public AcSponsor getSponsor() {
        return sponsor;
    }

    @Override
    public void setSponsor(AcSponsor sponsor) {
        this.sponsor = sponsor;
    }

    @Override
    public AcCampus getCampus() {
        return campus;
    }

    @Override
    public void setCampus(AcCampus campus) {
        this.campus = campus;
    }

    @Override
    public AcAdmissionStatus getAdmissionStatus() {
        return admissionStatus;
    }

    @Override
    public void setAdmissionStatus(AcAdmissionStatus admissionStatus) {
        this.admissionStatus = admissionStatus;
    }
}
