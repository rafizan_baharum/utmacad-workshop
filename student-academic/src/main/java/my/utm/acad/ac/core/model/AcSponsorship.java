package my.utm.acad.ac.core.model;

import java.util.Date;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 19/2/2015.
 */
public interface AcSponsorship extends AcMetaObject {

    // todo: transient ?
    boolean isCurrent();

    void setCurrent(boolean current);

    Date getStartDate();

    void setEndDate(Date endDate);

    AcSponsor getSponsor();

    void setSponsor(AcSponsor sponsor);

    AcStudent getStudent();

    void setStudent(AcStudent student);

    AcSponsorshipTier getTier();

    void setTier(AcSponsorshipTier tier);
}
