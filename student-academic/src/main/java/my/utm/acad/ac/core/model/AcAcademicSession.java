package my.utm.acad.ac.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcAcademicSession {
    
    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);

    AcAcademicSession getPrevious();

    void setPrevious(AcAcademicSession previous);
    
    AcAcademicSemester getSemester();

    void setSemester(AcAcademicSemester academicSemester);

    AcAcademicYear getYear();

    void setYear(AcAcademicYear academicYear);

}
