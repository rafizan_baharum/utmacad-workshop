package my.utm.acad.ac.core.dao;

import my.utm.acad.ac.core.model.AcUser;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface GenericDao<K, I> {

    I newInstance();

    I refresh(I i);

    I findById(K k);

    List<I> find(Integer offset, Integer limit);

    Integer count();

    void save(I entity, AcUser user);

    void saveOrUpdate(I entity, AcUser user);

    void update(I entity, AcUser user);

    void deactivate(I entity, AcUser user);

    void remove(I entity, AcUser user);

    void delete(I entity, AcUser user);

//    NOTE: available after workshop Spring Security
//    List<I> findAuthorized(Set<String> sids);
//
//    List<I> findAuthorized(Set<String> sids, Integer offset, Integer limit);
//
//    List<Long> findAuthorizedIds(Set<String> sids);

//    Integer countAuthorized(Set<String> sids);


}
