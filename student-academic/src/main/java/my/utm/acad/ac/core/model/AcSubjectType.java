package my.utm.acad.ac.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public enum AcSubjectType {
    CORE,
    ELECTIVE,
    REQUIRED;

    // wajib/teras
    // umum/required
    // BI
    // xx
    //x
}
