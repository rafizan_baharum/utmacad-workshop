package my.utm.acad.ac.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcEnrollment extends AcObject{

    AcEnrollmentStatus getEnrollmentStatus();

    void setEnrollmentStatus(AcEnrollmentStatus status);

    AcStudent getStudent();

    void setStudent(AcStudent student);

    AcSection getSection();

    void setSection(AcSection section);
}
