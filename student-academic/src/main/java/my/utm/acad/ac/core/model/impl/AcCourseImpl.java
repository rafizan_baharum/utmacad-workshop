package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.*;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class AcCourseImpl implements AcCourse {

    private Long id;
    private String code;
    private String title;
    private Integer creditHour;
    private AcAcademicClassification classification;
    private AcFaculty faculty;
    private List<AcOffering> offerings;
    private AcMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public Integer getCreditHour() {
        return creditHour;
    }

    @Override
    public void setCreditHour(Integer creditHour) {
        this.creditHour = creditHour;
    }

    public AcAcademicClassification getClassification() {
        return classification;
    }

    public void setClassification(AcAcademicClassification classification) {
        this.classification = classification;
    }

    @Override
    public AcFaculty getFaculty() {
        return faculty;
    }

    @Override
    public void setFaculty(AcFaculty faculty) {
        this.faculty = faculty;
    }

    @Override
    public List<AcOffering> getOfferings() {
        return offerings;
    }

    @Override
    public void setOfferings(List<AcOffering> offerings) {
        this.offerings = offerings;
    }

    @Override
    public AcMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(AcMetadata metadata) {
        this.metadata = metadata;
    }
}
