package my.utm.acad.ac.core.model;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface AcIntake extends AcObject {

    AcAcademicYear getYear();

    void setYear(AcAcademicYear year);
}
