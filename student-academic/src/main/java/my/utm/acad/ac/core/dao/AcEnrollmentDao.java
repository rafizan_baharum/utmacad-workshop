package my.utm.acad.ac.core.dao;

import my.utm.acad.ac.core.model.*;

import java.util.List;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public interface AcEnrollmentDao extends GenericDao<Long, AcEnrollmentDao> {

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    AcEnrollment findByMatricNoAndOffering(String matricNo, AcOffering offering);

    AcEnrollment findBySectionAndStudent(AcSection section, AcStudent student);

    List<AcEnrollment> find(AcProgram program);

    List<AcEnrollment> find(AcOffering offering);

    List<AcEnrollment> find(AcStudent student);

    List<AcEnrollment> find(AcAcademicSession academicSession);

    List<AcEnrollment> find(AcAcademicSession academicSession, AcSponsor sponsor);

    List<AcEnrollment> find(AcAcademicSession academicSession, AcOffering offering);

    List<AcEnrollment> find(AcOffering offering, Integer offset, Integer limit);

    List<AcEnrollment> find(String filter, AcOffering offering, Integer offset, Integer limit);

    List<AcEnrollment> find(AcAcademicSession academicSession, AcOffering offering, Integer offset, Integer limit);

    List<AcEnrollment> find(String filter, AcAcademicSession academicSession, AcOffering offering, Integer offset, Integer limit);

    List<AcEnrollment> find(AcSection section, Integer offset, Integer limit);

    List<AcEnrollment> find(String filter, AcSection section, Integer offset, Integer limit);

    List<AcEnrollment> find(AcStudent student, Integer offset, Integer limit);

    List<AcStudent> findStudents(AcProgram program);

    List<AcStudent> findStudents(AcProgram program, Integer offset, Integer limit);

    List<AcStudent> findStudents(AcOffering offering);

    List<AcStudent> findStudents(AcOffering offering, Integer offset, Integer limit);

    List<AcStudent> findStudents(AcSection section);

    List<AcStudent> findStudents(AcSection section, Integer offset, Integer limit);

    List<AcStudent> findAvailableStudents(AcOffering offering);

    List<AcStudent> findAvailableStudents(AcOffering offering, Integer offset, Integer limit);

    List<AcStudent> findAvailableStudents(String filter, AcOffering offering, Integer offset, Integer limit);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(AcOffering offering);

    Integer count(String filter, AcOffering offering);

    Integer count(AcAcademicSession academicSession, AcOffering offering);

    Integer count(String filter, AcAcademicSession academicSession, AcOffering offering);

    Integer count(AcSection section);

    Integer count(String filter, AcSection section);

    Integer count(AcStudent student);

    Integer countStudent(AcOffering offering);

    Integer countStudent(AcSection section);

    boolean isExists(AcSection section, AcStudent student);

    boolean isAnyExists(AcAcademicSession academicSession, AcOffering offering, AcStudent student);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

}
