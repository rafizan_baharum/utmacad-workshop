package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcActor;
import my.utm.acad.ac.core.model.AcUser;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "AcUser")
@Table(name = "AC_USER")
public class AcUserImpl extends AcPrincipalImpl implements AcUser {

    @NotNull
    @Column(name = "REAL_NAME")
    private String realName;

    @NotNull
    @Column(name = "PASSWORD")
    private String password;

    @OneToOne(targetEntity = AcActorImpl.class)
    @JoinColumn(name = "ACTOR_ID")
    private AcActor actor;

    @Override
    public String getRealName() {
        return realName;
    }

    @Override
    public void setRealName(String realName) {
        this.realName = realName;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public AcActor getActor() {
        return actor;
    }

    @Override
    public void setActor(AcActor actor) {
        this.actor = actor;
    }
}
