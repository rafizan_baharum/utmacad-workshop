package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcAcademicYear;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class AcAcademicYearImpl implements AcAcademicYear {

    private Long id;
    private String code;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }
}
