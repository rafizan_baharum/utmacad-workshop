package my.utm.acad.ac.core.model;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcCourse extends AcMetaObject{

    String getCode();

    void setCode(String code);

    String getTitle();

    void setTitle(String title);

    Integer getCreditHour();

    void setCreditHour(Integer creditHour);

    AcAcademicClassification getClassification();

    void setClassification(AcAcademicClassification classification);

    AcFaculty getFaculty();

    void setFaculty(AcFaculty faculty);

    List<AcOffering> getOfferings();

    void setOfferings(List<AcOffering> offerings);


}
