package my.utm.acad.ac.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcStaff extends AcActor {

    AcFaculty getFaculty();

    void setFaculty(AcFaculty faculty);
}
