package my.utm.acad.ac.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcFaculty extends AcMetaObject {

    String getCode();

    void setCode(String code);

    String getPrefixCode();

    void setPrefixCode(String prefixCode);

    String getDescription();

    void setDescription(String description);

}
