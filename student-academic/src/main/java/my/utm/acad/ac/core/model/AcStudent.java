package my.utm.acad.ac.core.model;

import java.util.Date;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface AcStudent extends AcActor {

    String getMatricNo();

    void setMatricNo(String matricNo);

    String getIdName();

    void setIdName(String idName);

    Date getBirthDate();

    void setBirthDate(Date birthDate);

    AcAdmissionStatus getAdmissionStatus();

    void setAdmissionStatus(AcAdmissionStatus admissionStatus);

    AcProgram getProgram();

    void setProgram(AcProgram program);

    AcSponsor getSponsor();

    void setSponsor(AcSponsor sponsor);

    AcIntake getIntake();

    void setIntake(AcIntake intake);

    AcCohort getCohort();

    void setCohort(AcCohort cohort);

}
