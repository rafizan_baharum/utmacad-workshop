package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcGroup;
import my.utm.acad.ac.core.model.AcGroupMember;

import javax.persistence.*;
import java.util.Set;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "AcGroup")
@Table(name = "AC_GROP")
public class AcGroupImpl extends AcPrincipalImpl implements AcGroup {

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = AcGroupMemberImpl.class)
    @JoinTable(name = "AC_GROP_MMBR", joinColumns = {
            @JoinColumn(name = "GROUP_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "PRACCIPAL_ID",
                    nullable = false, updatable = false)})
    private Set<AcGroupMember> members;

    @Override
    public Set<AcGroupMember> getMembers() {
        return members;
    }

    @Override
    public void setMembers(Set<AcGroupMember> members) {
        this.members = members;
    }
}
