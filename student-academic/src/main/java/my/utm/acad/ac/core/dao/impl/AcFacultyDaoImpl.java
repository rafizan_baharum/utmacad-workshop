package my.utm.acad.ac.core.dao.impl;

import my.utm.acad.ac.core.dao.AcFacultyDao;
import my.utm.acad.ac.core.dao.GenericDaoSupport;
import my.utm.acad.ac.core.model.*;
import my.utm.acad.ac.core.model.impl.AcFacultyImpl;
import org.apache.commons.lang.Validate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Repository("acFacultyDao")
public class AcFacultyDaoImpl extends GenericDaoSupport<Long, AcFaculty> implements AcFacultyDao {

    private static final Logger LOG = LoggerFactory.getLogger(AcFacultyDaoImpl.class);

    public AcFacultyDaoImpl() {
        super(AcFacultyImpl.class);
    }

    @Override
    public AcFaculty findByCode(String code) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from AcFaculty s where s.code = :code and  " +
                " s.metadata.state = :state");
        query.setString("code", code);
        query.setCacheable(true);
        query.setInteger("state", AcMetaState.ACTIVE.ordinal());
        return (AcFaculty) query.uniqueResult();
    }

    @Override
    public List<AcFaculty> find() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from AcFaculty s where " +
                " s.metadata.state = :state ");
        query.setInteger("state", AcMetaState.ACTIVE.ordinal());
        query.setCacheable(true);
        return (List<AcFaculty>) query.list();
    }

    @Override
    public List<AcFaculty> find(String filter, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from AcFaculty s where " +
                "(upper(s.code) like upper(:filter) " +
                "or upper(s.name) like upper(:filter)) " +
                "and s.metadata.state = :state ");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setInteger("state", AcMetaState.ACTIVE.ordinal());
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        query.setCacheable(true);
        return (List<AcFaculty>) query.list();
    }

    @Override
    public List<AcProgram> findPrograms(AcFaculty faculty) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from AcProgram s where " +
                "s.faculty = :faculty " +
                "and s.metadata.state = :state ");
        query.setInteger("state", AcMetaState.ACTIVE.ordinal());
        query.setEntity("faculty", faculty);
        query.setCacheable(true);
        return (List<AcProgram>) query.list();
    }

    @Override
    public List<AcProgram> findPrograms(AcFaculty faculty, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from AcProgram s where " +
                "s.faculty = :faculty " +
                "and s.metadata.state = :state ");
        query.setInteger("state", AcMetaState.ACTIVE.ordinal());
        query.setEntity("faculty", faculty);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        query.setCacheable(true);
        return (List<AcProgram>) query.list();
    }

    @Override
    public Integer count(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(s) from AcFaculty s where " +
                "(upper(s.code) like upper(:filter) " +
                "or upper(s.name) like upper(:filter)) " +
                "and s.metadata.state = :state ");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setInteger("state", AcMetaState.ACTIVE.ordinal());
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public Integer countProgram(AcFaculty faculty) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(s) from AcProgram s where " +
                "s.faculty = :faculty " +
                "and s.metadata.state = :state ");
        query.setEntity("faculty", faculty);
        query.setInteger("state", AcMetaState.ACTIVE.ordinal());
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public boolean isExists(String code) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(*) from AcFaculty s where " +
                "s.code = :code " +
                "and s.metadata.state = :state ");
        query.setString("code", code);
        query.setInteger("state", AcMetaState.ACTIVE.ordinal());
        return 0 < ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public void addProgram(AcFaculty faculty, AcProgram program, AcUser user) {
        Validate.notNull(user, "User cannot be null");
        Validate.notNull(faculty, "Faculty cannot be null");
        Validate.notNull(program, "program cannot be null");
        Session session = sessionFactory.getCurrentSession();
        program.setFaculty(faculty);

        // prepare metadata
        AcMetadata metadata = new AcMetadata();
        metadata.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        metadata.setCreatorId(user.getId());
        metadata.setState(AcMetaState.ACTIVE);
        program.setMetadata(metadata);
        session.save(program);
    }

    @Override
    public void updateProgram(AcFaculty faculty, AcProgram program, AcUser user) {
        Validate.notNull(user, "User cannot be null");
        Session session = sessionFactory.getCurrentSession();
        program.setFaculty(faculty);

        // prepare metadata
        AcMetadata metadata = program.getMetadata();
        metadata.setModifiedDate(new Timestamp(System.currentTimeMillis()));
        metadata.setModifierId(user.getId());
        program.setMetadata(metadata);
        session.update(program);
    }

    @Override
    public void removeProgram(AcFaculty faculty, AcProgram program, AcUser user) {
        Validate.notNull(user, "User cannot be null");
        Validate.notNull(faculty, "Faculty cannot be null");
        Validate.notNull(program, "program cannot be null");
        Session session = sessionFactory.getCurrentSession();
        program.setFaculty(faculty);

        // prepare metadata
        AcMetadata metadata = program.getMetadata();
        metadata.setDeletedDate(new Timestamp(System.currentTimeMillis()));
        metadata.setDeleterId(user.getId());
        metadata.setState(AcMetaState.INACTIVE);
        program.setMetadata(metadata);
        session.update(program);
    }

    @Override
    public void addCourse(AcFaculty faculty, AcCourse course, AcUser user) {
        Validate.notNull(user, "User cannot be null");
        Validate.notNull(faculty, "Faculty cannot be null");
        Validate.notNull(course, "section cannot be null");
        Session session = sessionFactory.getCurrentSession();
        course.setFaculty(faculty);

        // prepare metadata
        AcMetadata metadata = new AcMetadata();
        metadata.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        metadata.setCreatorId(user.getId());
        metadata.setState(AcMetaState.ACTIVE);
        course.setMetadata(metadata);
        session.save(course);
    }

    @Override
    public void updateCourse(AcFaculty faculty, AcCourse course, AcUser user) {
        Validate.notNull(user, "User cannot be null");
        Session session = sessionFactory.getCurrentSession();
        course.setFaculty(faculty);

        // prepare metadata
        AcMetadata metadata = course.getMetadata();
        metadata.setModifiedDate(new Timestamp(System.currentTimeMillis()));
        metadata.setModifierId(user.getId());
        course.setMetadata(metadata);
        session.update(course);
    }

    @Override
    public void removeCourse(AcFaculty faculty, AcCourse course, AcUser user) {
        Validate.notNull(user, "User cannot be null");
        Session session = sessionFactory.getCurrentSession();
        course.setFaculty(faculty);

        // prepare metadata
        AcMetadata metadata = course.getMetadata();
        metadata.setDeletedDate(new Timestamp(System.currentTimeMillis()));
        metadata.setDeleterId(user.getId());
        metadata.setState(AcMetaState.INACTIVE);
        course.setMetadata(metadata);
        session.update(course);
    }
}