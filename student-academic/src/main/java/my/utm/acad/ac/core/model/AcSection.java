package my.utm.acad.ac.core.model;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcSection extends AcObject {

    String getCode();

    void setCode(String code);

    AcAcademicSession getSession();

    void setSession(AcAcademicSession session);

    AcOffering getOffering();

    void setOffering(AcOffering offering);

    List<AcEnrollment> getEnrollments();

    void setEnrollments(List<AcEnrollment> enrollments);

    List<AcAppointment> getAppointments();

    void setAppointments(List<AcAppointment> appointments);
}
