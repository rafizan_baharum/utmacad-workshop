package my.utm.acad.ac.core.model;

import java.util.Set;

public interface AcGroup extends AcPrincipal {

    Set<AcGroupMember> getMembers();

    void setMembers(Set<AcGroupMember> members);
}
