package my.utm.acad.ac.core.model;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface AcCohort extends AcObject {

    String getCode();

    void setCode(String code);

    String getPrefixCode();

    void setPrefixCode(String prefixCode);

    AcAcademicClassification getAcademicClassification();

    void setAcademicClassification(AcAcademicClassification academicClassification);

    AcAcademicYear getAcademicYear();

    void setAcademicYear(AcAcademicYear year);

    AcProgram getProgram();

    void setProgram(AcProgram program);

}
