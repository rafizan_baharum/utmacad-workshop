package my.utm.acad.ac.core.model;

/**
 * Acceptance of Offer
 * Medical Certification Form
 * Medical Checkup and X-Ray
 * Student Financial Declaration Form
 * Student Pledge
 * Declaration Form
 * Completed Matric Card Form
 *
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface AcDocument {

    String getReferenceNo();

    void setReferenceNo(String referenceNo);

    String getSourceNo();

    void setSourceNo(String sourceNo);

    String getDescription();

    void setDescription(String description);

}
