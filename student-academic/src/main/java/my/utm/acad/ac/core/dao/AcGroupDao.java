package my.utm.acad.ac.core.dao;

import my.utm.acad.ac.core.model.AcGroup;
import my.utm.acad.ac.core.model.AcPrincipal;
import my.utm.acad.ac.core.model.AcPrincipalType;
import my.utm.acad.ac.core.model.AcUser;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcGroupDao extends GenericDao<Long, AcGroup> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    AcGroup findByName(String name);

    List<AcGroup> findAll();

    List<AcGroup> findImmediate(AcPrincipal principal);

    List<AcGroup> findImmediate(AcPrincipal principal, Integer offset, Integer limit);

//    Set<AcGroup> findEffectiveAsNative(AcPrincipal principal);

    List<AcGroup> findAvailableGroups(AcUser user);

    List<AcPrincipal> findAvailableMembers(AcGroup group);

    List<AcPrincipal> findMembers(AcGroup group, AcPrincipalType principalType);

    List<AcPrincipal> findMembers(AcGroup group);

    List<AcPrincipal> findMembers(AcGroup group, Integer offset, Integer limit);

    List<AcGroup> find(String filter, Integer offset, Integer limit);

    List<AcGroup> findMemberships(AcPrincipal principal);

    Integer count(String filter);

    // ====================================================================================================
    // CRUD
    // ====================================================================================================

    void addMember(AcGroup group, AcPrincipal member, AcUser user);

    void addMembers(AcGroup group, List<AcPrincipal> members, AcUser user);

    void removeMember(AcGroup group, AcPrincipal member);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    boolean hasMembership(AcGroup group, AcPrincipal principal);

    boolean isExists(String name);

}
