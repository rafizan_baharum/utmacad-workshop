package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcCurriculum;
import my.utm.acad.ac.core.model.AcProgram;
import my.utm.acad.ac.core.model.AcSubject;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class AcCurriculumImpl implements AcCurriculum {

    private Long id;
    private String code;
    private String description;
    private AcProgram program;
    private List<AcSubject> subjects;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public AcProgram getProgram() {
        return program;
    }

    @Override
    public void setProgram(AcProgram program) {
        this.program = program;
    }

    @Override
    public List<AcSubject> getSubjects() {
        return subjects;
    }

    @Override
    public void setSubjects(List<AcSubject> subjects) {
        this.subjects = subjects;
    }
}
