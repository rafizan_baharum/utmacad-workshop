package my.utm.acad.ac.core.model;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 18/2/2015.
 */
public interface AcPaymentBatchItem extends AcMetaObject{

    boolean isEligible();

    void setEligible(boolean eligible);

    AcEnrollment getEnrollment();

    void setEnrollment(AcEnrollment enrollment);
}
