package my.utm.acad.ac.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public enum AcAssessmentCategoryType {

    COURSE_WORK, // course work
    EXAM_WORK; // exam work

    public static AcAssessmentCategoryType get(int index) {
        return values()[index];
    }

}
