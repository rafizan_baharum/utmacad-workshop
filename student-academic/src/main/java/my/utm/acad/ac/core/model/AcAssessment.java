package my.utm.acad.ac.core.model;

import java.math.BigDecimal;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcAssessment {

    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);

    Integer getDisplayOrder();

    void setDisplayOrder(Integer displayOrder);

    BigDecimal getTotalWeight();

    void setTotalWeight(BigDecimal totalWeight);

    BigDecimal getTotalScore();

    void setTotalScore(BigDecimal totalScore);

    void setTotalScore(Integer totalScore);

    AcAssessmentType getAssessmentType();

    void setAssessmentType(AcAssessmentType assessmentType);

    AcAssessmentCategoryType getCategoryType();

    void setCategoryType(AcAssessmentCategoryType categoryType);

    AcOffering getOffering();

    void setOffering(AcOffering offering);

    AcAcademicSession getSession();

    void setSession(AcAcademicSession session);
}
