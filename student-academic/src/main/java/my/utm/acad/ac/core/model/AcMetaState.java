package my.utm.acad.ac.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public enum AcMetaState {
    INACTIVE, // 0
    ACTIVE,   // 1

}
