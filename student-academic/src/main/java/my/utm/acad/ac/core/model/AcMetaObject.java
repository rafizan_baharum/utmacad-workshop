package my.utm.acad.ac.core.model;

import java.io.Serializable;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcMetaObject extends Serializable {

    Long getId();

    void setId(Long id);

    AcMetadata getMetadata();

    void setMetadata(AcMetadata metadata);
}
