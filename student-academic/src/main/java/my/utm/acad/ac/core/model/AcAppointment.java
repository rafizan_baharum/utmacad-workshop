package my.utm.acad.ac.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */

public interface AcAppointment extends AcObject{

    AcStaff getStaff();

    void setStaff(AcStaff staff);

    AcSection getSection();

    void setSection(AcSection section);

}
