package my.utm.acad.ac.core.dao;

import my.utm.acad.ac.core.model.AcAcademicSession;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 18/2/2015.
 */
public interface AcAcademicSessionDao extends GenericDao<Long, AcAcademicSession> {

    AcAcademicSession findCurrent();

}
