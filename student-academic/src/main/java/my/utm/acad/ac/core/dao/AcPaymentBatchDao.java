package my.utm.acad.ac.core.dao;

import my.utm.acad.ac.core.model.AcAcademicSession;
import my.utm.acad.ac.core.model.AcPaymentBatch;
import my.utm.acad.ac.core.model.AcPaymentBatchItem;

import java.util.List;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public interface AcPaymentBatchDao extends GenericDao<Long, AcPaymentBatch> {

    // ====================================================================================================
    // HELPER
    // ====================================================================================================
    List<AcPaymentBatch> find(AcAcademicSession session);

    List<AcPaymentBatchItem> findItems(AcPaymentBatch batch);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================


    // ====================================================================================================
    // HELPER
    // ====================================================================================================

}
