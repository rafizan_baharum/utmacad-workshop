package my.utm.acad.ac.core.dao.impl;

import my.utm.acad.ac.core.dao.AcPrincipalDao;
import my.utm.acad.ac.core.dao.GenericDaoSupport;
import my.utm.acad.ac.core.model.AcPrincipal;
import my.utm.acad.ac.core.model.AcPrincipalType;
import my.utm.acad.ac.core.model.impl.AcPrincipalImpl;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Repository("acPrincipalDao")
public class AcPrincipalDaoImpl extends GenericDaoSupport<Long, AcPrincipal> implements AcPrincipalDao {

    private static final Logger LOG = Logger.getLogger(AcPrincipalDaoImpl.class);

    public AcPrincipalDaoImpl() {
        super(AcPrincipalImpl.class);
    }

    @Override
    public List<AcPrincipal> findAllPrincipals() {
        Session session = sessionFactory.getCurrentSession();
        List<AcPrincipal> results = new ArrayList<AcPrincipal>();
        Query query = session.createQuery("select p from AcUser p order by p.name");
        results.addAll((List<AcPrincipal>) query.list());

        Query queryGroup = session.createQuery("select p from AcGroup p order by p.name ");
        results.addAll((List<AcPrincipal>) queryGroup.list());

        return results;
    }

    @Override
    public List<AcPrincipal> find(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select p from AcPrincipal p where p.name like :filter order by p.name");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        return query.list();
    }

    @Override
    public List<AcPrincipal> find(String filter, AcPrincipalType type) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select p from AcPrincipal p where " +
                "p.name like :filter " +
                "and p.principalType = :principalType " +
                "order by p.name");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setInteger("principalType", type.ordinal());
        return query.list();
    }

    @Override
    public List<AcPrincipal> find(String filter, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select p from AcPrincipal p where " +
                "p.id in (" +
                "select u.id from AcUser u where " +
                "(upper(u.name) like upper(:filter))" +
                ") " +
                "or p.id in (select g.id from AcGroup g  where upper(g.name) like upper(:filter)) " +
                "order by p.name");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return query.list();
    }

    @Override
    public Integer count(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(u) from AcPrincipal u where " +
                "u.name like :filter ");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public AcPrincipal findByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select p from AcPrincipal p where " +
                "upper(p.name) = upper(:name) ");
        query.setString("name", name);
        return (AcPrincipal) query.uniqueResult();
    }
}
