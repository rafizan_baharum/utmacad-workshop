package my.utm.acad.ac.core.model;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public enum AcAdmissionStatus {
    FULL_TIME,
    PART_TIME;
}
