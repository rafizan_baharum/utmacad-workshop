package my.utm.acad.ac.core.model;

import java.math.BigDecimal;

/**
 * @author team utmacad
 * @since 8/2/2015.
 */
public interface AcSponsor {

    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);

    BigDecimal getCpaRequirement();

    void setCpaRequirement(BigDecimal eligibility);
}
