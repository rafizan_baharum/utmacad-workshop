package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcFaculty;
import my.utm.acad.ac.core.model.AcMetadata;
import my.utm.acad.ac.core.model.AcProgram;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class AcProgramImpl implements AcProgram {

    private Long id;
    private String code;
    private String prefixCode;
    private String description;
    private AcFaculty faculty;
    private AcMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getPrefixCode() {
        return prefixCode;
    }

    @Override
    public void setPrefixCode(String prefixCode) {
        this.prefixCode = prefixCode;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public AcFaculty getFaculty() {
        return faculty;
    }

    @Override
    public void setFaculty(AcFaculty faculty) {
        this.faculty = faculty;
    }

    @Override
    public AcMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(AcMetadata metadata) {
        this.metadata = metadata;
    }
}
