package my.utm.acad.ac.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public enum AcAcademicClassification {
    LEVEL_100,
    LEVEL_200,
    LEVEL_300,
    LEVEL_400,
    GRADUATED,
}
