package my.utm.acad.ac.core.model;

public interface AcUser extends AcPrincipal {

    String getRealName();

    void setRealName(String realName);

    String getPassword();

    void setPassword(String password);

    AcActor getActor();

    void setActor(AcActor actor);
}
