package my.utm.acad.ac.core.model;

import java.math.BigDecimal;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 19/2/2015.
 */
public interface AcCpa extends AcMetaObject {

    BigDecimal getValue();

    void setValue(BigDecimal value);

    AcStudent getStudent();

    void setStudent(AcStudent student);

    AcAcademicSession getSession();

    void setSession(AcAcademicSession session);
}
