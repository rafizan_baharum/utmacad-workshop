package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcFaculty;
import my.utm.acad.ac.core.model.AcStaff;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "AcStaff")
@Table(name = "AC_STAF")
public class AcStaffImpl extends AcActorImpl implements AcStaff {

    @ManyToOne(targetEntity = AcFacultyImpl.class)
    @JoinColumn(name = "FACULTY_ID")
    private AcFaculty faculty;

    @Override
    public AcFaculty getFaculty() {
        return faculty;
    }

    @Override
    public void setFaculty(AcFaculty faculty) {
        this.faculty = faculty;
    }
}
