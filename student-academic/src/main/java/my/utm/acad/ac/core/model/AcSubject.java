package my.utm.acad.ac.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface AcSubject extends AcObject {

    AcSubjectType getSubjectType();

    void setSubjectType(AcSubjectType subjectType);

    AcCourse getCourse();

    void setCourse(AcCourse course);

    AcCurriculum getCurriculum();

    void setCurriculum(AcCurriculum curriculum);
}
