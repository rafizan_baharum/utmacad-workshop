package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcDepartment;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class AcDepartmentImpl implements AcDepartment {

    private Long id;
    private String code;
    private String prefixCode;
    private String description;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getPrefixCode() {
        return prefixCode;
    }

    @Override
    public void setPrefixCode(String prefixCode) {
        this.prefixCode = prefixCode;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }
}
