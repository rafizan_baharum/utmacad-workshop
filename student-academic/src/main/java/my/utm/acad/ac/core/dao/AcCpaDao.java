package my.utm.acad.ac.core.dao;

import my.utm.acad.ac.core.model.AcAcademicSession;
import my.utm.acad.ac.core.model.AcCpa;
import my.utm.acad.ac.core.model.AcStudent;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 19/2/2015.
 */
public interface AcCpaDao {

    AcCpa find(AcAcademicSession session, AcStudent student);
}
