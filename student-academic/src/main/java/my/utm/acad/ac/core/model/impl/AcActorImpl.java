package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcActor;
import my.utm.acad.ac.core.model.AcCampus;
import my.utm.acad.ac.core.model.AcMetadata;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "AcActor")
@Table(name = "AC_ACTR")
@Inheritance(strategy = InheritanceType.JOINED)
public class AcActorImpl implements AcActor {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_AC_ACTR")
    @SequenceGenerator(name = "SEQ_AC_ACTR", sequenceName = "SEQ_AC_ACTR", allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "NAME", nullable = false)
    private String name;

    @NotNull
    @Column(name = "PRIMARY_EMAIL")
    private String primaryEmail;

    @NotNull
    @Column(name = "SECONDARY_EMAIL")
    private String secondaryEmail;

    @NotNull
    @Column(name = "IDENTITY_NO", nullable = false)
    private String identityNo;

    @NotNull
    @Column(name = "PASSPORT_NO", nullable = false)
    private String passportNo;

    @NotNull
    @Column(name = "NRIC_NO", nullable = false)
    private String nricNo;

    @ManyToOne(targetEntity = AcCampusImpl.class)
    @JoinColumn(name = "CAMPUS_ID")
    private AcCampus campus;

    @Embedded
    private AcMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getPrimaryEmail() {
        return primaryEmail;
    }

    @Override
    public void setPrimaryEmail(String primaryEmail) {
        this.primaryEmail = primaryEmail;
    }

    public String getSecondaryEmail() {
        return secondaryEmail;
    }

    public void setSecondaryEmail(String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    @Override
    public String getPassportNo() {
        return passportNo;
    }

    @Override
    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    @Override
    public String getNricNo() {
        return nricNo;
    }

    @Override
    public void setNricNo(String nricNo) {
        this.nricNo = nricNo;
    }

    @Override
    public AcCampus getCampus() {
        return campus;
    }

    @Override
    public void setCampus(AcCampus campus) {
        this.campus = campus;
    }

    public AcMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(AcMetadata metadata) {
        this.metadata = metadata;
    }
}
