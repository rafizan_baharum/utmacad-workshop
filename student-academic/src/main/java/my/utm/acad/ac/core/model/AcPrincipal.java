package my.utm.acad.ac.core.model;

public interface AcPrincipal extends AcMetaObject {

    String getName();

    void setName(String name);

    AcPrincipalType getPrincipalType();

    void setPrincipalType(AcPrincipalType type);
}
