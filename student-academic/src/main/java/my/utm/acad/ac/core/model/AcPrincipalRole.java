package my.utm.acad.ac.core.model;

import my.utm.acad.in.core.model.AcRole;

public interface AcPrincipalRole {

    AcPrincipal getPrincipal();

    AcRole getRole();

    void setPrincipal(AcPrincipal principal);

    void setRole(AcRole role);
}
