package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcEnrollment;
import my.utm.acad.ac.core.model.AcEnrollmentStatus;
import my.utm.acad.ac.core.model.AcSection;
import my.utm.acad.ac.core.model.AcStudent;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class AcEnrollmentImpl implements AcEnrollment {

    private Long id;
    private AcEnrollmentStatus enrollmentStatus;
    private AcStudent student;
    private AcSection section;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AcEnrollmentStatus getEnrollmentStatus() {
        return enrollmentStatus;
    }

    public void setEnrollmentStatus(AcEnrollmentStatus enrollmentStatus) {
        this.enrollmentStatus = enrollmentStatus;
    }

    @Override
    public AcStudent getStudent() {
        return student;
    }

    @Override
    public void setStudent(AcStudent student) {
        this.student = student;
    }

    @Override
    public AcSection getSection() {
        return section;
    }

    @Override
    public void setSection(AcSection section) {
        this.section = section;
    }
}
