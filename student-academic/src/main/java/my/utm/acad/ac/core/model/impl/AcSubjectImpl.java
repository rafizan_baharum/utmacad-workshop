package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcCourse;
import my.utm.acad.ac.core.model.AcCurriculum;
import my.utm.acad.ac.core.model.AcSubject;
import my.utm.acad.ac.core.model.AcSubjectType;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class AcSubjectImpl implements AcSubject {

    private Long id;
    private AcSubjectType subjectType;
    private AcCourse course;
    private AcCurriculum curriculum;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public AcSubjectType getSubjectType() {
        return subjectType;
    }

    @Override
    public void setSubjectType(AcSubjectType subjectType) {
        this.subjectType = subjectType;
    }

    public AcCourse getCourse() {
        return course;
    }

    public void setCourse(AcCourse course) {
        this.course = course;
    }

    @Override
    public AcCurriculum getCurriculum() {
        return curriculum;
    }

    @Override
    public void setCurriculum(AcCurriculum curriculum) {
        this.curriculum = curriculum;
    }
}
