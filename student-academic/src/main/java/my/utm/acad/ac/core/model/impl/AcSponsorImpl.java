package my.utm.acad.ac.core.model.impl;

import my.utm.acad.ac.core.model.AcSponsor;

import java.math.BigDecimal;

/**
 * @author team utmacad
 * @since 8/2/2015.
 */
public class AcSponsorImpl implements AcSponsor {

    private Long id;
    private String code;
    private String description;
    private BigDecimal cpaRequirement;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public BigDecimal getCpaRequirement() {
        return cpaRequirement;
    }

    @Override
    public void setCpaRequirement(BigDecimal cpaRequirement) {
        this.cpaRequirement = cpaRequirement;
    }
}
