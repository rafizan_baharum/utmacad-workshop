package my.utm.acad.ac.core.model;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 19/2/2015.
 */
public enum AcSponsorshipTier {
    PRIMARY,
    SECONDARY;
}
