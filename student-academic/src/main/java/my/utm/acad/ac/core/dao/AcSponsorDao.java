package my.utm.acad.ac.core.dao;

import my.utm.acad.ac.core.model.AcSponsor;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 18/2/2015.
 */
public interface AcSponsorDao extends GenericDao<Long, AcSponsor> {

}
