package my.utm.acad.in.core.model;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface InTypeOneIntakeCriteria extends InIntakeCriteria {

    InCriteriaSubject getSubject();

    void setSubject(InCriteriaSubject subject);

    InCriteriaOperator getOperator();

    void setOperator(InCriteriaOperator operator);

    InCriteriaGrade getGrade();

    void setGrade(InCriteriaGrade grade);
}
