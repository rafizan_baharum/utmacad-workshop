package my.utm.acad.in.core.model;

import java.io.Serializable;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface InMetaObject extends Serializable {

    Long getId();

    void setId(Long id);

    InMetadata getMetadata();

    void setMetadata(InMetadata metadata);
}
