package my.utm.acad.in.core.dao;

import my.utm.acad.in.core.model.*;

import java.util.List;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public interface InIntakePolicyDao extends GenericDao<Long, InIntakePolicy> {

    boolean isFulfilled(InCriteriaSubject subject, InCriteriaOperator operator, InCriteriaGrade grade, InIntakeApplication application);

    InIntakePolicy findBySessionAndProgramCode(InIntakeSession session, InProgramCode programCode);

    List<InIntakePolicy> findByProgramCode(InProgramCode programCode);

    List<InIntakeCriteria> findCriterias(InIntakePolicy policy);
}
