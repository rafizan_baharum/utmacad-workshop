package my.utm.acad.in.core.model;

public enum InAddressType {

    MAILING,
    BILLING,
    OFFICIAL;
}
