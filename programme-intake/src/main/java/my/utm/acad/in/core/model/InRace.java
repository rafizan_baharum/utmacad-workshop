package my.utm.acad.in.core.model;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public enum InRace {

    MELAYU,
    CINA,
    INDIA,
    BUMIPUTERA,
    OTHERS;
}
