package my.utm.acad.in.core.dao;

import my.utm.acad.in.core.model.InCampusCode;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface InCampusCodeDao extends GenericDao<Long, InCampusCode> {

    InCampusCode findByCode(String code);

    List<InCampusCode> find();

    List<InCampusCode> find(String filter, Integer offset, Integer limit);

    Integer count(String filter);

    boolean isExists(String code);
}
