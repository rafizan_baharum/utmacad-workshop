package my.utm.acad.in.core.model;

/**
 * A+,A,A-,B+,B,B-,C+,C,C-,
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface InCriteriaGrade {

    String getCode();

    void setCode(String code) ;

    Integer getOrdinal();

    void setOrdinal(Integer ordinal);

    String getDescription();

    void setDescription(String description);

}
