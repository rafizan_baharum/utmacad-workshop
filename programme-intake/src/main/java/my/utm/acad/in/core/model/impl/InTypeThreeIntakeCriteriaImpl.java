package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InCriteriaOperator;
import my.utm.acad.in.core.model.InTypeThreeIntakeCriteria;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class InTypeThreeIntakeCriteriaImpl extends InIntakeCriteriaImpl implements InTypeThreeIntakeCriteria {

    private Double value;

    @Override
    public Double getValue() {
        return value;
    }

    @Override
    public void setValue(Double value) {
        this.value = value;
    }
}
