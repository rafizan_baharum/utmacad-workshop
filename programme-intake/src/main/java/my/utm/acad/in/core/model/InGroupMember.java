package my.utm.acad.in.core.model;

public interface InGroupMember extends InMetaObject {

    InPrincipal getPrincipal();

    void setPrincipal(InPrincipal principal);

    InGroup getGroup();

    void setGroup(InGroup group);
}
