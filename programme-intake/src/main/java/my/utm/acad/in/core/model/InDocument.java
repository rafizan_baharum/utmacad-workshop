package my.utm.acad.in.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface InDocument {

    String getReferenceNo();

    void setReferenceNo(String referenceNo);

    String getSourceNo();

    void setSourceNo(String sourceNo);

    String getDescription();

    void setDescription(String description);
}
