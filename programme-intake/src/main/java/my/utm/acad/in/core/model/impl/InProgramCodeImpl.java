package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InProgramCode;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class InProgramCodeImpl implements InProgramCode {

    private Long id;
    private String code;
    private String prefixCode;
    private String description;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getPrefixCode() {
        return prefixCode;
    }

    @Override
    public void setPrefixCode(String prefixCode) {
        this.prefixCode = prefixCode;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }
}
