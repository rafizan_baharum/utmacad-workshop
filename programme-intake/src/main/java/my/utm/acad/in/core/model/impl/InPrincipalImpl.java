package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InMetadata;
import my.utm.acad.in.core.model.InPrincipal;
import my.utm.acad.in.core.model.InPrincipalType;

import javax.persistence.*;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "InPrincipal")
@Table(name = "IN_PCPL")
@Inheritance(strategy = InheritanceType.JOINED)
public class InPrincipalImpl implements InPrincipal {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_IN_PCPL")
    @SequenceGenerator(name = "SEQ_IN_PCPL", sequenceName = "SEQ_IN_PCPL", allocationSize = 1)
    private Long id;

    @Column(name = "NAME", unique = true, nullable = false)
    private String name;

    @Column(name = "PRINCIPAL_TYPE")
    private InPrincipalType principalType;

    @Embedded
    private InMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public InPrincipalType getPrincipalType() {
        return principalType;
    }

    @Override
    public void setPrincipalType(InPrincipalType principalType) {
        this.principalType = principalType;
    }

    @Override
    public InMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(InMetadata metadata) {
        this.metadata = metadata;
    }
}
