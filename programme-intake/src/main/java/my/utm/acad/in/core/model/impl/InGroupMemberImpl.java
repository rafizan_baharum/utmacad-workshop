package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InGroup;
import my.utm.acad.in.core.model.InGroupMember;
import my.utm.acad.in.core.model.InMetadata;
import my.utm.acad.in.core.model.InPrincipal;

import javax.persistence.*;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "InGroupMember")
@Table(name = "AC_GROP_MMBR")
public class InGroupMemberImpl implements InGroupMember {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_AC_GROP_MMBR")
    @SequenceGenerator(name = "SEQ_AC_GROP_MMBR", sequenceName = "SEQ_AC_GROP_MMBR", allocationSize = 1)
    private Long id;

    @OneToOne(targetEntity = InGroupImpl.class)
    @JoinColumn(name = "GROUP_ID")
    private InGroup group;

    @OneToOne(targetEntity = InPrincipalImpl.class)
    @JoinColumn(name = "PRINCIPAL_ID")
    private InPrincipal principal;

    @Embedded
    private InMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public InGroup getGroup() {
        return group;
    }

    @Override
    public void setGroup(InGroup group) {
        this.group = group;
    }

    @Override
    public InPrincipal getPrincipal() {
        return principal;
    }

    @Override
    public void setPrincipal(InPrincipal principal) {
        this.principal = principal;
    }

    @Override
    public InMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(InMetadata metadata) {
        this.metadata = metadata;
    }
}
