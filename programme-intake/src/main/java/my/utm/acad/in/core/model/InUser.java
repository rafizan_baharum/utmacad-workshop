package my.utm.acad.in.core.model;

public interface InUser extends InPrincipal {

    String getRealName();

    void setRealName(String realName);

    String getPassword();

    void setPassword(String password);

    InActor getActor();

    void setActor(InActor actor);
}
