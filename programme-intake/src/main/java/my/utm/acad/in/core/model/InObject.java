package my.utm.acad.in.core.model;

/**
 * @author rafizan.baharum
 * @since 26/1/2015
 */
public interface InObject {

    Long getId();
}
