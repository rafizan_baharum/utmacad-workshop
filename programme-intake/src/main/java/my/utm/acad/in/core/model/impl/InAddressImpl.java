package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.*;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class InAddressImpl implements InAddress {

    private Long id;
    private String address1;
    private String address2;
    private String address3;
    private String postCode;

    private InAddressType type;
    private InStateCode stateCode;
    private InCountryCode countryCode;
    private InIntakeApplication application;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getAddress1() {
        return address1;
    }

    @Override
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @Override
    public String getAddress2() {
        return address2;
    }

    @Override
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @Override
    public String getAddress3() {
        return address3;
    }

    @Override
    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    @Override
    public InAddressType getType() {
        return type;
    }

    public void setType(InAddressType type) {
        this.type = type;
    }

    @Override
    public String getPostCode() {
        return postCode;
    }

    @Override
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Override
    public InStateCode getStateCode() {
        return stateCode;
    }

    @Override
    public void setStateCode(InStateCode stateCode) {
        this.stateCode = stateCode;
    }

    @Override
    public InCountryCode getCountryCode() {
        return countryCode;
    }

    @Override
    public void setCountryCode(InCountryCode countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public InIntakeApplication getApplication() {
        return application;
    }

    @Override
    public void setApplication(InIntakeApplication application) {
        this.application = application;
    }
}
