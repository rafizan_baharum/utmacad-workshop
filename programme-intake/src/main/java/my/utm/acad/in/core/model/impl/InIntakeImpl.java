package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.*;

import javax.persistence.Entity;
import java.util.Date;
import java.util.List;

/**
 * @author team utamacad
 * @since 1/2/2015.
 */
@Entity(name = "InIntake")
public class InIntakeImpl implements InIntake {

    private Long id;
    private String referenceNo; // programCode + session  SCK/201420151
    private Date startDate;
    private Date endDate;
    private Integer projection;
    private InIntakeSession session;
    private InIntakeCategory category;
    private List<InIntakeApplication> applications;
    private List<InIntakePolicy> admissionPolicies;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Override
    public Date getEndDate() {
        return endDate;
    }

    @Override
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public Integer getProjection() {
        return projection;
    }

    @Override
    public void setProjection(Integer projection) {
        this.projection = projection;
    }

    @Override
    public InIntakeSession getSession() {
        return session;
    }

    @Override
    public void setSession(InIntakeSession session) {
        this.session = session;
    }

    @Override
    public InIntakeCategory getCategory() {
        return category;
    }

    @Override
    public void setCategory(InIntakeCategory category) {
        this.category = category;
    }

    @Override
    public List<InIntakeApplication> getApplications() {
        return applications;
    }

    public void setApplications(List<InIntakeApplication> applications) {
        this.applications = applications;
    }

    public List<InIntakePolicy> getAdmissionPolicies() {
        return admissionPolicies;
    }

    public void setAdmissionPolicies(List<InIntakePolicy> admissionPolicies) {
        this.admissionPolicies = admissionPolicies;
    }
}
