package my.utm.acad.in.core.model;

/**
 * FT FULL TIME
 * PT PART TIME
 *
 * @author rafizan.baharum
 * @since 30/1/2015
 */
public interface InProgramCode extends InObject {

    String getCode();

    void setCode(String code);

    String getPrefixCode();

    void setPrefixCode(String prefixCode);

    String getDescription();

    void setDescription(String description);
}
