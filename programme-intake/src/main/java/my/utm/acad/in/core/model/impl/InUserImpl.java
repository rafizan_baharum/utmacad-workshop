package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InActor;
import my.utm.acad.in.core.model.InUser;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "InUser")
@Table(name = "IN_USER")
public class InUserImpl extends InPrincipalImpl implements InUser {

    @NotNull
    @Column(name = "REAL_NAME")
    private String realName;

    @NotNull
    @Column(name = "PASSWORD")
    private String password;

    @OneToOne(targetEntity = InActorImpl.class)
    @JoinColumn(name = "ACTOR_ID")
    private InActor actor;

    @Override
    public String getRealName() {
        return realName;
    }

    @Override
    public void setRealName(String realName) {
        this.realName = realName;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public InActor getActor() {
        return actor;
    }

    @Override
    public void setActor(InActor actor) {
        this.actor = actor;
    }
}
