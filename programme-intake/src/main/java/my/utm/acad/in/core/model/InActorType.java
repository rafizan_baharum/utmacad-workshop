package my.utm.acad.in.core.model;

public enum InActorType {

    CANDIDATE,
    SECRETARIAT,
}
