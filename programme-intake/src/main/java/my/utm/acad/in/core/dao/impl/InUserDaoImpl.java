package my.utm.acad.in.core.dao.impl;

import my.utm.acad.in.core.dao.GenericDaoSupport;
import my.utm.acad.in.core.dao.InUserDao;
import my.utm.acad.in.core.model.*;
import my.utm.acad.in.core.model.impl.InUserImpl;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Repository("inUserDao")
public class InUserDaoImpl extends GenericDaoSupport<Long, InUser> implements InUserDao {

    private static final Logger LOG = Logger.getLogger(InUserDaoImpl.class);

    public InUserDaoImpl() {
        super(InUserImpl.class);
    }

    // =============================================================================
    // FINDER METHODS
    // =============================================================================

    @Override
    public List<InGroup> findGroups(InUser user) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select r from InGroup r inner join r.members m where m.id = :id");
        query.setLong("id", user.getId());
        return (List<InGroup>) query.list();
    }

    @Override
    public InUser findByEmail(String email) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select u from InUser u where u.email = :email ");
        query.setString("email", email);
        return (InUser) query.uniqueResult();
    }

    @Override
    public InUser findByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select u from InUser u where u.name = :username ");
        query.setString("username", username);
        return (InUser) query.uniqueResult();
    }

    @Override
    public InUser findByActor(InActor actor) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select u from InUser u where u.actor = :actor ");
        query.setEntity("actor", actor);
        return (InUser) query.uniqueResult();
    }

    @Override
    public List<InUser> find(String filter, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from InUser s where (" +
                "upper(s.name) like upper(:filter) " +
                "or upper(s.firstName) like upper(:filter) " +
                "or upper(s.lastName) like upper(:filter)) " +
                "order by s.firstName, s.lastName, s.name");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return (List<InUser>) query.list();
    }

    @Override
    public Integer count() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(u) from InUser u");
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public Integer count(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(s) from InUser s where " +
                "upper(s.name) like upper(:filter) " +
                "or upper(s.firstName) like upper(:filter) " +
                "or upper(s.lastName) like upper(:filter)");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public void save(InUser user) {
        Validate.notNull(user, "User cannot be null");
        try {
            Session session = sessionFactory.getCurrentSession();
            InMetadata metadata = new InMetadata();
            metadata.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            metadata.setCreatorId(0L);
            metadata.setState(InMetaState.ACTIVE);
            user.setMetadata(metadata);
            session.save(user);
        } catch (HibernateException e) {
            LOG.debug("error occurred", e);
        }
    }

    // =============================================================================
    // HELPER
    // =============================================================================

    @Override
    public boolean isExists(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(*) from InUser u where " +
                "upper(u.name) = upper(:username) ");
        query.setString("username", username);
        return 0 < ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public boolean hasUser(InActor actor) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(*) from InUser u where " +
                "u.actor = :actor");
        query.setEntity("actor", actor);
        return 0 < ((Long) query.uniqueResult()).intValue();
    }

}
