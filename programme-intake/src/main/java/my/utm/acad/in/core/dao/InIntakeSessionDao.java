package my.utm.acad.in.core.dao;

import my.utm.acad.in.core.model.InIntakeSession;
import my.utm.acad.in.core.model.InProgramCode;

import java.util.List;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public interface InIntakeSessionDao extends GenericDao<Long, InIntakeSession> {

    List<InProgramCode> findOfferedProgramCodes(InIntakeSession session);
}
