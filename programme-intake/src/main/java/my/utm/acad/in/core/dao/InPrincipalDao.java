package my.utm.acad.in.core.dao;

import my.utm.acad.in.core.model.InPrincipal;
import my.utm.acad.in.core.model.InPrincipalType;

import java.util.List;

public interface InPrincipalDao extends GenericDao<Long, InPrincipal> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    InPrincipal findByName(String name);

    List<InPrincipal> findAllPrincipals();

    List<InPrincipal> find(String filter);

    List<InPrincipal> find(String filter, InPrincipalType type);

    List<InPrincipal> find(String filter, Integer offset, Integer limit);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(String filter);
}
