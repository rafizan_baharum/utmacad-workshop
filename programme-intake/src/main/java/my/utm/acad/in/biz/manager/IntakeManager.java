package my.utm.acad.in.biz.manager;

import my.utm.acad.in.core.model.InIntake;
import my.utm.acad.in.core.model.InIntakeApplication;
import my.utm.acad.in.core.model.InProgramCode;

import java.util.List;

/**
 * @author rafizan.baharum
 * @since 26/1/2015
 */
public interface IntakeManager {

    void processIntake(InIntake intake, InIntakeApplication application);

    List<InProgramCode> findValidChoices(InIntake intake, InIntakeApplication application);
}
