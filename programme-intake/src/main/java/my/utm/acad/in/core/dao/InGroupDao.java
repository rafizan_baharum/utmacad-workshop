package my.utm.acad.in.core.dao;


import my.utm.acad.in.core.model.InGroup;
import my.utm.acad.in.core.model.InPrincipal;
import my.utm.acad.in.core.model.InPrincipalType;
import my.utm.acad.in.core.model.InUser;

import java.util.List;

public interface InGroupDao extends GenericDao<Long, InGroup> {


    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    InGroup findByName(String name);

    List<InGroup> findAll();

    List<InGroup> findImmediate(InPrincipal principal);

    List<InGroup> findImmediate(InPrincipal principal, Integer offset, Integer limit);

//    Set<InGroup> findEffectiveAsNative(InPrincipal principal);

    List<InGroup> findAvailableGroups(InUser user);

    List<InPrincipal> findAvailableMembers(InGroup group);

    List<InPrincipal> findMembers(InGroup group, InPrincipalType principalType);

    List<InPrincipal> findMembers(InGroup group);

    List<InPrincipal> findMembers(InGroup group, Integer offset, Integer limit);

    List<InGroup> find(String filter, Integer offset, Integer limit);

    List<InGroup> findMemberships(InPrincipal principal);

    Integer count(String filter);

    // ====================================================================================================
    // CRUD
    // ====================================================================================================

    void addMember(InGroup group, InPrincipal member, InUser user);

    void addMembers(InGroup group, List<InPrincipal> members, InUser user);

    void removeMember(InGroup group, InPrincipal member);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    boolean hasMembership(InGroup group, InPrincipal principal);

    boolean isExists(String name);
}
