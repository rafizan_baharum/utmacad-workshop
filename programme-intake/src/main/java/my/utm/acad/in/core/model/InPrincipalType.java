package my.utm.acad.in.core.model;

public enum InPrincipalType {
    USER,
    GROUP,
}
