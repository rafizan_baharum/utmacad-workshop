package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InCriteriaGrade;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class InCriteriaGradeImpl implements InCriteriaGrade {

    private String code;
    private String description;
    private Integer ordinal;

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public Integer getOrdinal() {
        return ordinal;
    }

    @Override
    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }
}
