package my.utm.acad.in.core.model;

public interface InIntakeSession extends InObject {

    String getCode();

    void setCode(String code);

    Integer getSemester();

    void getSemester(Integer semester);

    Integer getStartYear();

    void setStartYear(Integer year);

    Integer getEndYear();

    void setEndYear(Integer year);
}
