package my.utm.acad.in.core.dao;

import my.utm.acad.in.core.model.InIntakeSession;
import my.utm.acad.in.core.model.InProgramCode;
import my.utm.acad.in.core.model.impl.InIntakeSessionImpl;

import java.util.List;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 17/2/2015.
 */
public class InIntakeSessionDaoImpl extends GenericDaoSupport<Long, InIntakeSession> implements InIntakeSessionDao {

    public InIntakeSessionDaoImpl() {
        super(InIntakeSessionImpl.class);
    }


    @Override
    public List<InProgramCode> findOfferedProgramCodes(InIntakeSession session) {
        return null;
    }
}
