package my.utm.acad.in.core.model;

public interface InPrincipal extends InMetaObject {

    String getName();

    void setName(String name);

    InPrincipalType getPrincipalType();

    void setPrincipalType(InPrincipalType type);
}
