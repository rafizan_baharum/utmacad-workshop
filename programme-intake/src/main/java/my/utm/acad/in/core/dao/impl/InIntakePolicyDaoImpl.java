package my.utm.acad.in.core.dao.impl;

import my.utm.acad.in.core.dao.GenericDaoSupport;
import my.utm.acad.in.core.dao.InIntakePolicyDao;
import my.utm.acad.in.core.model.*;
import my.utm.acad.in.core.model.impl.InIntakePolicyImpl;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
@Repository
public class InIntakePolicyDaoImpl extends GenericDaoSupport<Long, InIntakePolicy> implements InIntakePolicyDao {

    public InIntakePolicyDaoImpl() {
        super(InIntakePolicyImpl.class);
    }

    @Override
    public boolean isFulfilled(InCriteriaSubject subject, InCriteriaOperator operator, InCriteriaGrade grade, InIntakeApplication application) {
        return true;
    }

    @Override
    public InIntakePolicy findBySessionAndProgramCode(InIntakeSession intakeSession, InProgramCode programCode) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select c from InIntakePolicy c where " +
                "c.programCode = :programCode " +
                "and c.session = :session " +
                "and c.metadata.state = :state");
        query.setEntity("programCode", programCode);
        query.setCacheable(true);
        query.setInteger("state", InMetaState.ACTIVE.ordinal());
        return (InIntakePolicy) query.uniqueResult();
    }

    @Override
    public List<InIntakePolicy> findByProgramCode(InProgramCode programCode) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select c from InIntakePolicy c where " +
                "c.programCode = :programCode " +
                "and c.metadata.state = :state");
        query.setEntity("programCode", programCode);
        query.setCacheable(true);
        query.setInteger("state", InMetaState.ACTIVE.ordinal());
        return (List<InIntakePolicy>) query.list();
    }

    @Override
    public List<InIntakeCriteria> findCriterias(InIntakePolicy policy) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select c from InIntakeCriteria c where " +
                "c.policy = :policy " +
                "and c.metadata.state = :state");
        query.setEntity("policy", policy);
        query.setCacheable(true);
        query.setInteger("state", InMetaState.ACTIVE.ordinal());
        return (List<InIntakeCriteria>) query.list();
    }
}
