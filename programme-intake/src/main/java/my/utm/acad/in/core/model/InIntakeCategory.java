package my.utm.acad.in.core.model;

/**
 * SPM/STPM/O-LEVEL
 * STPM/A-LEVEL
 * DIPLOMA
 * FOUNDATION
 * DEGREE
 * INTERNATIONAL BACCAULERATE
 * OTHERS
 *
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface InIntakeCategory {

    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);

}
