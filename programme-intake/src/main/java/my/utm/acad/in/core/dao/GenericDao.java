package my.utm.acad.in.core.dao;


import my.utm.acad.in.core.model.InUser;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface GenericDao<K, I> {

    I newInstance();

    I refresh(I i);

    I findById(K k);

    List<I> find(Integer offset, Integer limit);

    Integer count();

    void save(I entity, InUser user);

    void saveOrUpdate(I entity, InUser user);

    void update(I entity, InUser user);

    void deactivate(I entity, InUser user);

    void remove(I entity, InUser user);

    void delete(I entity, InUser user);

//    NOTE: available after workshop Spring Security
//    List<I> findAuthorized(Set<String> sids);
//
//    List<I> findAuthorized(Set<String> sids, Integer offset, Integer limit);
//
//    List<Long> findAuthorizedIds(Set<String> sids);

//    Integer countAuthorized(Set<String> sids);


}
