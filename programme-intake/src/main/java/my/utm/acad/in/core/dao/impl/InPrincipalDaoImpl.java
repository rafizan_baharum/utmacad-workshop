package my.utm.acad.in.core.dao.impl;

import my.utm.acad.in.core.dao.GenericDaoSupport;
import my.utm.acad.in.core.dao.InPrincipalDao;
import my.utm.acad.in.core.model.InPrincipal;
import my.utm.acad.in.core.model.InPrincipalType;
import my.utm.acad.in.core.model.impl.InPrincipalImpl;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Repository("inPrincipalDao")
public class InPrincipalDaoImpl extends GenericDaoSupport<Long, InPrincipal> implements InPrincipalDao {

    private static final Logger LOG = Logger.getLogger(InPrincipalDaoImpl.class);

    public InPrincipalDaoImpl() {
        super(InPrincipalImpl.class);
    }

    @Override
    public List<InPrincipal> findAllPrincipals() {
        Session session = sessionFactory.getCurrentSession();
        List<InPrincipal> results = new ArrayList<InPrincipal>();
        Query query = session.createQuery("select p from InUser p order by p.name");
        results.addAll((List<InPrincipal>) query.list());

        Query queryGroup = session.createQuery("select p from InGroup p order by p.name ");
        results.addAll((List<InPrincipal>) queryGroup.list());

        return results;
    }

    @Override
    public List<InPrincipal> find(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select p from InPrincipal p where p.name like :filter order by p.name");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        return query.list();
    }

    @Override
    public List<InPrincipal> find(String filter, InPrincipalType type) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select p from InPrincipal p where " +
                "p.name like :filter " +
                "and p.principalType = :principalType " +
                "order by p.name");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setInteger("principalType", type.ordinal());
        return query.list();
    }

    @Override
    public List<InPrincipal> find(String filter, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select p from InPrincipal p where " +
                "p.id in (" +
                "select u.id from InUser u where " +
                "(upper(u.name) like upper(:filter)" +
                "or upper(u.name) like upper(:filter))" +
                ") " +
                "or p.id in (select g.id from InGroup g  where upper(g.name) like upper(:filter)) " +
                "order by p.name");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return query.list();
    }

    @Override
    public Integer count(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(u) from InPrincipal u where " +
                "u.name like :filter ");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public InPrincipal findByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select p from InPrincipal p where " +
                "upper(p.name) = upper(:name) ");
        query.setString("name", name);
        return (InPrincipal) query.uniqueResult();
    }
}
