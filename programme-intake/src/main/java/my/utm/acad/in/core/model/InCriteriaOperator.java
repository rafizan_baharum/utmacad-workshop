package my.utm.acad.in.core.model;

/**
 * IS AT LEAST
 *
 * @author team utamacad
 * @since 3/2/2015.
 */
public enum InCriteriaOperator {

    AT_LEAST,
    EQUIVALENT,
    GREATER_THAN;
}
