package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InCriteriaGrade;
import my.utm.acad.in.core.model.InCriteriaOperator;
import my.utm.acad.in.core.model.InCriteriaSubject;
import my.utm.acad.in.core.model.InTypeOneIntakeCriteria;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class InTypeOneIntakeCriteriaImpl extends InIntakeCriteriaImpl implements InTypeOneIntakeCriteria {

    private InCriteriaSubject subject;
    private InCriteriaOperator operator;
    private InCriteriaGrade grade;

    @Override
    public InCriteriaSubject getSubject() {
        return subject;
    }

    @Override
    public void setSubject(InCriteriaSubject subject) {
        this.subject = subject;
    }

    @Override
    public InCriteriaOperator getOperator() {
        return operator;
    }

    @Override
    public void setOperator(InCriteriaOperator operator) {
        this.operator = operator;
    }

    @Override
    public InCriteriaGrade getGrade() {
        return grade;
    }

    @Override
    public void setGrade(InCriteriaGrade grade) {
        this.grade = grade;
    }
}
