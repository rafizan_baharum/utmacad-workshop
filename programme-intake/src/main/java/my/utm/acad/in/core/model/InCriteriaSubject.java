package my.utm.acad.in.core.model;

/**
 * @author team utamacad
 * @since 1/2/2015.
 */
public interface InCriteriaSubject {

    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);
}
