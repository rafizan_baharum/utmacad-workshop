package my.utm.acad.in.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public enum InMetaState {
    INACTIVE, // 0
    ACTIVE,   // 1

}
