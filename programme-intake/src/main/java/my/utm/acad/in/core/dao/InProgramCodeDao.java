package my.utm.acad.in.core.dao;

import my.utm.acad.in.core.model.InIntakeSession;
import my.utm.acad.in.core.model.InProgramCode;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public interface InProgramCodeDao {

    InProgramCode findByIntakeSession(InIntakeSession session);
}
