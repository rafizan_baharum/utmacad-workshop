package my.utm.acad.in.core.model;

import java.util.List;

public interface InIntakeApplication extends InDocument {

    String getName();

    void setName(String name);

    String getNricNo();

    void setNricNo(String nricNo);

    String getPassportNo();

    void setPassportNo(String passportNo);

    boolean isNationality();

    void setNationality(boolean nationality);

    String getSchoolName();

    void setSchoolName(String schoolName);

    InSchoolType getSchoolType();

    void setSchoolType(InSchoolType schoolType);

    Integer getSchoolBatch();

    void setSchoolBatch(Integer schoolBatch);

    InCampusCode getCampusCode();

    void setCampusCode(InCampusCode campus);

    Integer getAge();

    void setAge(Integer age);

    InRace getRace();

    void setRace(InRace race);

    InReligion getReligion();

    void setReligion(InReligion religion);

    InIntake getIntake();

    void setIntake(InIntake intake);

    InGender getGender();

    void setGender(InGender gender);

    List<InAddress> getAddresses();

    void setAddresses(List<InAddress> addresses);

    InIntakeCategory getCategory();

    void setCategory(InIntakeCategory level);

    List<InCriteriaResult> getResults();

    void setResults(List<InCriteriaResult> results);

    List<InProgramCode> getChoices();

    void setChoices(List<InProgramCode> programs);

    List<InAttachment> getDocuments();

    void setDocuments(List<InAttachment> documents);
}
