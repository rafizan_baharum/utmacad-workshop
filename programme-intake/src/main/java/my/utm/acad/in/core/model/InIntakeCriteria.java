package my.utm.acad.in.core.model;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface InIntakeCriteria extends InObject {


    InIntakeCategory getCategory();

    void setCategory(InIntakeCategory category);

    InCriteriaOperator getOperator();

    void setOperator(InCriteriaOperator operator);

    InIntakePolicy getPolicy();

    void setPolicy(InIntakePolicy policy);

}
