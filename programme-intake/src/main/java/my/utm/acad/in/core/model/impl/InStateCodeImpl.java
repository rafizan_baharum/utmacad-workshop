package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InStateCode;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class InStateCodeImpl implements InStateCode {

    private Long id;
    private String code;
    private String prefixCode;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    public String getPrefixCode() {
        return prefixCode;
    }

    public void setPrefixCode(String prefixCode) {
        this.prefixCode = prefixCode;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }
}
