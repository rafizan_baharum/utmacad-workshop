package my.utm.acad.in.core.dao;


import my.utm.acad.in.core.model.InActor;
import my.utm.acad.in.core.model.InGroup;
import my.utm.acad.in.core.model.InUser;

import java.util.List;

public interface InUserDao extends GenericDao<Long, InUser> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    InUser findByEmail(String email);

    InUser findByUsername(String username);

    InUser findByActor(InActor actor);

    List<InUser> find(String filter, Integer offset, Integer limit);

    List<InGroup> findGroups(InUser user);

    Integer count(String filter);

    // ====================================================================================================
    // CRUD
    // ====================================================================================================
    void save(InUser user);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    boolean isExists(String username);

    boolean hasUser(InActor actor);
}
