package my.utm.acad.in.core.model;

import java.util.Set;

public interface InGroup extends InPrincipal {

    Set<InGroupMember> getMembers();

    void setMembers(Set<InGroupMember> members);
}
