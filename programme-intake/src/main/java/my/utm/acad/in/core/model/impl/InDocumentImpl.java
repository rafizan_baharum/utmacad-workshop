package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InAttachment;
import my.utm.acad.in.core.model.InIntakeApplication;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class InDocumentImpl implements InAttachment {

    private String name;
    private String url;
    private InIntakeApplication application;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public InIntakeApplication getApplication() {
        return application;
    }

    @Override
    public void setApplication(InIntakeApplication application) {
        this.application = application;
    }
}
