package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InIntakeCategory;
import my.utm.acad.in.core.model.InIntakeCriteria;
import my.utm.acad.in.core.model.InIntakePolicy;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public abstract class InIntakeCriteriaImpl implements InIntakeCriteria {

    private Long id;
    private InIntakeCategory category;
    private InIntakePolicy policy;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public InIntakeCategory getCategory() {
        return category;
    }

    @Override
    public void setCategory(InIntakeCategory category) {
        this.category = category;
    }

    @Override
    public InIntakePolicy getPolicy() {
        return policy;
    }

    @Override
    public void setPolicy(InIntakePolicy policy) {
        this.policy = policy;
    }
}
