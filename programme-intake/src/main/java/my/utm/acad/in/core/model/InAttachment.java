package my.utm.acad.in.core.model;

/**
 * IC, Birth Certificate, SPM, Other Qualification
 *
 * @author team utamacad
 * @since 4/2/2015.
 */
public interface InAttachment {

    String getName();

    void setName(String name);

    String getUrl();

    void setUrl(String url);

    InIntakeApplication getApplication();

    void setApplication(InIntakeApplication application);
}
