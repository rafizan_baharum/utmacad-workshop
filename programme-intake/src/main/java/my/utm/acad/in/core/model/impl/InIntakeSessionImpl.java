package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InIntakeSession;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class InIntakeSessionImpl implements InIntakeSession {

    private Long id;
    private String code;
    private Integer semester;
    private Integer startYear;
    private Integer endYear;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public Integer getSemester() {
        return semester;
    }

    @Override
    public void getSemester(Integer semester) {
        this.semester = semester;
    }

    @Override
    public Integer getStartYear() {
        return startYear;
    }

    @Override
    public void setStartYear(Integer year) {
        this.startYear = year;
    }

    public void setSemester(Integer semester) {
        this.semester = semester;
    }

    @Override
    public Integer getEndYear() {
        return endYear;
    }

    @Override
    public void setEndYear(Integer endYear) {
        this.endYear = endYear;
    }
}
