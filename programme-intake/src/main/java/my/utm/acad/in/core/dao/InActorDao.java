package my.utm.acad.in.core.dao;


import my.utm.acad.in.core.model.InActor;
import my.utm.acad.in.core.model.InActorType;

import java.util.List;

public interface InActorDao extends GenericDao<Long, InActorDao> {

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    InActor findByCode(String code);

    InActor findByIdentityNo(String identityNo);

    List<InActor> find(String filter, Integer offset, Integer limit);

    List<InActor> find(InActorType type, Integer offset, Integer limit);

    List<InActor> find(InActorType type, String filter, Integer offset, Integer limit);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(String filter);

    Integer count(InActorType type);
}
