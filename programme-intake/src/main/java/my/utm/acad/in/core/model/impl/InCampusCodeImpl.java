package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InCampusCode;
import my.utm.acad.in.core.model.InMetadata;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
@Entity(name = "InCampusCode")
@Table(name = "IN_CMPS_CODE")
public class InCampusCodeImpl implements InCampusCode {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_IN_CMPS_CODE")
    @SequenceGenerator(name = "SEQ_IN_CMPS_CODE", sequenceName = "SEQ_IN_CMPS_CODE", allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "CODE", nullable = false)
    private String code;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Embedded
    private InMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    public InMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(InMetadata metadata) {
        this.metadata = metadata;
    }
}
