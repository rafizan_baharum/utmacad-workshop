package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InIntake;
import my.utm.acad.in.core.model.InIntakeCriteria;
import my.utm.acad.in.core.model.InIntakePolicy;
import my.utm.acad.in.core.model.InProgramCode;

import javax.persistence.Entity;
import java.util.List;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
@Entity(name = "InIntakePolicy")
public class InIntakePolicyImpl implements InIntakePolicy {

    private Long id;
    private InIntake intake;
    private InProgramCode programCode;
    private List<InIntakeCriteria> criterias;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InIntake getIntake() {
        return intake;
    }

    public void setIntake(InIntake intake) {
        this.intake = intake;
    }

    @Override
    public InProgramCode getProgramCode() {
        return programCode;
    }

    @Override
    public void setProgramCode(InProgramCode program) {
        this.programCode = program;
    }

    @Override
    public List<InIntakeCriteria> getCriterias() {
        return criterias;
    }

    @Override
    public void setCriterias(List<InIntakeCriteria> criterias) {
        this.criterias = criterias;
    }
}
