package my.utm.acad.in.core.model;

public interface InActor extends InObject {

    String getIdentityNo();

    void setIdentityNo(String identityNo);

    String getName();

    void setName(String name);

    String getPhone();

    void setPhone(String phone);

    String getFax();

    void setFax(String fax);

    String getEmail();

    void setEmail(String email);

    InActorType getActorType();

    void setActorType(InActorType actorType);
}
