package my.utm.acad.in.biz.manager;

import my.utm.acad.in.core.dao.InIntakePolicyDao;
import my.utm.acad.in.core.dao.InIntakeSessionDao;
import my.utm.acad.in.core.model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author team utamacad
 * @since 1/2/2015.
 */
//@Service("intakeManager")
public class IntakeManagerImpl implements IntakeManager {

    //    @Autowired
    private InIntakePolicyDao admissionPolicyDao;

    //    @Autowired
    private InIntakeSessionDao intakeSessionDao;

    @Override
    public void processIntake(InIntake intake, InIntakeApplication application) {
    }

    public List<InProgramCode> findValidChoices(InIntake intake, InIntakeApplication application) {
        // find offered program codes per session
        InIntakeSession intakeSession = intake.getSession();
        List<InProgramCode> validChoices = new ArrayList<InProgramCode>();
        List<InProgramCode> offeredProgramCodes = intakeSessionDao.findOfferedProgramCodes(intakeSession);

        for (InProgramCode programCode : offeredProgramCodes) {
            InIntakePolicy policy = admissionPolicyDao.findBySessionAndProgramCode(intakeSession, programCode);

            // NOTE: success-fast
            // find criteria by application level per intake session
            boolean compoundFulfilled = true;
            List<InIntakeCriteria> criterias = admissionPolicyDao.findCriterias(policy);
            for (InIntakeCriteria criteria : criterias) {
                boolean fulfilled = isFulfilledByCandidate(criteria, application);
                compoundFulfilled = fulfilled && compoundFulfilled;
            }

            // if all is fulfilled
            if (compoundFulfilled)
                validChoices.add(programCode);
        }
        return validChoices;
    }


    private boolean isFulfilledByCandidate(InIntakeCriteria criteria, InIntakeApplication application) {
        boolean fulfilled = false;
        if (criteria instanceof InTypeOneIntakeCriteria) {
            InTypeOneIntakeCriteria typeOne = ((InTypeOneIntakeCriteria) criteria);
            InCriteriaSubject subject = typeOne.getSubject();
            InCriteriaOperator operator = typeOne.getOperator();
            InCriteriaGrade grade = typeOne.getGrade();
            fulfilled = admissionPolicyDao.isFulfilled(subject, operator, grade, application);

        } else if (criteria instanceof InTypeTwoIntakeCriteria) {
            InTypeTwoIntakeCriteria typeTwo = (InTypeTwoIntakeCriteria) criteria;
            InCriteriaGrade grade = typeTwo.getGrade();
            InCriteriaOperator operator = criteria.getOperator();
            List<InCriteriaSubject> includes = typeTwo.getIncludes();
            List<InCriteriaSubject> excludes = typeTwo.getExcludes();
            // TODO: what to do with excludes ??

            for (InCriteriaSubject include : includes) {
                fulfilled = admissionPolicyDao.isFulfilled(include, operator, grade, application);
            }
        } else if (criteria instanceof InTypeThreeIntakeCriteria) {
            InTypeThreeIntakeCriteria typeThree = (InTypeThreeIntakeCriteria) criteria;
            Double value = typeThree.getValue();
            // TODO: where do we keep value in application?
        }
        return fulfilled;
    }
}
