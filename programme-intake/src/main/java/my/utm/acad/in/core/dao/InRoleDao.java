package my.utm.acad.in.core.dao;

import my.utm.acad.in.core.model.InRole;
import my.utm.acad.in.core.model.InUser;

public interface InRoleDao {

    void grant(InUser principal, InRole role, InUser user);

    void revoke(InUser principal, InRole role, InUser user);

    void revokeAll(InUser principal, InUser user);

    void overwrite(InUser principal, InRole role, InUser user);

}
