package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InIntakeApplication;
import my.utm.acad.in.core.model.InCriteriaGrade;
import my.utm.acad.in.core.model.InCriteriaResult;
import my.utm.acad.in.core.model.InCriteriaSubject;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class InCriteriaResultImpl implements InCriteriaResult {

    private InCriteriaSubject subject;
    private InCriteriaGrade grade;

    @Override
    public InCriteriaSubject getSubject() {
        return subject;
    }

    @Override
    public void setSubject(InCriteriaSubject subject) {
        this.subject = subject;
    }

    @Override
    public InCriteriaGrade getGrade() {
        return grade;
    }

    @Override
    public void setGrade(InCriteriaGrade grade) {
        this.grade = grade;
    }

    @Override
    public InIntakeApplication getApplication() {
        return null;
    }

    @Override
    public void setApplication(InIntakeApplication application) {

    }
}
