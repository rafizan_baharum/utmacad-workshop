package my.utm.acad.in.core.model;

public enum InRole {

    ROLE_ADMINISTRATOR("Administrator"),
    ROLE_USER("User"),
    ROLE_GUEST("Guest");

    private String desc;

    InRole(String desc) {
        this.desc = desc;
    }

    public InRole getRole(String desc) {
        for (InRole role : InRole.values()) {
            if (role.desc.equals(desc))
                return role;
        }
        return null;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return desc;
    }
}
