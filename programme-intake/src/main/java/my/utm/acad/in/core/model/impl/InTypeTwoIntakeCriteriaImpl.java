package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InCriteriaGrade;
import my.utm.acad.in.core.model.InCriteriaOperator;
import my.utm.acad.in.core.model.InCriteriaSubject;
import my.utm.acad.in.core.model.InTypeTwoIntakeCriteria;

import java.util.List;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class InTypeTwoIntakeCriteriaImpl extends InIntakeCriteriaImpl implements InTypeTwoIntakeCriteria {

    private Integer count;
    private InCriteriaOperator operator;
    private InCriteriaGrade grade;
    private List<InCriteriaSubject> includes;
    private List<InCriteriaSubject> excludes;

    @Override
    public Integer getCount() {
        return count;
    }

    @Override
    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public InCriteriaOperator getOperator() {
        return operator;
    }

    @Override
    public void setOperator(InCriteriaOperator operator) {
        this.operator = operator;
    }

    @Override
    public InCriteriaGrade getGrade() {
        return grade;
    }

    @Override
    public void setGrade(InCriteriaGrade grade) {
        this.grade = grade;
    }

    @Override
    public List<InCriteriaSubject> getIncludes() {
        return includes;
    }

    @Override
    public void setIncludes(List<InCriteriaSubject> includes) {
        this.includes = includes;
    }

    @Override
    public List<InCriteriaSubject> getExcludes() {
        return excludes;
    }

    @Override
    public void setExcludes(List<InCriteriaSubject> excludes) {
        this.excludes = excludes;
    }
}


