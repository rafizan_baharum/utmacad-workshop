package my.utm.acad.in.core.model;

public interface InAddress extends InObject {

    InAddressType getType();

    void setType(InAddressType type);

    String getAddress1();

    void setAddress1(String address1);

    String getAddress2();

    void setAddress2(String address2);

    String getAddress3();

    void setAddress3(String address3);

    String getPostCode();

    void setPostCode(String postCode);

    InCountryCode getCountryCode();

    void setCountryCode(InCountryCode countryCode);

    InStateCode getStateCode();

    void setStateCode(InStateCode stateCode);

    InIntakeApplication getApplication();

    void setApplication(InIntakeApplication application);
}
