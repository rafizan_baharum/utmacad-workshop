package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InGroup;
import my.utm.acad.in.core.model.InGroupMember;

import javax.persistence.*;
import java.util.Set;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "InGroup")
@Table(name = "IN_GROP")
public class InGroupImpl extends InPrincipalImpl implements InGroup {

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = InGroupMemberImpl.class)
    @JoinTable(name = "IN_GROP_MMBR", joinColumns = {
            @JoinColumn(name = "GROUP_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "PRINCIPAL_ID",
                    nullable = false, updatable = false)})
    private Set<InGroupMember> members;

    @Override
    public Set<InGroupMember> getMembers() {
        return members;
    }

    @Override
    public void setMembers(Set<InGroupMember> members) {
        this.members = members;
    }
}
