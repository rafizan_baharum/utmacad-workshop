package my.utm.acad.in.core.model;

/**
 * [Level] With CGPA/Point/Band at least [point]
 * SPM with CGPA/Point/Band at least 2.5
 *
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface InTypeThreeIntakeCriteria extends InIntakeCriteria {

    Double getValue();

    void setValue(Double value);
}
