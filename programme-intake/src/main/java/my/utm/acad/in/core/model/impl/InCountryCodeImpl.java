package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.InCountryCode;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class InCountryCodeImpl implements InCountryCode {

    private String code;
    private String prefixCode;
    private String description;

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    public String getPrefixCode() {
        return prefixCode;
    }

    public void setPrefixCode(String prefixCode) {
        this.prefixCode = prefixCode;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }
}
