package my.utm.acad.in.core.model;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public enum InSchoolType {

    SMS,
    SBP,
    SBPI,
    MRSM,
    SMK,
    SMAT,
    SMAP,
    SMA,
    SMKA,
    SMV,
    SMT,
    SEKOLAH,
    KOLEJ

}
