package my.utm.acad.in.core.model;

import java.util.List;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface InIntakePolicy extends InObject {

    InIntake getIntake();

    void setIntake(InIntake intake);

    InProgramCode getProgramCode();

    void setProgramCode(InProgramCode program);

    List<InIntakeCriteria> getCriterias();

    void setCriterias(List<InIntakeCriteria> criterias);
}
