package my.utm.acad.in.core.model.impl;

import my.utm.acad.in.core.model.*;

import java.util.List;

/**
 * @author team utamacad
 * @since 1/2/2015.
 */
public class InIntakeApplicationImpl implements InIntakeApplication {

    private String referenceNo;
    private String sourceNo;
    private String description;
    private String name;
    private String email;
    private String nricNo;
    private String passportNo;
    private boolean nationality;

    private Integer age;
    private InRace race;
    private InGender gender;
    private InReligion religion;

    private InSchoolType schoolType;
    private String schoolName;
    private Integer schoolBatch;

    private InCampusCode campusCode;
    private InIntake intake;
    private InIntakeCategory category;
    private List<InProgramCode> choices;
    private List<InCriteriaResult> results;
    private List<InAddress> addresses;
    private List<InAttachment> documents;

    @Override
    public String getReferenceNo() {
        return referenceNo;
    }

    @Override
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    @Override
    public String getSourceNo() {
        return sourceNo;
    }

    @Override
    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getNricNo() {
        return nricNo;
    }

    @Override
    public void setNricNo(String nricNo) {
        this.nricNo = nricNo;
    }

    @Override
    public String getPassportNo() {
        return passportNo;
    }

    @Override
    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    @Override
    public boolean isNationality() {
        return nationality;
    }

    @Override
    public void setNationality(boolean nationality) {
        this.nationality = nationality;
    }

    @Override
    public Integer getAge() {
        return age;
    }

    @Override
    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public InRace getRace() {
        return race;
    }

    @Override
    public void setRace(InRace race) {
        this.race = race;
    }

    @Override
    public InGender getGender() {
        return gender;
    }

    @Override
    public void setGender(InGender gender) {
        this.gender = gender;
    }

    @Override
    public InReligion getReligion() {
        return religion;
    }

    @Override
    public void setReligion(InReligion religion) {
        this.religion = religion;
    }

    public InSchoolType getSchoolType() {
        return schoolType;
    }

    public void setSchoolType(InSchoolType schoolType) {
        this.schoolType = schoolType;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public Integer getSchoolBatch() {
        return schoolBatch;
    }

    public void setSchoolBatch(Integer schoolBatch) {
        this.schoolBatch = schoolBatch;
    }

    @Override
    public InCampusCode getCampusCode() {
        return campusCode;
    }

    @Override
    public void setCampusCode(InCampusCode campus) {
        this.campusCode = campus;
    }

    @Override
    public InIntakeCategory getCategory() {
        return category;
    }

    @Override
    public void setCategory(InIntakeCategory category) {
        this.category = category;
    }

    @Override
    public InIntake getIntake() {
        return intake;
    }

    @Override
    public void setIntake(InIntake intake) {
        this.intake = intake;
    }

    @Override
    public List<InProgramCode> getChoices() {
        return choices;
    }

    @Override
    public void setChoices(List<InProgramCode> programs) {
        this.choices = programs;
    }

    @Override
    public List<InCriteriaResult> getResults() {
        return results;
    }

    @Override
    public void setResults(List<InCriteriaResult> results) {
        this.results = results;
    }

    @Override
    public List<InAddress> getAddresses() {
        return addresses;
    }

    @Override
    public void setAddresses(List<InAddress> addresses) {
        this.addresses = addresses;
    }

    @Override
    public List<InAttachment> getDocuments() {
        return documents;
    }

    @Override
    public void setDocuments(List<InAttachment> documents) {
        this.documents = documents;
    }
}
