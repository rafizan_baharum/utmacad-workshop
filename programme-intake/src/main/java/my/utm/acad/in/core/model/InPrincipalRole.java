package my.utm.acad.in.core.model;

public interface InPrincipalRole {

    InPrincipal getPrincipal();

    InRole getRole();

    void setPrincipal(InPrincipal principal);

    /**
     * set role
     *
     * @param role
     */
    void setRole(InRole role);
}
