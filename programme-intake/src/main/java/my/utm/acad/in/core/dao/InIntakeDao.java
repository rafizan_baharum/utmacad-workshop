package my.utm.acad.in.core.dao;

import my.utm.acad.in.core.model.InIntake;
import my.utm.acad.in.core.model.InIntakeApplication;
import my.utm.acad.in.core.model.InIntakeCategory;
import my.utm.acad.in.core.model.InIntakeSession;

import java.util.List;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public interface InIntakeDao extends GenericDao<Long, InIntake> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    InIntake findByReferenceNo(String referenceNo);

    InIntake findBySessionAndCategory(InIntakeSession session, InIntakeCategory category);

    List<InIntake> find(InIntakeSession session);

    List<InIntake> find(InIntakeSession session, Integer limit, Integer offset);

    List<InIntakeApplication> findApplications(InIntake inIntake);

    List<InIntakeApplication> findApplications(InIntake inIntake, Integer limit, Integer offset);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(InIntakeSession session);

    Integer countApplication(InIntake inIntake);

    // ====================================================================================================
    // CRUD
    // ====================================================================================================

}
