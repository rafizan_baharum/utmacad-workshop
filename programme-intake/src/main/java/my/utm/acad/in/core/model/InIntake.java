package my.utm.acad.in.core.model;

import java.util.Date;
import java.util.List;

public interface InIntake {

    String getReferenceNo();

    void setReferenceNo(String referenceNo);

    Date getStartDate();

    void setStartDate(Date startDate);

    Date getEndDate();

    void setEndDate(Date endDate);

    Integer getProjection();

    void setProjection(Integer projection);

    InIntakeSession getSession();

    void setSession(InIntakeSession session);

    InIntakeCategory getCategory();

    void setCategory(InIntakeCategory category);

    List<InIntakePolicy> getAdmissionPolicies();

    void setAdmissionPolicies(List<InIntakePolicy> policies);

    List<InIntakeApplication> getApplications();

    void setApplications(List<InIntakeApplication> applications);
}
