package my.utm.acad.in.core.model;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface InCountryCode {

    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);

}
