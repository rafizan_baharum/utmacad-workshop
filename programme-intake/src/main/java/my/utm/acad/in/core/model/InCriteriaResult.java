package my.utm.acad.in.core.model;

/**
 * @author team utamacad
 * @since 1/2/2015.
 */
public interface InCriteriaResult {

    InCriteriaSubject getSubject();

    void setSubject(InCriteriaSubject subject);

    InCriteriaGrade getGrade();

    void setGrade(InCriteriaGrade grade);

    InIntakeApplication getApplication();

    void setApplication(InIntakeApplication application);

}
