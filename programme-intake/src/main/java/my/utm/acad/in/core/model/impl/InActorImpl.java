package my.utm.acad.in.core.model.impl;


import my.utm.acad.in.core.model.InActor;
import my.utm.acad.in.core.model.InActorType;
import my.utm.acad.in.core.model.InAddress;
import my.utm.acad.in.core.model.InMetadata;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author team utmInAd
 * @since 7/2/2015.
 */
@Entity(name = "InActor")
@Table(name = "IN_ACTR")
@Inheritance(strategy = InheritanceType.JOINED)
public class InActorImpl implements InActor {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_IN_ACTR")
    @SequenceGenerator(name = "SEQ_IN_ACTR", sequenceName = "SEQ_IN_ACTR", allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "NAME", nullable = false)
    private String name;

    @NotNull
    @Column(name = "EMAIL")
    private String email;

    @NotNull
    @Column(name = "PHONE")
    private String phone;

    @NotNull
    @Column(name = "FAX")
    private String fax;

    @NotNull
    @Column(name = "IDENTITY_NO", nullable = false)
    private String identityNo;

    @NotNull
    @Column(name = "ACTOR_TYPE", nullable = false)
    private InActorType actorType;

    @Embedded
    private InMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getPhone() {
        return phone;
    }

    @Override
    public String getFax() {
        return fax;
    }

    @Override
    public void setFax(String fax) {
        this.fax = fax;
    }

    @Override
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public InActorType getActorType() {
        return actorType;
    }

    @Override
    public void setActorType(InActorType actorType) {
        this.actorType = actorType;
    }

    public InMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(InMetadata metadata) {
        this.metadata = metadata;
    }
}
