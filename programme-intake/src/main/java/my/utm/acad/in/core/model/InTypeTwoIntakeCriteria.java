package my.utm.acad.in.core.model;

import java.util.List;

/**
 * [level] has [count] subjects [operator] [grade], includes [,,,], excludes [,,,]
 * SPM has 2 subjects at least C, includes [Bahasa Malaysia, Geography] excludes [English]
 *
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface InTypeTwoIntakeCriteria extends InIntakeCriteria {

    Integer getCount();

    void setCount(Integer count);

    InCriteriaOperator getOperator();

    void setOperator(InCriteriaOperator operator);

    InCriteriaGrade getGrade();

    void setGrade(InCriteriaGrade grade);

    List<InCriteriaSubject> getIncludes();

    void setIncludes(List<InCriteriaSubject> includes);

    List<InCriteriaSubject> getExcludes();

    void setExcludes(List<InCriteriaSubject> excludes);
}
