package my.utm.acad.sf.core.model;

public enum SfPrincipalType {
    USER,
    GROUP,
}
