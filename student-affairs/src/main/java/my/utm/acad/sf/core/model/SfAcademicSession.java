package my.utm.acad.sf.core.model;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */

public interface SfAcademicSession extends SfMetaObject {

    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);
}
