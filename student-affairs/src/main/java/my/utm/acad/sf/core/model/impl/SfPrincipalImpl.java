package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfMetadata;
import my.utm.acad.sf.core.model.SfPrincipal;
import my.utm.acad.sf.core.model.SfPrincipalType;

import javax.persistence.*;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "SfPrincipal")
@Table(name = "SF_PCPL")
@Inheritance(strategy = InheritanceType.JOINED)
public class SfPrincipalImpl implements SfPrincipal {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_SF_PCPL")
    @SequenceGenerator(name = "SEQ_SF_PCPL", sequenceName = "SEQ_SF_PCPL", allocationSize = 1)
    private Long id;

    @Column(name = "NAME", unique = true, nullable = false)
    private String name;

    @Column(name = "PRINCIPAL_TYPE")
    private SfPrincipalType principalType;

    @Embedded
    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public SfPrincipalType getPrincipalType() {
        return principalType;
    }

    @Override
    public void setPrincipalType(SfPrincipalType principalType) {
        this.principalType = principalType;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
