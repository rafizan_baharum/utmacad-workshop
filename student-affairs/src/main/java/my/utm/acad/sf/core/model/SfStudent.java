package my.utm.acad.sf.core.model;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfStudent extends SfActor {

    String getMatricNo();

    void setMatricNo(String matricNo);
}
