package my.utm.acad.sf.core.model;

import java.util.Date;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfFacilityOccupancy extends SfMetaObject {

    Date getCheckInDate();

    void setCheckInDate(Date checkInDate);

    Date getCheckOutDate();

    void setCheckOutDate(Date checkOutDate);

    SfFacility getFacility();

    void setFacility(SfFacility facility);

    SfClub getClub();

    void setClub(SfClub club);

}
