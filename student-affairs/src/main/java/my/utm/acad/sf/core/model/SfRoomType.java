package my.utm.acad.sf.core.model;

/**
 * @author team utmacad
 * @since 10/2/2015.
 */
public enum SfRoomType {

    SINGLE,
    DOUBLE,
    SUITE;
}
