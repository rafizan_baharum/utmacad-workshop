package my.utm.acad.sf.core.model;

public interface SfUser extends SfPrincipal {

    String getRealName();

    void setRealName(String realName);

    String getPassword();

    void setPassword(String password);

    SfActor getActor();

    void setActor(SfActor actor);
}
