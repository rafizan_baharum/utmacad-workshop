package my.utm.acad.sf.core.dao;

import my.utm.acad.sf.core.model.SfRoom;
import my.utm.acad.sf.core.model.SfRoomOccupancy;
import my.utm.acad.sf.core.model.SfUser;

import java.util.Date;
import java.util.List;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfRoomDao extends GenericDao<Long, SfRoom> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    SfRoom findByCode(String code);

    List<SfRoom> find(String filter, Integer offset, Integer limit);

    void addOccupancy(SfRoom room, SfRoomOccupancy occupancy, SfUser user);

    void updateOccupancy(SfRoom room, SfRoomOccupancy occupancy, SfUser user);

    void removeOccupancy(SfRoom room, SfRoomOccupancy occupancy, SfUser user);


    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(String filter);

    boolean isAvailable(Date startDate, Date endDate, SfRoom room);

}
