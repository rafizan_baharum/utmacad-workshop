package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfStudent;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class SfStudentImpl extends SfActorImpl implements SfStudent {


    @Override
    public String getMatricNo() {
        return getIdentityNo();
    }

    @Override
    public void setMatricNo(String matricNo) {
        setIdentityNo(matricNo);
    }
}