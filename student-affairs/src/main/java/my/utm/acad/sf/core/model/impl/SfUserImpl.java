package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfActor;
import my.utm.acad.sf.core.model.SfUser;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author team utmSfAd
 * @since 7/2/2015.
 */
@Entity(name = "SfUser")
@Table(name = "SF_USER")
public class SfUserImpl extends SfPrincipalImpl implements SfUser {

    @NotNull
    @Column(name = "REAL_NAME")
    private String realName;

    @NotNull
    @Column(name = "PASSWORD")
    private String password;

    @OneToOne(targetEntity = SfActorImpl.class)
    @JoinColumn(name = "ACTOR_ID")
    private SfActor actor;

    @Override
    public String getRealName() {
        return realName;
    }

    @Override
    public void setRealName(String realName) {
        this.realName = realName;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public SfActor getActor() {
        return actor;
    }

    @Override
    public void setActor(SfActor actor) {
        this.actor = actor;
    }
}
