package my.utm.acad.sf.core.dao;

import my.utm.acad.sf.core.model.SfAcademicSession;

import java.util.Date;
import java.util.List;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfAcademicSessionDao extends GenericDao<Long, SfAcademicSession> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    SfAcademicSession findByCode(String code);

    SfAcademicSession findCurrent();

    List<SfAcademicSession> find(String filter, Integer offset, Integer limit);

    List<SfAcademicSession> find(String filter, SfAcademicSession academicSession, Integer offset, Integer limit);


    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(String filter);

    boolean isAvailable(Date startDate, Date endDate, SfAcademicSession AcademicSession);

}
