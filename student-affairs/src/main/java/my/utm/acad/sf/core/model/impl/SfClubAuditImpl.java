package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.*;

import java.util.List;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 21/2/2015.
 */
public class SfClubAuditImpl implements SfClubAudit {

    private Long id;
    private String referenceNo;
    private String sourceNo;
    private String description;

    private SfAcademicSession session;
    private SfClub club;
    private List<SfClubAuditItem> items;

    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getReferenceNo() {
        return referenceNo;
    }

    @Override
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    @Override
    public String getSourceNo() {
        return sourceNo;
    }

    @Override
    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public SfAcademicSession getSession() {
        return session;
    }

    @Override
    public void setSession(SfAcademicSession session) {
        this.session = session;
    }

    @Override
    public SfClub getClub() {
        return club;
    }

    @Override
    public void setClub(SfClub club) {
        this.club = club;
    }

    @Override
    public List<SfClubAuditItem> getItems() {
        return items;
    }

    public void setItems(List<SfClubAuditItem> items) {
        this.items = items;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
