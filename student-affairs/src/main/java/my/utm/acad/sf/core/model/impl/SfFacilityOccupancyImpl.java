package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfClub;
import my.utm.acad.sf.core.model.SfFacility;
import my.utm.acad.sf.core.model.SfFacilityOccupancy;
import my.utm.acad.sf.core.model.SfMetadata;

import java.util.Date;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public class SfFacilityOccupancyImpl implements SfFacilityOccupancy {

    private Long id;
    private Date checkInDate;
    private Date checkOutDate;
    private SfFacility facility;
    private SfClub club;

    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Date getCheckInDate() {
        return checkInDate;
    }

    @Override
    public void setCheckInDate(Date checkInDate) {
        this.checkInDate = checkInDate;
    }

    @Override
    public Date getCheckOutDate() {
        return checkOutDate;
    }

    @Override
    public void setCheckOutDate(Date checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    @Override
    public SfFacility getFacility() {
        return facility;
    }

    @Override
    public void setFacility(SfFacility facility) {
        this.facility = facility;
    }

    @Override
    public SfClub getClub() {
        return club;
    }

    @Override
    public void setClub(SfClub club) {
        this.club = club;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
