package my.utm.acad.sf.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface SfDocument extends SfMetaObject {

    String getReferenceNo();

    void setReferenceNo(String referenceNo);

    String getSourceNo();

    void setSourceNo(String sourceNo);

    String getDescription();

    void setDescription(String description);
}
