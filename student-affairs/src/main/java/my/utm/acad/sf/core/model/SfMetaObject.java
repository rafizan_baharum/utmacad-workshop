package my.utm.acad.sf.core.model;

import java.io.Serializable;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface SfMetaObject extends Serializable {

    Long getId();

    void setId(Long id);

    SfMetadata getMetadata();

    void setMetadata(SfMetadata metadata);
}
