package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfClubActivityApplication;
import my.utm.acad.sf.core.model.SfClubActivityOutcome;
import my.utm.acad.sf.core.model.SfMetadata;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public class SfClubActivityOutcomeImpl implements SfClubActivityOutcome {

    private Long id;
    private String description;
    private SfClubActivityApplication application;
    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public SfClubActivityApplication getApplication() {
        return application;
    }

    @Override
    public void setApplication(SfClubActivityApplication application) {
        this.application = application;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
