package my.utm.acad.sf.core.model;

import java.util.Date;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfRoomOccupancy extends SfMetaObject {

    Date getCheckInDate();

    void setCheckInDate(Date checkInDate);

    Date getCheckOutDate();

    void setCheckOutDate(Date checkOutDate);

    SfRoom getRoom();

    void setRoom(SfRoom roomCode);

    SfStudent getStudent();

    void setStudent(SfStudent student);
}
