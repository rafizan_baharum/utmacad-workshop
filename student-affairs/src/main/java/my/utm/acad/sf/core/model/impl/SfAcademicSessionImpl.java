package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfAcademicSession;
import my.utm.acad.sf.core.model.SfMetadata;

import javax.persistence.*;

/**
 * @author team utamacdm
 * @since 4/2/2015.
 */
@Entity(name = "SfAcademicSessionImpl")
@Table(name = "SF_ACDM_SESN")
public class SfAcademicSessionImpl implements SfAcademicSession {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SF_ACDM_SESN")
    @SequenceGenerator(name = "SEQ_SF_ACDM_SESN", sequenceName = "SEQ_SF_ACDM_SESN", allocationSize = 1)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "CODE", nullable = false)
    private String code;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Embedded
    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
