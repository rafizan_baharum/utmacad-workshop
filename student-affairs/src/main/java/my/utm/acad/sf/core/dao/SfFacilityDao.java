package my.utm.acad.sf.core.dao;

import my.utm.acad.sf.core.model.SfFacility;
import my.utm.acad.sf.core.model.SfFacilityOccupancy;
import my.utm.acad.sf.core.model.SfUser;

import java.util.Date;
import java.util.List;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfFacilityDao extends GenericDao<Long, SfFacility> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    SfFacility findByCode(String code);

    List<SfFacility> find(String filter, Integer offset, Integer limit);

    void addOccupancy(SfFacility Facility, SfFacilityOccupancy occupancy, SfUser user);

    void updateOccupancy(SfFacility Facility, SfFacilityOccupancy occupancy, SfUser user);

    void removeOccupancy(SfFacility Facility, SfFacilityOccupancy occupancy, SfUser user);


    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(String filter);

    boolean isAvailable(Date startDate, Date endDate, SfFacility Facility);

}
