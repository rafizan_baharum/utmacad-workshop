package my.utm.acad.sf.core.model;


public interface SfPrincipal extends SfMetaObject {

    String getName();

    void setName(String name);

    SfPrincipalType getPrincipalType();

    void setPrincipalType(SfPrincipalType type);
}
