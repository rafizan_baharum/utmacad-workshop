package my.utm.acad.sf.core.model;

public interface SfGroupMember extends SfMetaObject {

    SfPrincipal getPrincipal();

    void setPrincipal(SfPrincipal principal);

    SfGroup getGroup();

    void setGroup(SfGroup group);
}
