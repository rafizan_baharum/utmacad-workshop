package my.utm.acad.sf.core.model;

import java.math.BigDecimal;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfClubRole extends SfMetaObject {

    String getTitle();

    void setTitle(String title);

    String getDescription();

    void setDescription(String description);

    BigDecimal getMerit();

    void setMerit(BigDecimal merit);

    SfClub getClub();

    void setClub(SfClub club);
}
