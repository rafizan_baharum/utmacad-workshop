package my.utm.acad.sf.core.model;

import java.util.Date;
import java.util.List;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfClubFacilityApplication extends SfDocument {

    Date getEventDate();

    void setEventDate(Date eventDate);

    SfClub getClub();

    void setClub(SfClub club);

    SfStudent getDirector();

    void setDirector(SfStudent director);

    SfStaff getSupervisor();

    void setSupervisor(SfStaff supervisor);

    List<SfFacility> getFacilities();

    void setFacilities(List<SfFacility> facilities);


}
