package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfClub;
import my.utm.acad.sf.core.model.SfClubRole;
import my.utm.acad.sf.core.model.SfMetadata;

import java.math.BigDecimal;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public class SfClubRoleImpl implements SfClubRole {

    private Long id;
    private String title;
    private String description;
    private BigDecimal merit;

    private SfClub club;
    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public BigDecimal getMerit() {
        return merit;
    }

    @Override
    public void setMerit(BigDecimal merit) {
        this.merit = merit;
    }

    @Override
    public SfClub getClub() {
        return club;
    }

    @Override
    public void setClub(SfClub club) {
        this.club = club;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
