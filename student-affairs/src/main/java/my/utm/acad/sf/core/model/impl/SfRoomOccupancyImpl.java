package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfMetadata;
import my.utm.acad.sf.core.model.SfRoom;
import my.utm.acad.sf.core.model.SfRoomOccupancy;
import my.utm.acad.sf.core.model.SfStudent;

import java.util.Date;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public class SfRoomOccupancyImpl implements SfRoomOccupancy {

    private Long id;
    private Date checkInDate;
    private Date checkOutDate;
    private SfRoom room;
    private SfStudent student;

    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Date getCheckInDate() {
        return checkInDate;
    }

    @Override
    public void setCheckInDate(Date checkInDate) {
        this.checkInDate = checkInDate;
    }

    @Override
    public Date getCheckOutDate() {
        return checkOutDate;
    }

    @Override
    public void setCheckOutDate(Date checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    @Override
    public SfRoom getRoom() {
        return room;
    }

    @Override
    public void setRoom(SfRoom room) {
        this.room = room;
    }

    @Override
    public SfStudent getStudent() {
        return student;
    }

    @Override
    public void setStudent(SfStudent student) {
        this.student = student;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
