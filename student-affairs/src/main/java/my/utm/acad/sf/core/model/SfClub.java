package my.utm.acad.sf.core.model;

import java.util.List;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfClub extends SfMetaObject{

    String getCode();

    void setCode(String code);

    String getName();

    void setName(String name);

    SfClubType getClubType();

    void setClubType(SfClubType clubType);

    List<SfClubMember> getMembers();

    void setMembers(List<SfClubMember> members);

}
