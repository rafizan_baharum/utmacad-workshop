package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfClubAudit;
import my.utm.acad.sf.core.model.SfClubAuditItem;
import my.utm.acad.sf.core.model.SfClubMember;

import java.math.BigDecimal;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 21/2/2015.
 */
public class SfClubAuditItemImpl implements SfClubAuditItem {

    private Long id;
    private BigDecimal merit;
    private String memo;
    private SfClubAudit audit;
    private SfClubMember member;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public BigDecimal getMerit() {
        return merit;
    }

    @Override
    public void setMerit(BigDecimal merit) {
        this.merit = merit;
    }

    @Override
    public String getMemo() {
        return memo;
    }

    @Override
    public void setMemo(String memo) {
        this.memo = memo;
    }

    public SfClubAudit getAudit() {
        return audit;
    }

    public void setAudit(SfClubAudit audit) {
        this.audit = audit;
    }

    @Override
    public SfClubMember getMember() {
        return member;
    }

    @Override
    public void setMember(SfClubMember member) {
        this.member = member;
    }
}
