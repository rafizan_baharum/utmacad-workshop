package my.utm.acad.sf.core.model;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfClubApplication extends SfDocument{

    String getName();

    void setName(String name);

    SfClubType getClubType();

    void setClubType(SfClubType clubType);
}
