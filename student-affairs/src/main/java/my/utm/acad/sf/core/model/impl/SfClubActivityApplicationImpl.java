package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public class SfClubActivityApplicationImpl implements SfClubActivityApplication {

    private Long id;
    private String referenceNo;
    private String sourceNo;
    private String description;
    private String eventVenue;
    private Date eventDate;
    private Date participationCount;
    private BigDecimal requestedAmount;
    private SfClubActivityType activityType;

    private SfClub club;
    private SfStudent director;
    private SfStaff supervisor;

    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    @Override
    public String getSourceNo() {
        return sourceNo;
    }

    @Override
    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getEventVenue() {
        return eventVenue;
    }

    @Override
    public void setEventVenue(String eventVenue) {
        this.eventVenue = eventVenue;
    }

    @Override
    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    @Override
    public BigDecimal getRequestedAmount() {
        return requestedAmount;
    }

    @Override
    public void setRequestedAmount(BigDecimal requestedAmount) {
        this.requestedAmount = requestedAmount;
    }

    public Date getParticipationCount() {
        return participationCount;
    }

    public void setParticipationCount(Date participationCount) {
        this.participationCount = participationCount;
    }

    @Override
    public SfClubActivityType getActivityType() {
        return activityType;
    }

    @Override
    public void setActivityType(SfClubActivityType activityType) {
        this.activityType = activityType;
    }

    @Override
    public SfClub getClub() {
        return club;
    }

    @Override
    public void setClub(SfClub club) {
        this.club = club;
    }

    public SfStudent getDirector() {
        return director;
    }

    public void setDirector(SfStudent director) {
        this.director = director;
    }

    @Override
    public SfStaff getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(SfStaff supervisor) {
        this.supervisor = supervisor;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
