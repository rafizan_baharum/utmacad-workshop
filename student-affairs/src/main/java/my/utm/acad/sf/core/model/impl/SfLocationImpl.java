package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfLocation;
import my.utm.acad.sf.core.model.SfMetadata;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public class SfLocationImpl implements SfLocation {

    private Long id;
    private String code;
    private String description;

    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
