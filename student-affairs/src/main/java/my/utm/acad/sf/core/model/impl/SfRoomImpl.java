package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfLocation;
import my.utm.acad.sf.core.model.SfMetadata;
import my.utm.acad.sf.core.model.SfRoom;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public class SfRoomImpl implements SfRoom {

    private Long id;
    private String code;
    private String description;

    private SfLocation location;

    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public SfLocation getLocation() {
        return location;
    }

    @Override
    public void setLocation(SfLocation location) {
        this.location = location;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
