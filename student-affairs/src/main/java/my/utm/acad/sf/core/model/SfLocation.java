package my.utm.acad.sf.core.model;

/**
 * @author team utmacad
 * @since 10/2/2015.
 */
public interface SfLocation extends SfMetaObject {

    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);

}
