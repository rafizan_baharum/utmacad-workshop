package my.utm.acad.sf.core.model;

/**
 * SENI MEMPERTAHANKAN DIRI
 * SUKAN DAN REKREASI
 * KEPEMIMPINAN
 * KREATIF
 * KEUSAHAWANAN
 * KEROHANIAN
 * KEBUDAYAAN
 * KEBAJIKAN
 * FAKULTI
 * KOLEJ
 * 1MALAYSIA
 *
 * @author team utmacad
 * @since 12/2/2015.
 */
public enum SfClubType {

    INDUK,
    AGAMA,
    SENI,
    AKADEMIK,
    BUKAN_AKADEMIK,
    STUDENT_CHAPTER,
    SAINS,
}
