package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfMetadata;
import my.utm.acad.sf.core.model.SfRoom;
import my.utm.acad.sf.core.model.SfRoomApplication;
import my.utm.acad.sf.core.model.SfStudent;

import java.util.Date;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public class SfRoomApplicationImpl implements SfRoomApplication {

    private Long id;
    private String referenceNo;
    private String sourceNo;
    private String description;
    private Date checkInDate;
    private Date checkOutDate;

    private SfStudent student;
    private SfRoom roomCode;

    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getReferenceNo() {
        return referenceNo;
    }

    @Override
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    @Override
    public String getSourceNo() {
        return sourceNo;
    }

    @Override
    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Date getCheckInDate() {
        return checkInDate;
    }

    @Override
    public void setCheckInDate(Date checkInDate) {
        this.checkInDate = checkInDate;
    }

    @Override
    public Date getCheckOutDate() {
        return checkOutDate;
    }

    @Override
    public void setCheckOutDate(Date checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public SfStudent getStudent() {
        return student;
    }

    public void setStudent(SfStudent student) {
        this.student = student;
    }

    public SfRoom getRoomCode() {
        return roomCode;
    }

    public void setRoomCode(SfRoom roomCode) {
        this.roomCode = roomCode;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
