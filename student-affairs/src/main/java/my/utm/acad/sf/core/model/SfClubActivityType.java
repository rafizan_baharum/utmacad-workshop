package my.utm.acad.sf.core.model;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public enum SfClubActivityType {

    SOKONGAN_AKADEMIK,
    KEPIMPINAN,
    KEBAJIKAN_MASYARAKAT,
    SUKAN,
    KREATIF,
    FIZIKAL_MENTAL,
    KEBUDAYAAN_KESENIAN,
    KEROHANIAN,
    KEUSAHAWAN,
    SENI_PERTAHAN_DIRI,
    INOVASI;
}
