package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.*;

import java.util.Date;
import java.util.List;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public class SfClubFacilityApplicationImpl implements SfClubFacilityApplication {

    private Long id;
    private String referenceNo;
    private String sourceNo;
    private String description;
    private Date eventDate;

    private SfStudent director;
    private SfStaff supervisor;
    private SfClub club;
    private List<SfFacility> facilities;

    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getReferenceNo() {
        return referenceNo;
    }

    @Override
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    @Override
    public String getSourceNo() {
        return sourceNo;
    }

    @Override
    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Date getEventDate() {
        return eventDate;
    }

    @Override
    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    @Override
    public SfStudent getDirector() {
        return director;
    }

    @Override
    public void setDirector(SfStudent director) {
        this.director = director;
    }

    @Override
    public SfStaff getSupervisor() {
        return supervisor;
    }

    @Override
    public void setSupervisor(SfStaff supervisor) {
        this.supervisor = supervisor;
    }

    @Override
    public SfClub getClub() {
        return club;
    }

    @Override
    public void setClub(SfClub club) {
        this.club = club;
    }

    @Override
    public List<SfFacility> getFacilities() {
        return facilities;
    }

    @Override
    public void setFacilities(List<SfFacility> facilities) {
        this.facilities = facilities;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
