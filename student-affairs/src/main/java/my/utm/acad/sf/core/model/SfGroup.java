package my.utm.acad.sf.core.model;

import java.util.Set;

public interface SfGroup extends SfPrincipal {

    Set<SfGroupMember> getMembers();

    void setMembers(Set<SfGroupMember> members);
}
