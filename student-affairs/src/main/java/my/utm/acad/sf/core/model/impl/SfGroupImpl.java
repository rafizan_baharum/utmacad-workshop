package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfGroup;
import my.utm.acad.sf.core.model.SfGroupMember;

import javax.persistence.*;
import java.util.Set;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "SfGroup")
@Table(name = "SF_GROP")
public class SfGroupImpl extends SfPrincipalImpl implements SfGroup {

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = SfGroupMemberImpl.class)
    @JoinTable(name = "SF_GROP_MMBR", joinColumns = {
            @JoinColumn(name = "GROUP_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "PRACCIPAL_ID",
                    nullable = false, updatable = false)})
    private Set<SfGroupMember> members;

    @Override
    public Set<SfGroupMember> getMembers() {
        return members;
    }

    @Override
    public void setMembers(Set<SfGroupMember> members) {
        this.members = members;
    }
}
