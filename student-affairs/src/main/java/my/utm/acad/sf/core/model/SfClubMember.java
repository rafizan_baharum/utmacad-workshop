package my.utm.acad.sf.core.model;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfClubMember extends SfMetaObject{

    SfAcademicSession getSession();

    void setSession(SfAcademicSession session);

    SfStudent getStudent();

    void setStudent(SfStudent student);

    SfClubRole getRole();

    void setRole(SfClubRole role);
}
