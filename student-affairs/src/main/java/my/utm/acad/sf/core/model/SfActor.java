package my.utm.acad.sf.core.model;

public interface SfActor extends SfObject {

    String getIdentityNo();

    void setIdentityNo(String identityNo);

    String getName();

    void setName(String name);

    String getPhone();

    void setPhone(String phone);

    String getFax();

    void setFax(String fax);

    String getEmail();

    void setEmail(String email);

    SfActorType getActorType();

    void setActorType(SfActorType actorType);
}
