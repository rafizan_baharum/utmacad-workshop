package my.utm.acad.sf.core.model;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfStaff extends SfActor {

    String getStaffNo();

    void setStaffNo(String staffNo);
}
