package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.*;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public class SfClubMemberImpl implements SfClubMember {

    private Long id;
    private SfAcademicSession session;
    private SfClub club;
    private SfStudent student;
    private SfClubRole role;

    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public SfAcademicSession getSession() {
        return session;
    }

    @Override
    public void setSession(SfAcademicSession session) {
        this.session = session;
    }

    public SfClub getClub() {
        return club;
    }

    public void setClub(SfClub club) {
        this.club = club;
    }

    @Override
    public SfStudent getStudent() {
        return student;
    }

    @Override
    public void setStudent(SfStudent student) {
        this.student = student;
    }

    @Override
    public SfClubRole getRole() {
        return role;
    }

    @Override
    public void setRole(SfClubRole role) {
        this.role = role;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
