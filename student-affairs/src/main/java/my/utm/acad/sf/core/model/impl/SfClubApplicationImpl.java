package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfClubApplication;
import my.utm.acad.sf.core.model.SfClubType;
import my.utm.acad.sf.core.model.SfMetadata;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public class SfClubApplicationImpl implements SfClubApplication {

    private Long id;
    private String referenceNo;
    private String sourceNo;
    private String description;
    private String name;
    private SfClubType clubType;

    private SfMetadata metadata;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getReferenceNo() {
        return referenceNo;
    }

    @Override
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    @Override
    public String getSourceNo() {
        return sourceNo;
    }

    @Override
    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public SfClubType getClubType() {
        return clubType;
    }

    @Override
    public void setClubType(SfClubType clubType) {
        this.clubType = clubType;
    }

    public SfMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
