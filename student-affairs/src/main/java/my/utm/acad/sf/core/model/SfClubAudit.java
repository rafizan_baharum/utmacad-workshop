package my.utm.acad.sf.core.model;

import java.util.List;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 21/2/2015.
 */
public interface SfClubAudit extends SfDocument {

    SfAcademicSession getSession();

    void setSession(SfAcademicSession session);

    SfClub getClub();

    void setClub(SfClub club);

    List<SfClubAuditItem> getItems();

    void setItems(List<SfClubAuditItem> items);
}
