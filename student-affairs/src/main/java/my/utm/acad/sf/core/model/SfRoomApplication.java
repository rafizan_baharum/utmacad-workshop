package my.utm.acad.sf.core.model;

import java.util.Date;

/**
 * @author team utmacad
 * @since 10/2/2015.
 */
public interface SfRoomApplication extends SfDocument {

    Date getCheckInDate();

    void setCheckInDate(Date checkInDate);

    Date getCheckOutDate();

    void setCheckOutDate(Date checkOutDate);

    SfRoom getRoomCode();

    void setRoomCode(SfRoom roomCode);

    SfStudent getStudent();

    void setStudent(SfStudent student);
}
