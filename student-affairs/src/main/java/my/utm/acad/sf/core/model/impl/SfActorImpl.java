package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfActor;
import my.utm.acad.sf.core.model.SfActorType;
import my.utm.acad.sf.core.model.SfMetadata;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "SfActor")
@Table(name = "SF_ACTR")
@Inheritance(strategy = InheritanceType.JOINED)
public class SfActorImpl implements SfActor {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_SF_ACTR")
    @SequenceGenerator(name = "SEQ_SF_ACTR", sequenceName = "SEQ_SF_ACTR", allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "NAME", nullable = false)
    private String name;

    @NotNull
    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "FAX")
    private String fax;

    @NotNull
    @Column(name = "IDENTITY_NO", nullable = false)
    private String identityNo;

    @NotNull
    @Column(name = "ACTOR_TYPE", nullable = false)
    private SfActorType actorType;

    @Embedded
    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public SfActorType getActorType() {
        return actorType;
    }

    public void setActorType(SfActorType actorType) {
        this.actorType = actorType;
    }

    public SfMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
