package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfClub;
import my.utm.acad.sf.core.model.SfClubMember;
import my.utm.acad.sf.core.model.SfClubType;
import my.utm.acad.sf.core.model.SfMetadata;

import java.util.List;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public class SfClubImpl implements SfClub {

    private Long id;
    private String code;
    private String name;
    private SfClubType clubType;

    private List<SfClubMember> members;

    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public SfClubType getClubType() {
        return clubType;
    }

    @Override
    public void setClubType(SfClubType clubType) {
        this.clubType = clubType;
    }

    @Override
    public List<SfClubMember> getMembers() {
        return members;
    }

    @Override
    public void setMembers(List<SfClubMember> members) {
        this.members = members;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
