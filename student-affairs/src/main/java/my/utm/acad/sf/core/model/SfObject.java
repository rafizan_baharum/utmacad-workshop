package my.utm.acad.sf.core.model;

/**
 * @author rafizan.baharum
 * @since 26/1/2015
 */
public interface SfObject {

    Long getId();
}
