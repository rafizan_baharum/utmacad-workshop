package my.utm.acad.sf.core.model;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfClubActivityOutcome extends SfMetaObject {

    String getDescription();

    void setDescription(String description);

    SfClubActivityApplication getApplication();

    void setApplication(SfClubActivityApplication application);
}
