package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfGroup;
import my.utm.acad.sf.core.model.SfGroupMember;
import my.utm.acad.sf.core.model.SfMetadata;
import my.utm.acad.sf.core.model.SfPrincipal;

import javax.persistence.*;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "SfGroupMember")
@Table(name = "SF_GROP_MMBR")
public class SfGroupMemberImpl implements SfGroupMember {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_SF_GROP_MMBR")
    @SequenceGenerator(name = "SEQ_SF_GROP_MMBR", sequenceName = "SEQ_SF_GROP_MMBR", allocationSize = 1)
    private Long id;

    @OneToOne(targetEntity = SfGroupImpl.class)
    @JoinColumn(name = "GROUP_ID")
    private SfGroup group;

    @OneToOne(targetEntity = SfPrincipalImpl.class)
    @JoinColumn(name = "PRINCIPAL_ID")
    private SfPrincipal principal;

    @Embedded
    private SfMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public SfGroup getGroup() {
        return group;
    }

    @Override
    public void setGroup(SfGroup group) {
        this.group = group;
    }

    @Override
    public SfPrincipal getPrincipal() {
        return principal;
    }

    @Override
    public void setPrincipal(SfPrincipal principal) {
        this.principal = principal;
    }

    @Override
    public SfMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(SfMetadata metadata) {
        this.metadata = metadata;
    }
}
