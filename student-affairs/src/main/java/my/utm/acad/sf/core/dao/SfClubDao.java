package my.utm.acad.sf.core.dao;

import my.utm.acad.sf.core.model.*;

import java.util.List;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfClubDao extends GenericDao<Long, SfClub> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    SfClub findByCode(String code);

    List<SfClub> find(String filter, Integer offset, Integer limit);

    List<SfClub> find(String filter, SfClubType type, Integer offset, Integer limit);

    List<SfClubMember> findMembers(SfAcademicSession current, SfClub club);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(String filter);

    Integer count(SfClubType clubType);

    Integer countMember(SfClub club);

    // ====================================================================================================
    // CLUB
    // ====================================================================================================

    void addMember(SfClub Club, SfClubMember Member, SfUser user);

    void updateMember(SfClub Club, SfClubMember Member, SfUser user);

    void removeMember(SfClub Club, SfClubMember Member, SfUser user);
}
