package my.utm.acad.sf.core.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public interface SfClubActivityApplication extends SfDocument {

    String getEventVenue();

    void setEventVenue(String eventVenue);

    Date getEventDate();

    BigDecimal getRequestedAmount();

    void setRequestedAmount(BigDecimal requestedAmount);

    void setEventDate(Date eventDate);

    SfClubActivityType getActivityType();

    void setActivityType(SfClubActivityType activityType);

    SfClub getClub();

    void setClub(SfClub club);

    SfStudent getDirector();

    void setDirector(SfStudent director);

    SfStaff getSupervisor();

    void setSupervisor(SfStaff supervisor);

}
