package my.utm.acad.sf.core.dao.impl;

import my.utm.acad.sf.core.dao.GenericDaoSupport;
import my.utm.acad.sf.core.dao.SfRoomDao;
import my.utm.acad.sf.core.model.*;
import my.utm.acad.sf.core.model.impl.SfRoomImpl;
import org.apache.commons.lang.Validate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Repository("sfRoomDao")
public class SfRoomDaoImpl extends GenericDaoSupport<Long, SfRoom> implements SfRoomDao {

    private static final Logger LOG = LoggerFactory.getLogger(SfRoomDaoImpl.class);

    public SfRoomDaoImpl() {
        super(SfRoomImpl.class);
    }

    @Override
    public SfRoom findByCode(String code) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from SfRoom s where s.code = :code and  " +
                " s.metadata.state = :state");
        query.setString("code", code);
        query.setCacheable(true);
        query.setInteger("state", SfMetaState.ACTIVE.ordinal());
        return (SfRoom) query.uniqueResult();
    }

    @Override
    public List<SfRoom> find(String filter, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from SfRoom s where " +
                "(upper(s.code) like upper(:filter) " +
                "or upper(s.name) like upper(:filter)) " +
                "and s.metadata.state = :state ");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setInteger("state", SfMetaState.ACTIVE.ordinal());
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        query.setCacheable(true);
        return (List<SfRoom>) query.list();
    }

    @Override
    public Integer count(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(s) from SfRoom s where " +
                "(upper(s.code) like upper(:filter) " +
                "or upper(s.name) like upper(:filter)) " +
                "and s.metadata.state = :state ");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setInteger("state", SfMetaState.ACTIVE.ordinal());
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public boolean isAvailable(Date startDate, Date endDate, SfRoom room) {
        return false; // TODO
    }

    @Override
    public void addOccupancy(SfRoom room, SfRoomOccupancy occupancy, SfUser user) {
        Validate.notNull(user, "User cannot be null");
        Validate.notNull(room, "room cannot be null");
        Validate.notNull(occupancy, "occupancy cannot be null");
        Session session = sessionFactory.getCurrentSession();
        occupancy.setRoom(room);

        // prepare metadata
        SfMetadata metadata = new SfMetadata();
        metadata.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        metadata.setCreatorId(user.getId());
        metadata.setState(SfMetaState.ACTIVE);
        occupancy.setMetadata(metadata);
        session.save(occupancy);
    }

    @Override
    public void updateOccupancy(SfRoom room, SfRoomOccupancy occupancy, SfUser user) {
        Validate.notNull(user, "User cannot be null");
        Session session = sessionFactory.getCurrentSession();
        occupancy.setRoom(room);

        // prepare metadata
        SfMetadata metadata = occupancy.getMetadata();
        metadata.setModifiedDate(new Timestamp(System.currentTimeMillis()));
        metadata.setModifierId(user.getId());
        occupancy.setMetadata(metadata);
        session.update(occupancy);
    }

    @Override
    public void removeOccupancy(SfRoom room, SfRoomOccupancy occupancy, SfUser user) {
        Validate.notNull(user, "User cannot be null");
        Session session = sessionFactory.getCurrentSession();
        occupancy.setRoom(room);

        // prepare metadata
        SfMetadata metadata = occupancy.getMetadata();
        metadata.setDeletedDate(new Timestamp(System.currentTimeMillis()));
        metadata.setDeleterId(user.getId());
        metadata.setState(SfMetaState.INACTIVE);
        occupancy.setMetadata(metadata);
        session.update(occupancy);
    }
}