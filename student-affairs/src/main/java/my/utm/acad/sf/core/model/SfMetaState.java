package my.utm.acad.sf.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public enum SfMetaState {
    INACTIVE, // 0
    ACTIVE,   // 1

}
