package my.utm.acad.sf.core.dao;


import my.utm.acad.sf.core.model.SfUser;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface GenericDao<K, I> {

    I newInstance();

    I refresh(I i);

    I findById(K k);

    List<I> find(Integer offset, Integer limit);

    Integer count();

    void save(I entity, SfUser user);

    void saveOrUpdate(I entity, SfUser user);

    void update(I entity, SfUser user);

    void deactivate(I entity, SfUser user);

    void remove(I entity, SfUser user);

    void delete(I entity, SfUser user);

//    NOTE: available after workshop Spring Security
//    List<I> findAuthorized(Set<String> sids);
//
//    List<I> findAuthorized(Set<String> sids, Integer offset, Integer limit);
//
//    List<Long> findAuthorizedIds(Set<String> sids);

//    Integer countAuthorized(Set<String> sids);


}
