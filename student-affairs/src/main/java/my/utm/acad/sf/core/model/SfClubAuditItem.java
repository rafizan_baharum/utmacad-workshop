package my.utm.acad.sf.core.model;

import java.math.BigDecimal;

/**
 * @author rafizan.baharum@canang.com.my
 * @since 21/2/2015.
 */
public interface SfClubAuditItem {

    SfClubMember getMember();

    void setMember(SfClubMember member);

    BigDecimal getMerit();

    void setMerit(BigDecimal merit);

    String getMemo();

    void setMemo(String memo);
}
