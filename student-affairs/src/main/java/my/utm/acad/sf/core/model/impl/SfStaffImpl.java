package my.utm.acad.sf.core.model.impl;

import my.utm.acad.sf.core.model.SfStaff;

/**
 * @author team utmacad
 * @since 12/2/2015.
 */
public class SfStaffImpl extends SfActorImpl implements SfStaff {

    @Override
    public String getStaffNo() {
        return getIdentityNo();
    }

    @Override
    public void setStaffNo(String staffNo) {
        setIdentityNo(staffNo);
    }
}
