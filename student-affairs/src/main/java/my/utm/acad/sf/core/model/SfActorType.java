package my.utm.acad.sf.core.model;

public enum SfActorType {
    STAFF,
    STUDENT;
}
