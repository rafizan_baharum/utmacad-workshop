package my.utm.acad.sf.core;

import my.utm.acad.configuration.AbstractDaoContextBaseTest;
import my.utm.acad.sf.core.dao.SfAcademicSessionDao;
import my.utm.acad.sf.core.dao.SfClubDao;
import my.utm.acad.sf.core.dao.SfFacilityDao;
import my.utm.acad.sf.core.dao.SfRoomDao;
import my.utm.acad.sf.core.model.*;
import my.utm.acad.sf.core.model.impl.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class DomainTest extends AbstractDaoContextBaseTest {

    @Autowired
    private SfRoomDao roomDao;

    @Autowired
    private SfFacilityDao facilityDao;

    @Autowired
    private SfClubDao clubDao;

    @Autowired
    private SfAcademicSessionDao sessionDao;

    private SfStudent student;
    private SfStaff staff;
    private SfClub club;
    private SfClubRole president;
    private SfClubRole vice;
    private SfRoom room;
    private SfLocation jb;
    private SfFacility facility;

    @Before
    public void setUp() {
        // setup club
        club = new SfClubImpl();
        club.setCode("KU-UTM");
        club.setClubType(SfClubType.BUKAN_AKADEMIK);
        club.setName("KELAB USAHAWAN");

        president = new SfClubRoleImpl();
        president.setTitle("PRESIDENT");
        president.setDescription("CLUB PRESIDENT");

        vice = new SfClubRoleImpl();
        vice.setTitle("VICE PRESIDENT");
        vice.setDescription("CLUB VICE PRESIDENT");

        // setup room
        jb = new SfLocationImpl();
        jb.setCode("JB");
        jb.setDescription("Johor Bahru");

        room = new SfRoomImpl();
        room.setCode("A01-01-01");
        room.setLocation(jb);

        facility = new SfFacilityImpl();
        facility.setCode("DWN-0001");
        facility.setDescription("DEWAN SERBAGUNA");
        facility.setLocation(jb);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testClub() {
        student = new SfStudentImpl();
        student.setName("RAFIZAN BAHARUM");
        student.setEmail("rafizan.baharum@utm.my");
        student.setMatricNo("INT/SEP/2015/BSC/0001");

        staff = new SfStaffImpl();
        staff.setName("RAMLI NGAH");
        staff.setEmail("ramli@utm.my");
        staff.setStaffNo("29101");

        SfClubMember member = new SfClubMemberImpl();
        member.setRole(president);
        member.setStudent(student);

        SfClubMember member2 = new SfClubMemberImpl();
        member2.setRole(vice);
        member2.setStudent(student);
    }

    public void testActivity() {
        SfClubActivityApplication application = new SfClubActivityApplicationImpl();
        application.setReferenceNo("JACT20150100001");
        application.setSourceNo("XXX");
        application.setDescription("Lawatan Sambil Belajar");
        application.setActivityType(SfClubActivityType.KEBAJIKAN_MASYARAKAT);
        application.setClub(club);
        application.setEventVenue("Kuala Lumpur");
        application.setEventDate(new Date());
        application.setDirector(student);
        application.setSupervisor(staff);
    }

    @Test
    public void testRoomApplication() {
        SfRoomApplication booking = new SfRoomApplicationImpl();
        booking.setCheckInDate(new Date());
        booking.setCheckOutDate(new Date());
        booking.setRoomCode(room);
        booking.setStudent(student);

        // check room availability
        if (roomDao.isAvailable(new Date(), new Date(), room)) {
            // if okay then occupy
            SfRoomOccupancy occupancy = new SfRoomOccupancyImpl();
            occupancy.setRoom(room);
            occupancy.setCheckInDate(booking.getCheckInDate());
            occupancy.setCheckOutDate(booking.getCheckOutDate());
        }
    }

    @Test
    public void testFacilityApplication() {
        SfClubFacilityApplication booking = new SfClubFacilityApplicationImpl();
        booking.setReferenceNo("GENERATED");
        booking.setSourceNo("XXX");
        booking.setDirector(student);
        booking.setSupervisor(staff);

        // check facility availability
        if (facilityDao.isAvailable(new Date(), new Date(), facility)) {
            // if okay then occupy
            SfFacilityOccupancy occupancy = new SfFacilityOccupancyImpl();
            occupancy.setFacility(facility);
            occupancy.setCheckInDate(booking.getEventDate());
            occupancy.setCheckOutDate(booking.getEventDate());
        }
    }

    @Test
    public void testClubAudit() {
        SfAcademicSession current = sessionDao.findCurrent();
        List<SfClub> clubs = clubDao.find("%", current, 0, 999);

        // audit club
        for (SfClub club : clubs) {
            SfClubAudit audit = new SfClubAuditImpl();
            audit.setClub(club);
            audit.setSession(current);

            // add member as item
            List<SfClubMember> members = clubDao.findMembers(current, club);
            for (SfClubMember member : members) {
                SfClubAuditItem item = new SfClubAuditItemImpl();
                item.setMember(member);
                item.setMemo("");
                item.setMerit(member.getRole().getMerit());
            }
        }
    }
}