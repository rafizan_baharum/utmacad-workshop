package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.ErActor;
import my.utm.acad.er.core.model.ErActorType;
import my.utm.acad.er.core.model.ErMetadata;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "ErActor")
@Table(name = "ER_ACTR")
@Inheritance(strategy = InheritanceType.JOINED)
public class ErActorImpl implements ErActor {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_ER_ACTR")
    @SequenceGenerator(name = "SEQ_ER_ACTR", sequenceName = "SEQ_ER_ACTR", allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "NAME", nullable = false)
    private String name;

    @NotNull
    @Column(name = "EMAIl")
    private String email;

    // NRIC/ISID
    @NotNull
    @Column(name = "IDENTITY_NO", nullable = false)
    private String identityNo;

    @Column(name = "ACID_NO", nullable = false)
    private String acidNo;

    @NotNull
    @Column(name = "ACTOR_TYPE", nullable = false)
    private ErActorType actorType;

    @Embedded
    private ErMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    @Override
    public String getAcidNo() {
        return acidNo;
    }

    @Override
    public void setAcidNo(String acidNo) {
        this.acidNo = acidNo;
    }

    @Override
    public ErActorType getActorType() {
        return actorType;
    }

    @Override
    public void setActorType(ErActorType actorType) {
        this.actorType = actorType;
    }

    public ErMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(ErMetadata metadata) {
        this.metadata = metadata;
    }
}
