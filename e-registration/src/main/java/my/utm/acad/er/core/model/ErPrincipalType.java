package my.utm.acad.er.core.model;

public enum ErPrincipalType {
    USER,
    GROUP,
}
