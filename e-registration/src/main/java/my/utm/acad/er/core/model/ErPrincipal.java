package my.utm.acad.er.core.model;

public interface ErPrincipal extends ErMetaObject {

    String getName();

    void setName(String name);

    ErPrincipalType getPrincipalType();

    void setPrincipalType(ErPrincipalType type);
}
