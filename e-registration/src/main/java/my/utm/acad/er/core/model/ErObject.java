package my.utm.acad.er.core.model;

/**
 * @author rafizan.baharum
 * @since 26/1/2015
 */
public interface ErObject {

    Long getId();
}
