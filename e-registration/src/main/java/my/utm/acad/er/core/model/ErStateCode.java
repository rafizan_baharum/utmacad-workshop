package my.utm.acad.er.core.model;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface ErStateCode extends ErMetaObject{

    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);

}
