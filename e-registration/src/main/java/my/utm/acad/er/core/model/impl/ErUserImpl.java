package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.ErActor;
import my.utm.acad.er.core.model.ErUser;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author team utmErAd
 * @since 7/2/2015.
 */
@Entity(name = "ErUser")
@Table(name = "ER_USER")
public class ErUserImpl extends ErPrincipalImpl implements ErUser {

    @NotNull
    @Column(name = "REAL_NAME")
    private String realName;

    @NotNull
    @Column(name = "PASSWORD")
    private String password;

    @OneToOne(targetEntity = ErActorImpl.class)
    @JoinColumn(name = "ACTOR_ID")
    private ErActor actor;

    @Override
    public String getRealName() {
        return realName;
    }

    @Override
    public void setRealName(String realName) {
        this.realName = realName;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public ErActor getActor() {
        return actor;
    }

    @Override
    public void setActor(ErActor actor) {
        this.actor = actor;
    }
}
