package my.utm.acad.er.core.model;

public interface ErActor extends ErMetaObject {

    // NRIC/ISID
    String getIdentityNo();

    void setIdentityNo(String identityNo);

    String getAcidNo();

    void setAcidNo(String acidNo);

    String getName();

    void setName(String name);

    String getEmail();

    void setEmail(String email);

    ErActorType getActorType();

    void setActorType(ErActorType actorType);
}
