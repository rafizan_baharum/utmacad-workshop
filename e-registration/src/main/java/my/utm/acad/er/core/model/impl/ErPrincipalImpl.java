package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.ErMetadata;
import my.utm.acad.er.core.model.ErPrincipal;
import my.utm.acad.er.core.model.ErPrincipalType;

import javax.persistence.*;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "ErPrincipal")
@Table(name = "ER_PCPL")
@Inheritance(strategy = InheritanceType.JOINED)
public class ErPrincipalImpl implements ErPrincipal {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_ER_PCPL")
    @SequenceGenerator(name = "SEQ_ER_PCPL", sequenceName = "SEQ_ER_PCPL", allocationSize = 1)
    private Long id;

    @Column(name = "NAME", unique = true, nullable = false)
    private String name;

    @Column(name = "PRINCIPAL_TYPE")
    private ErPrincipalType principalType;

    @Embedded
    private ErMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public ErPrincipalType getPrincipalType() {
        return principalType;
    }

    @Override
    public void setPrincipalType(ErPrincipalType principalType) {
        this.principalType = principalType;
    }

    @Override
    public ErMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(ErMetadata metadata) {
        this.metadata = metadata;
    }
}
