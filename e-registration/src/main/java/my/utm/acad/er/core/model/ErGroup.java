package my.utm.acad.er.core.model;

import java.util.Set;

public interface ErGroup extends ErPrincipal {

    Set<ErGroupMember> getMembers();

    void setMembers(Set<ErGroupMember> members);
}
