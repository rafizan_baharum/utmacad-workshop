package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.ErMetadata;
import my.utm.acad.er.core.model.ErStateCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
@Entity(name = "ErStateCode")
@Table(name = "ER_STTE_CODE")
public class ErStateCodeImpl implements ErStateCode {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_ER_STTE_CODE")
    @SequenceGenerator(name = "SEQ_ER_STTE_CODE", sequenceName = "SEQ_ER_STTE_CODE", allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "CODE", nullable = false)
    private String code;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Embedded
    private ErMetadata metadata;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public ErMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(ErMetadata metadata) {
        this.metadata = metadata;
    }
}
