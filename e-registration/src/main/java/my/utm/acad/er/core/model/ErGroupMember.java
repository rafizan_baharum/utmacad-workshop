package my.utm.acad.er.core.model;


public interface ErGroupMember extends ErMetaObject {

    ErPrincipal getPrincipal();

    void setPrincipal(ErPrincipal principal);

    ErGroup getGroup();

    void setGroup(ErGroup group);
}
