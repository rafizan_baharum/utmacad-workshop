package my.utm.acad.er.core.dao.impl;

import my.utm.acad.er.core.dao.GenericDaoSupport;
import my.utm.acad.er.core.dao.ErUserDao;
import my.utm.acad.er.core.model.*;
import my.utm.acad.er.core.model.impl.ErUserImpl;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author team utmerad
 * @since 7/2/2015.
 */
@Repository("erUserDao")
public class ErUserDaoImpl extends GenericDaoSupport<Long, ErUser> implements ErUserDao {

    private static final Logger LOG = Logger.getLogger(ErUserDaoImpl.class);

    public ErUserDaoImpl() {
        super(ErUserImpl.class);
    }

    // =============================================================================
    // FINDER METHODS
    // =============================================================================

    @Override
    public List<ErGroup> findGroups(ErUser user) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select r from ErGroup r inner join r.members m where m.id = :id");
        query.setLong("id", user.getId());
        return (List<ErGroup>) query.list();
    }

    @Override
    public ErUser findByEmail(String email) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select u from ErUser u where u.email = :email ");
        query.setString("email", email);
        return (ErUser) query.uniqueResult();
    }

    @Override
    public ErUser findByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select u from ErUser u where u.name = :username ");
        query.setString("username", username);
        return (ErUser) query.uniqueResult();
    }

    @Override
    public ErUser findByActor(ErActor actor) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select u from ErUser u where u.actor = :actor ");
        query.setEntity("actor", actor);
        return (ErUser) query.uniqueResult();
    }

    @Override
    public List<ErUser> find(String filter, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from ErUser s where (" +
                "upper(s.name) like upper(:filter) " +
                "or upper(s.name) like upper(:filter)) " +
                "order by s.name, s.name");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return (List<ErUser>) query.list();
    }

    @Override
    public Integer count() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(u) from ErUser u");
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public Integer count(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(s) from ErUser s where " +
                "upper(s.name) like upper(:filter) " +
                "or upper(s.name) like upper(:filter)");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public void save(ErUser user) {
        Validate.notNull(user, "User cannot be null");
        try {
            Session session = sessionFactory.getCurrentSession();
            ErMetadata metadata = new ErMetadata();
            metadata.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            metadata.setCreatorId(0L);
            metadata.setState(ErMetaState.ACTIVE);
            user.setMetadata(metadata);
            session.save(user);
        } catch (HibernateException e) {
            LOG.debug("error occurred", e);
        }
    }

    // =============================================================================
    // HELPER
    // =============================================================================

    @Override
    public boolean isExists(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(*) from ErUser u where " +
                "upper(u.name) = upper(:username) ");
        query.setString("username", username);
        return 0 < ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public boolean hasUser(ErActor actor) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(*) from ErUser u where " +
                "u.actor = :actor");
        query.setEntity("actor", actor);
        return 0 < ((Long) query.uniqueResult()).intValue();
    }

}
