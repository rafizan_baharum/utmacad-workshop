package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.ErCandidate;
import my.utm.acad.er.core.model.ErDisabilityType;
import my.utm.acad.er.core.model.ErGuardian;
import my.utm.acad.er.core.model.ErSponsorType;

import java.util.List;

/**
 * @author team utmacad
 * @since 8/2/2015.
 */
public class ErCandidateImpl extends ErActorImpl implements ErCandidate {

    private ErDisabilityType disabilityType;
    private ErSponsorType sponsorType;
    private List<ErGuardian> guardians;

    @Override
    public ErDisabilityType getDisabilityType() {
        return disabilityType;
    }

    @Override
    public void setDisabilityType(ErDisabilityType disabilityType) {
        this.disabilityType = disabilityType;
    }

    public ErSponsorType getSponsorType() {
        return sponsorType;
    }

    public void setSponsorType(ErSponsorType sponsorType) {
        this.sponsorType = sponsorType;
    }

    @Override
    public List<ErGuardian> getGuardians() {
        return guardians;
    }

    @Override
    public void setGuardians(List<ErGuardian> guardians) {
        this.guardians = guardians;
    }
}
