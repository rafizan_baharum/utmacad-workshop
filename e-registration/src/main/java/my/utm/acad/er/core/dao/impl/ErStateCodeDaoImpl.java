package my.utm.acad.er.core.dao.impl;

import my.utm.acad.er.core.dao.GenericDaoSupport;
import my.utm.acad.er.core.dao.ErStateCodeDao;
import my.utm.acad.er.core.model.ErMetaState;
import my.utm.acad.er.core.model.ErStateCode;
import my.utm.acad.er.core.model.impl.ErStateCodeImpl;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Repository("erStateCode")
public class ErStateCodeDaoImpl extends GenericDaoSupport<Long, ErStateCode> implements ErStateCodeDao {

    private static final Logger LOG = LoggerFactory.getLogger(ErStateCodeDaoImpl.class);

    public ErStateCodeDaoImpl() {
        super(ErStateCodeImpl.class);
    }

    @Override
    public ErStateCode findByCode(String code) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from ErStateCode s where s.code = :code and  " +
                " s.metadata.state = :state");
        query.setString("code", code);
        query.setCacheable(true);
        query.setInteger("state", ErMetaState.ACTIVE.ordinal());
        return (ErStateCode) query.uniqueResult();
    }

    @Override
    public List<ErStateCode> find() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from ErStateCode s where " +
                " s.metadata.state = :state ");
        query.setInteger("state", ErMetaState.ACTIVE.ordinal());
        query.setCacheable(true);
        return (List<ErStateCode>) query.list();
    }

    @Override
    public List<ErStateCode> find(String filter, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from ErStateCode s where " +
                "(upper(s.code) like upper(:filter) " +
                "or upper(s.name) like upper(:filter)) " +
                "and s.metadata.state = :state ");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setInteger("state", ErMetaState.ACTIVE.ordinal());
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        query.setCacheable(true);
        return (List<ErStateCode>) query.list();
    }

    @Override
    public Integer count(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(s) from ErStateCode s where " +
                "(upper(s.code) like upper(:filter) " +
                "or upper(s.name) like upper(:filter)) " +
                "and s.metadata.state = :state ");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setInteger("state", ErMetaState.ACTIVE.ordinal());
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public boolean isExists(String code) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(*) from ErStateCode s where " +
                "s.code = :code " +
                "and s.metadata.state = :state ");
        query.setString("code", code);
        query.setInteger("state", ErMetaState.ACTIVE.ordinal());
        return 0 < ((Long) query.uniqueResult()).intValue();
    }
}
