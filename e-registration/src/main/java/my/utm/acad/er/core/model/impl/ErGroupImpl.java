package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.ErGroup;
import my.utm.acad.er.core.model.ErGroupMember;

import javax.persistence.*;
import java.util.Set;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "ErGroup")
@Table(name = "ER_GROP")
public class ErGroupImpl extends ErPrincipalImpl implements ErGroup {

    @ManyToMany(targetEntity = ErPrincipalImpl.class)
    @JoinTable(name = "ER_GROP_MMBR", joinColumns = {
            @JoinColumn(name = "GROUP_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "PRINCIPAL_ID",
                    nullable = false, updatable = false)})
    private Set<ErGroupMember> members;

    @Override
    public Set<ErGroupMember> getMembers() {
        return members;
    }

    @Override
    public void setMembers(Set<ErGroupMember> members) {
        this.members = members;
    }
}
