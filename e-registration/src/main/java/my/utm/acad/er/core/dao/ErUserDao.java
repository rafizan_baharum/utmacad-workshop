package my.utm.acad.er.core.dao;

import my.utm.acad.er.core.model.ErActor;
import my.utm.acad.er.core.model.ErGroup;
import my.utm.acad.er.core.model.ErUser;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface ErUserDao extends GenericDao<Long, ErUser> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    ErUser findByEmail(String email);

    ErUser findByUsername(String username);

    ErUser findByActor(ErActor actor);

    List<ErUser> find(String filter, Integer offset, Integer limit);

    List<ErGroup> findGroups(ErUser user);

    Integer count(String filter);

    // ====================================================================================================
    // CRUD
    // ====================================================================================================
    void save(ErUser user);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    boolean isExists(String username);

    boolean hasUser(ErActor actor);
}