package my.utm.acad.er.core.model;

public interface ErUser extends ErPrincipal {

    String getRealName();

    void setRealName(String realName);

    String getPassword();

    void setPassword(String password);

    ErActor getActor();

    void setActor(ErActor actor);
}
