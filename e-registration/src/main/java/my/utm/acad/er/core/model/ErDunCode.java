package my.utm.acad.er.core.model;

/**
 * DUN
 *
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface ErDunCode {

    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);

}
