package my.utm.acad.er.core.model;

import java.io.Serializable;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface ErMetaObject extends Serializable {

    Long getId();

    void setId(Long id);

    ErMetadata getMetadata();

    void setMetadata(ErMetadata metadata);
}
