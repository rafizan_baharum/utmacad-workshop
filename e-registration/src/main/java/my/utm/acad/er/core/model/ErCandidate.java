package my.utm.acad.er.core.model;

import java.util.List;

public interface ErCandidate extends ErActor {

    ErDisabilityType getDisabilityType();

    void setDisabilityType(ErDisabilityType disabilityType);

    ErSponsorType getSponsorType();

    void setSponsorType(ErSponsorType supportType);

    List<ErGuardian> getGuardians();

    void setGuardians(List<ErGuardian> guardians);

}
