package my.utm.acad.er.core.dao;


import my.utm.acad.er.core.model.ErStateCode;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface ErStateCodeDao extends GenericDao<Long, ErStateCode> {

    ErStateCode findByCode(String code);

    List<ErStateCode> find();

    List<ErStateCode> find(String filter, Integer offset, Integer limit);

    Integer count(String filter);

    boolean isExists(String code);
}
