package my.utm.acad.er.core.model;


/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public interface ErGuardian extends ErMetaObject {

    ErGuardianType getType();

    void setType(ErGuardianType guardianType);

    String getName();

    void setName(String name);

    String getSourceNo();

    void setSourceNo(String sourceNo);

    String getTelephoneNo();

    String getCellphoneNo();

    String getAddress1();

    void setAddress1(String address1);

    String getAddress2();

    void setAddress2(String address2);

    String getAddress3();

    void setAddress3(String address3);

    ErCountryCode getCountryCode();

    void setCountryCode(ErCountryCode countryCode);

    ErStateCode getStateCode();

    void setStateCode(ErStateCode stateCode);

    String getPostCode();

    void setPostCode(String postCode);
}
