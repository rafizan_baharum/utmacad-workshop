package my.utm.acad.er.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public enum ErMetaState {
    INACTIVE, // 0
    ACTIVE,   // 1

}
