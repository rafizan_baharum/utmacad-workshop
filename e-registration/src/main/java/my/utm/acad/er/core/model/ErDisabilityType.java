package my.utm.acad.er.core.model;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public enum ErDisabilityType {

    DISABLED,
    QUADRAPLEGIC,
    NO_DISABILITY;
}
