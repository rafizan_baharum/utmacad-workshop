package my.utm.acad.er.core.dao;


import my.utm.acad.er.core.model.ErUser;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface GenericDao<K, I> {

    I newInstance();

    I refresh(I i);

    I findById(K k);

    List<I> find(Integer offset, Integer limit);

    Integer count();

    void save(I entity, ErUser user);

    void saveOrUpdate(I entity, ErUser user);

    void update(I entity, ErUser user);

    void deactivate(I entity, ErUser user);

    void remove(I entity, ErUser user);

    void delete(I entity, ErUser user);

//    NOTE: available after workshop Spring Security
//    List<I> findAuthorized(Set<String> sids);
//
//    List<I> findAuthorized(Set<String> sids, Integer offset, Integer limit);
//
//    List<Long> findAuthorizedIds(Set<String> sids);

//    Integer countAuthorized(Set<String> sids);


}
