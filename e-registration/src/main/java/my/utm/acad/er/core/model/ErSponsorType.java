package my.utm.acad.er.core.model;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public enum ErSponsorType {

    FULLY_SPONSORED("I am fully sponsored by home government/employer"),
    SELF_SPONSORED("I am self-sponsored"),
    POTENTIALLY_SPONSORED("I will apply for financial aid from government employer or others");

    private String label;

    ErSponsorType(String label) {
        this.label = label;
    }
}
