package my.utm.acad.er.core.model;

public enum ErActorType {

    CANDIDATE,
    SECRETARIAT,
}
