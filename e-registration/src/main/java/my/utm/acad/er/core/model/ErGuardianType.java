package my.utm.acad.er.core.model;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public enum ErGuardianType {

    FATHER,
    MOTHER,
    GUARDIAN;

}
