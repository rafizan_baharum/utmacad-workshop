package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.*;

/**
 * @author team utamacad
 * @sErCe 4/2/2015.
 */
public class ErGuardianImpl implements ErGuardian {

    private Long id;
    private String sourceNo;
    private String name;
    private String email;
    private String cellphoneNo;
    private String telephoneNo;

    private String address1;
    private String address2;
    private String address3;
    private String postCode;

    private ErGuardianType type;
    private ErStateCode stateCode;
    private ErCountryCode countryCode;

    private ErMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getSourceNo() {
        return sourceNo;
    }

    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getCellphoneNo() {
        return cellphoneNo;
    }

    public void setCellphoneNo(String cellphoneNo) {
        this.cellphoneNo = cellphoneNo;
    }

    @Override
    public String getTelephoneNo() {
        return telephoneNo;
    }

    public void setTelephoneNo(String telephoneNo) {
        this.telephoneNo = telephoneNo;
    }

    @Override
    public String getAddress1() {
        return address1;
    }

    @Override
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @Override
    public String getAddress2() {
        return address2;
    }

    @Override
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @Override
    public String getAddress3() {
        return address3;
    }

    @Override
    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    @Override
    public String getPostCode() {
        return postCode;
    }

    @Override
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public ErGuardianType getType() {
        return type;
    }

    public void setType(ErGuardianType type) {
        this.type = type;
    }

    public ErStateCode getStateCode() {
        return stateCode;
    }

    public void setStateCode(ErStateCode stateCode) {
        this.stateCode = stateCode;
    }

    public ErCountryCode getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(ErCountryCode countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public ErMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(ErMetadata metadata) {
        this.metadata = metadata;
    }
}

