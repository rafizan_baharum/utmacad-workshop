package my.utm.acad.er.core.dao.impl;

import my.utm.acad.configuration.AbstractDaoContextBaseTest;
import my.utm.acad.er.core.dao.ErStateCodeDao;
import my.utm.acad.er.core.dao.ErUserDao;
import my.utm.acad.er.core.model.ErPrincipalType;
import my.utm.acad.er.core.model.ErStateCode;
import my.utm.acad.er.core.model.ErUser;
import my.utm.acad.er.core.model.impl.ErStateCodeImpl;
import my.utm.acad.er.core.model.impl.ErUserImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class ErStateDaoTest extends AbstractDaoContextBaseTest {

    private static final Logger LOG = LoggerFactory.getLogger(ErStateDaoTest.class);

    @Autowired
    private ErStateCodeDao stateCodeDao;

    @Autowired
    private ErUserDao userDao;
    private ErUser root;

    @Before
    public void setUp() {
        root = new ErUserImpl();
        root.setName("root");
        root.setRealName("ROOT");
        root.setPassword("abc123");
        root.setPrincipalType(ErPrincipalType.USER);
        userDao.save(root);
    }

    @After
    public void tearDown() {
    }

    @Test
    @Rollback(true)
    public void list() {
        ErStateCode stateCode = new ErStateCodeImpl();
        stateCode.setCode("JOHOR");
        stateCode.setDescription("Johor Darul Takzim");
        stateCodeDao.save(stateCode, root);

        ErStateCode johor = stateCodeDao.findByCode("JOHOR");
        Assert.assertNotNull(johor);
        LOG.debug("state code {}", johor.getDescription());
    }
}
