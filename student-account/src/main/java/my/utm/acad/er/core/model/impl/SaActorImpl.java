package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaActor;
import my.utm.acad.er.core.model.SaMetadata;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "SaActor")
@Table(name = "SA_ACTR")
@Inheritance(strategy = InheritanceType.JOINED)
public class SaActorImpl implements SaActor {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_SA_ACTR")
    @SequenceGenerator(name = "SEQ_SA_ACTR", sequenceName = "SEQ_SA_ACTR", allocationSize = 1)
    private Long id;

    @NotNull
    @Column(name = "NAME", nullable = false)
    private String name;

    @NotNull
    @Column(name = "EMAIl")
    private String email;

    @NotNull
    @Column(name = "IDENTITY_NO", nullable = false)
    private String identityNo;

    @Embedded
    private SaMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public SaMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(SaMetadata metadata) {
        this.metadata = metadata;
    }
}
