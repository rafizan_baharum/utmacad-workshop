package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaGroup;
import my.utm.acad.er.core.model.SaGroupMember;

import javax.persistence.*;
import java.util.Set;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "SaGroup")
@Table(name = "SA_GROP")
public class SaGroupImpl extends SaPrincipalImpl implements SaGroup {

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = SaGroupMemberImpl.class)
    @JoinTable(name = "SA_GROP_MMBR", joinColumns = {
            @JoinColumn(name = "GROUP_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "PRINCIPAL_ID",
                    nullable = false, updatable = false)})
    private Set<SaGroupMember> members;

    @Override
    public Set<SaGroupMember> getMembers() {
        return members;
    }

    @Override
    public void setMembers(Set<SaGroupMember> members) {
        this.members = members;
    }
}
