package my.utm.acad.er.core.model;

public interface SaPrincipal extends SaMetaObject {

    String getName();

    void setName(String name);

    SaPrincipalType getPrincipalType();

    void setPrincipalType(SaPrincipalType type);
}
