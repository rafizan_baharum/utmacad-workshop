package my.utm.acad.er.core.dao;

import my.utm.acad.er.core.model.*;

import java.util.List;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public interface SaInvoiceDao extends GenericDao<Long, SaInvoice> {

    // ====================================================================================================
    // HELPER
    // ====================================================================================================


    SaInvoice findByReferenceNo(String referenceNo);

    SaInvoice findBySourceNo(String sourceNo);

    SaInvoiceItem findItemById(Long id);

    SaInvoiceTransaction findTransactionById(Long id);

    List<SaInvoice> findManyInvoiceBySourceNo(String sourceNo);

    List<SaInvoice> find(SaStudentAccountStatement statement);

    List<SaInvoice> find(SaStudentAccountStatement statement, Integer offset, Integer limit);

    List<SaInvoice> find(String filter, SaStudentAccountStatement statement);

    List<SaInvoice> find(String filter, SaStudentAccountStatement statement, Integer offset, Integer limit);

    List<SaInvoiceItem> findItems(SaInvoice invoice);

    List<SaInvoiceItem> findItems(SaInvoice invoice, Integer offset, Integer limit);

    List<SaInvoiceTransaction> findTransactions(SaInvoice invoice);

    List<SaInvoiceTransaction> findTransactions(SaInvoice invoice, Integer offset, Integer limit);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(SaStudentAccountStatement statement);

    Integer count(String filter, SaStudentAccountStatement statement);

    Integer countItem(SaInvoice invoice);

    Integer countTransaction(SaInvoice invoice);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    void addItem(SaInvoice invoice, SaInvoiceItem item, SaUser user);

    void updateItem(SaInvoice invoice, SaInvoiceItem item, SaUser user);

    void removeItem(SaInvoice invoice, SaInvoiceItem item, SaUser user);

    void addTransaction(SaInvoice invoice, SaInvoiceTransaction transaction, SaUser user);

    void updateTransaction(SaInvoice invoice, SaInvoiceTransaction transaction, SaUser user);

    void removeTransaction(SaInvoice invoice, SaInvoiceTransaction transaction, SaUser user);
}
