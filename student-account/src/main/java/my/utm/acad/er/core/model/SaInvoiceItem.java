package my.utm.acad.er.core.model;

import java.math.BigDecimal;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public interface SaInvoiceItem extends SaMetaObject{

    String getDescription();

    void setDescription(String description);

    BigDecimal getUnit();

    void setUnit(BigDecimal unit);

    BigDecimal getPrice();

    void setPrice(BigDecimal price);

    BigDecimal getAmount();

    void setAmount(BigDecimal amount);

    SaInvoice getInvoice();

    void setInvoice(SaInvoice invoice);

}
