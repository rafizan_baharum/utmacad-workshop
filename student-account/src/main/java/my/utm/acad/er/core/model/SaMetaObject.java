package my.utm.acad.er.core.model;

import java.io.Serializable;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface SaMetaObject extends Serializable {

    Long getId();

    void setId(Long id);

    SaMetadata getMetadata();

    void setMetadata(SaMetadata metadata);
}
