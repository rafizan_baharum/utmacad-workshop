package my.utm.acad.er.core.model;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface SaBankCode extends SaMetaObject {

    String getCode();

    void setCode(String code);

    String getSwiftCode();

    void setSwiftCode(String swiftCode);

    String getIbgCode();

    void setIbgCode(String ibgCode);

    String getDescription();

    void setDescription(String description);
}
