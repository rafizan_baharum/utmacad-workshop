package my.utm.acad.er.core.model;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public interface SaSponsorshipCode {

    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);

}
