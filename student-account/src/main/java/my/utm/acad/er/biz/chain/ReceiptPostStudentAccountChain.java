package my.utm.acad.er.biz.chain;

import my.utm.acad.er.core.model.*;
import my.utm.acad.er.core.model.impl.SaStudentAccountTransactionImpl;

import java.util.List;

/**
 * @author team utmacad
 * @since 4/2/2015.
 */
public class ReceiptPostStudentAccountChain {

    public boolean process(ChainContext context) {

        SaDocument document = context.getDocument();

        if (!(document instanceof SaReceipt))
            return false;

        SaReceipt receipt = ((SaReceipt) document);
        List<SaReceiptPayment> payments = receipt.getPayments();
        for (SaReceiptPayment payment : payments) {
            SaStudentAccount account = payment.getAccount();
            SaAcademicSession session = payment.getSession();
            List<SaReceiptPaymentItem> items = payment.getItems();
            for (SaReceiptPaymentItem item : items) {
                // create transaction
                SaStudentAccountTransaction transaction = new SaStudentAccountTransactionImpl();
                transaction.setSession(session);
                transaction.setAmount(item.getAmount());
                transaction.setSourceNo(receipt.getReferenceNo());
                transaction.setAccount(account);
                transaction.setTransactionCode(SaStudentAccountTransactionCode.RECEIPT);
            }
        }
        return true;
    }
}
