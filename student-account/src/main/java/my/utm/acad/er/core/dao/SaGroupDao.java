package my.utm.acad.er.core.dao;


import my.utm.acad.er.core.model.SaGroup;
import my.utm.acad.er.core.model.SaPrincipal;
import my.utm.acad.er.core.model.SaPrincipalType;
import my.utm.acad.er.core.model.SaUser;

import java.util.List;

public interface SaGroupDao extends GenericDao<Long, SaGroup> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    SaGroup findByName(String name);

    List<SaGroup> findAll();

    List<SaGroup> findImmediate(SaPrincipal principal);

    List<SaGroup> findImmediate(SaPrincipal principal, Integer offset, Integer limit);

//    Set<SaGroup> findEffectiveAsNative(SaPrincipal principal);

    List<SaGroup> findAvailableGroups(SaUser user);

    List<SaPrincipal> findAvailableMembers(SaGroup group);

    List<SaPrincipal> findMembers(SaGroup group, SaPrincipalType principalType);

    List<SaPrincipal> findMembers(SaGroup group);

    List<SaPrincipal> findMembers(SaGroup group, Integer offset, Integer limit);

    List<SaGroup> find(String filter, Integer offset, Integer limit);

    List<SaGroup> findMemberships(SaPrincipal principal);

    Integer count(String filter);

    // ====================================================================================================
    // CRUD
    // ====================================================================================================

    void addMember(SaGroup group, SaPrincipal member, SaUser user);

    void addMembers(SaGroup group, List<SaPrincipal> members, SaUser user);

    void removeMember(SaGroup group, SaPrincipal member);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    boolean hasMembership(SaGroup group, SaPrincipal principal);

    boolean isExists(String name);
}
