package my.utm.acad.er.core.model;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface SaReceipt extends SaDocument {

    BigDecimal getTotalAmount();

    void setTotalAmount(BigDecimal totalAmount);

    SaReceiptType getReceiptType();

    void setReceiptType(SaReceiptType type);

    List<SaReceiptPayment> getPayments();

    void setPayments(List<SaReceiptPayment> payments);
}
