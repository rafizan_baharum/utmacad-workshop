package my.utm.acad.er.biz;

import my.utm.acad.er.core.model.SaDocument;
import my.utm.acad.er.core.model.SaReceipt;

/**
 * @author team utmacad
 * @since 4/2/2015.
 */
public interface PaymentManager {

    void processReceipt(SaReceipt receipt);

    void post(SaDocument document);
}
