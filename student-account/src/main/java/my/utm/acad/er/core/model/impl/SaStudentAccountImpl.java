package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaStudent;
import my.utm.acad.er.core.model.SaStudentAccount;
import my.utm.acad.er.core.model.SaStudentAccountTransaction;

import java.util.List;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class SaStudentAccountImpl implements SaStudentAccount {

    private Long id;
    private String code;
    private SaStudent student;
    private List<SaStudentAccountTransaction> transactions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public SaStudent getStudent() {
        return student;
    }

    @Override
    public void setStudent(SaStudent student) {
        this.student = student;
    }

    @Override
    public List<SaStudentAccountTransaction> getTransactions() {
        return transactions;
    }

    @Override
    public void setTransactions(List<SaStudentAccountTransaction> transactions) {
        this.transactions = transactions;
    }
}
