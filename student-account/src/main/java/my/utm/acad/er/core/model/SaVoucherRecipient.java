package my.utm.acad.er.core.model;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author team utmacad
 * @since 8/2/2015.
 */
public interface SaVoucherRecipient extends SaMetaObject {

    BigDecimal getTotalAmount();

    void setTotalAmount(BigDecimal totalAmount);

    SaVoucher getVoucher();

    void setVoucher(SaVoucher voucher);

    List<SaVoucherRecipientItem> getItems();

    void setItems(List<SaVoucherRecipientItem> items);
}
