package my.utm.acad.er.core.dao;


import my.utm.acad.er.core.model.SaFacultyCode;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface SaFacultyCodeDao extends GenericDao<Long, SaFacultyCode> {

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    SaFacultyCode findByCode(String code);

    List<SaFacultyCode> find();

    List<SaFacultyCode> find(String filter, Integer offset, Integer limit);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(String filter);

    boolean isExists(String code);
}
