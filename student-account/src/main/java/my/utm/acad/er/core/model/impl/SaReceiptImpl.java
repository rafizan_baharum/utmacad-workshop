package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaMetadata;
import my.utm.acad.er.core.model.SaReceipt;
import my.utm.acad.er.core.model.SaReceiptPayment;
import my.utm.acad.er.core.model.SaReceiptType;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class SaReceiptImpl implements SaReceipt {

    private Long id;
    private String referenceNo;
    private String sourceNo;
    private String description;
    private BigDecimal totalAmount;
    private SaReceiptType receiptType;

    private List<SaReceiptPayment> payments;
    private SaMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getReferenceNo() {
        return referenceNo;
    }

    @Override
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    @Override
    public String getSourceNo() {
        return sourceNo;
    }

    @Override
    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    @Override
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public SaReceiptType getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(SaReceiptType receiptType) {
        this.receiptType = receiptType;
    }

    @Override
    public List<SaReceiptPayment> getPayments() {
        return payments;
    }

    @Override
    public void setPayments(List<SaReceiptPayment> payments) {
        this.payments = payments;
    }

    @Override
    public SaMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SaMetadata metadata) {
        this.metadata = metadata;
    }
}
