package my.utm.acad.er.core.model;

/**
 * @author team utmacad
 * @since 14/2/2015.
 */
public enum SaReceiptType {

    COUNTER,
    ELECTRONIC;
}
