package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaGroup;
import my.utm.acad.er.core.model.SaGroupMember;
import my.utm.acad.er.core.model.SaMetadata;
import my.utm.acad.er.core.model.SaPrincipal;

import javax.persistence.*;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "SaGroupMember")
@Table(name = "SA_GROP_MMBR")
public class SaGroupMemberImpl implements SaGroupMember {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_SA_GROP_MMBR")
    @SequenceGenerator(name = "SEQ_SA_GROP_MMBR", sequenceName = "SEQ_SA_GROP_MMBR", allocationSize = 1)
    private Long id;

    @OneToOne(targetEntity = SaGroupImpl.class)
    @JoinColumn(name = "GROUP_ID")
    private SaGroup group;

    @OneToOne(targetEntity = SaPrincipalImpl.class)
    @JoinColumn(name = "PRINCIPAL_ID")
    private SaPrincipal principal;

    @Embedded
    private SaMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public SaGroup getGroup() {
        return group;
    }

    @Override
    public void setGroup(SaGroup group) {
        this.group = group;
    }

    @Override
    public SaPrincipal getPrincipal() {
        return principal;
    }

    @Override
    public void setPrincipal(SaPrincipal principal) {
        this.principal = principal;
    }

    @Override
    public SaMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(SaMetadata metadata) {
        this.metadata = metadata;
    }
}
