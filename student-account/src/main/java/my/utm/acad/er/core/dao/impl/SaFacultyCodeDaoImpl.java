package my.utm.acad.er.core.dao.impl;

import my.utm.acad.er.core.dao.GenericDaoSupport;
import my.utm.acad.er.core.dao.SaFacultyCodeDao;
import my.utm.acad.er.core.model.SaFacultyCode;
import my.utm.acad.er.core.model.SaMetaState;
import my.utm.acad.er.core.model.impl.SaFacultyCodeImpl;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Repository("saFacultyCode")
public class SaFacultyCodeDaoImpl extends GenericDaoSupport<Long, SaFacultyCode> implements SaFacultyCodeDao {

    private static final Logger LOG = LoggerFactory.getLogger(SaFacultyCodeDaoImpl.class);

    public SaFacultyCodeDaoImpl() {
        super(SaFacultyCodeImpl.class);
    }

    @Override
    public SaFacultyCode findByCode(String code) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from SaFacultyCode s where s.code = :code and  " +
                " s.metadata.state = :state");
        query.setString("code", code);
        query.setCacheable(true);
        query.setInteger("state", SaMetaState.ACTIVE.ordinal());
        return (SaFacultyCode) query.uniqueResult();
    }

    @Override
    public List<SaFacultyCode> find() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from SaFacultyCode s where " +
                " s.metadata.state = :state ");
        query.setInteger("state", SaMetaState.ACTIVE.ordinal());
        query.setCacheable(true);
        return (List<SaFacultyCode>) query.list();
    }

    @Override
    public List<SaFacultyCode> find(String filter, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from SaFacultyCode s where " +
                "(upper(s.code) like upper(:filter) " +
                "or upper(s.description) like upper(:filter)) " +
                "and s.metadata.state = :state ");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setInteger("state", SaMetaState.ACTIVE.ordinal());
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        query.setCacheable(true);
        return (List<SaFacultyCode>) query.list();
    }

    @Override
    public Integer count(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(s) from SaFacultyCode s where " +
                "(upper(s.code) like upper(:filter) " +
                "or upper(s.description) like upper(:filter)) " +
                "and s.metadata.state = :state ");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setInteger("state", SaMetaState.ACTIVE.ordinal());
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public boolean isExists(String code) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(*) from SaFacultyCode s where " +
                "s.code = :code " +
                "and s.metadata.state = :state ");
        query.setString("code", code);
        query.setInteger("state", SaMetaState.ACTIVE.ordinal());
        return 0 < ((Long) query.uniqueResult()).intValue();
    }
}
