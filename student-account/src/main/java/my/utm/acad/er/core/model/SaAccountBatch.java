package my.utm.acad.er.core.model;

import java.util.List;

/**
 * payment batch
 *
 * @author team utmacad
 * @since 8/2/2015.
 */
public interface SaAccountBatch extends SaMetaObject {

    String getReferenceNo();

    void setReferenceNo(String referenceNo);

    SaAcademicSession getSession();

    void setSession(SaAcademicSession session);

    List<SaStudentAccount> getAccounts();

    void setAccounts(List<SaStudentAccount> accounts);
}
