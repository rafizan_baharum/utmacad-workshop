package my.utm.acad.er.core.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public interface SaInvoice extends SaDocument {

    BigDecimal getTotalAmount();

    void setTotalAmount(BigDecimal totalAmount);

    BigDecimal getAmendedAmount();

    void setAmendedAmount(BigDecimal amendedAmount);

    BigDecimal getBalanceAmount();

    void setBalanceAmount(BigDecimal balanceAmount);

    boolean isPaid();

    void setPaid(boolean paid);

    Date getIssuedDate();

    void setIssuedDate(Date issuedDate);

    SaFacultyCode getFacultyCode();

    void setFacultyCode(SaFacultyCode facultyCode);

    SaStudentAccountStatement getStatement();

    void setStatement(SaStudentAccountStatement statement);

    SaAcademicSession getSession();

    void setSession(SaAcademicSession session);

    List<SaInvoiceTransaction> getTransactions();

    void setTransactions(List<SaInvoiceTransaction> transactions);

    List<SaInvoiceItem> getItems();

    void setItems(List<SaInvoiceItem> items);
}
