package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaAcademicSession;
import my.utm.acad.er.core.model.SaAccountBatch;
import my.utm.acad.er.core.model.SaMetadata;
import my.utm.acad.er.core.model.SaStudentAccount;

import java.util.List;

/**
 * Account Batch for payment per academic session
 *
 * @author team utmacad
 * @since 8/2/2015.
 */
public class SaAccountBatchImpl implements SaAccountBatch {

    private Long id;
    private String referenceNo;
    private SaAcademicSession session;
    private List<SaStudentAccount> accounts;
    private SaMetadata metadata;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getReferenceNo() {
        return referenceNo;
    }

    @Override
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public SaAcademicSession getSession() {
        return session;
    }

    public void setSession(SaAcademicSession session) {
        this.session = session;
    }

    @Override
    public List<SaStudentAccount> getAccounts() {
        return accounts;
    }

    @Override
    public void setAccounts(List<SaStudentAccount> accounts) {
        this.accounts = accounts;
    }

    @Override
    public SaMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SaMetadata metadata) {
        this.metadata = metadata;
    }
}
