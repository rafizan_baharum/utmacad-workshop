package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaMetadata;
import my.utm.acad.er.core.model.SaVoucher;
import my.utm.acad.er.core.model.SaVoucherRecipient;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author team utmacad
 * @since 14/2/2015.
 */
public class SaVoucherImpl implements SaVoucher {

    private Long id;
    private String referenceNo;
    private String sourceNo;
    private String description;
    private BigDecimal totalAmount;

    private List<SaVoucherRecipient> recipients;

    private SaMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getReferenceNo() {
        return referenceNo;
    }

    @Override
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    @Override
    public String getSourceNo() {
        return sourceNo;
    }

    @Override
    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    @Override
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public List<SaVoucherRecipient> getRecipients() {
        return recipients;
    }

    @Override
    public void setRecipients(List<SaVoucherRecipient> recipients) {
        this.recipients = recipients;
    }

    @Override
    public SaMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SaMetadata metadata) {
        this.metadata = metadata;
    }
}
