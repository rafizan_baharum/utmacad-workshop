package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaItemCode;
import my.utm.acad.er.core.model.SaReceiptPayment;
import my.utm.acad.er.core.model.SaReceiptPaymentItem;

import java.math.BigDecimal;

/**
 * @author team utmacad
 * @since 4/2/2015.
 */
public class SaReceiptPaymentItemImpl implements SaReceiptPaymentItem {

    private Long id;
    private String description;
    private BigDecimal amount;
    private SaItemCode itemCode;
    private SaReceiptPayment payment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public SaItemCode getItemCode() {
        return itemCode;
    }

    @Override
    public void setItemCode(SaItemCode itemCode) {
        this.itemCode = itemCode;
    }

    @Override
    public SaReceiptPayment getPayment() {
        return payment;
    }

    public void setPayment(SaReceiptPayment payment) {
        this.payment = payment;
    }
}
