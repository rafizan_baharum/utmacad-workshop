package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaMetadata;
import my.utm.acad.er.core.model.SaProgramCode;

import javax.persistence.*;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
@Entity(name = "SaProgramCode")
@Table(name = "SA_PRGM_CODE")
public class SaProgramCodeImpl implements SaProgramCode {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_SA_PRGM_CODE")
    @SequenceGenerator(name = "SEQ_SA_PRGM_CODE", sequenceName = "SEQ_SA_PRGM_CODE", allocationSize = 1)
    private Long id;

    @Column(name = "CODE", unique = true, nullable = false)
    private String code;

    @Column(name = "SWIFT_CODE", unique = true, nullable = false)
    private String swiftCode;

    @Column(name = "IBG_CODE", unique = true, nullable = false)
    private String ibgCode;

    @Column(name = "DESCRIPTION")
    private String description;

    @Embedded
    private SaMetadata metadata;


    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    public SaMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(SaMetadata metadata) {
        this.metadata = metadata;
    }
}
