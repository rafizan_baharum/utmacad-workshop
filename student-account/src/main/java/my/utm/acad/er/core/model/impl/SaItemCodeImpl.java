package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaItemCode;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class SaItemCodeImpl implements SaItemCode {

    private Long id;
    private String code;
    private String description;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }
}
