package my.utm.acad.er.core.model;

/**
 * @author team utmacad
 * @since 8/2/2015.
 */
public interface SaSponsor extends SaMetaObject{

    String getCode();

    void setCode(String code);

    String getAccountNo();

    void setAccountNo(String accountNo);
}
