package my.utm.acad.er.core.model;

public enum SaActorType {

    CANDIDATE,
    SECRETARIAT,
}
