package my.utm.acad.er.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface SaActor extends SaObject {

    String getName();

    void setName(String name);

    String getEmail();

    void setEmail(String email);
}
