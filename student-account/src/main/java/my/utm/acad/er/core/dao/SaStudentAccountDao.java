package my.utm.acad.er.core.dao;

import my.utm.acad.er.core.model.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public interface SaStudentAccountDao extends GenericDao<Long, SaStudentAccountDao> {

    // ====================================================================================================
    // HELPER
    // ====================================================================================================


    SaStudentAccount findByCode(String code);

    SaStudentAccountStatement findStatementById(Long id);

    SaStudentAccount findByStudent(SaStudent student);

    SaStudentAccountTransaction findTransactionById(Long id);

    List<SaStudentAccount> find();

    List<SaStudentAccount> find(String filter, Integer offset, Integer limit);

    List<SaStudentAccountStatement> findStatements(SaStudentAccount account);

    List<SaStudentAccountStatement> findStatements(SaStudentAccount account, Integer limit, Integer offset);

    List<SaStudentAccountTransaction> findTransactions(String sourceNo, SaStudentAccount account);

    List<SaStudentAccountTransaction> findTransactions(SaStudent student);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count();

    Integer count(String filter);

    Integer countStatement(SaStudentAccount account);

    Integer countTransaction(SaStudentAccount account);

    BigDecimal sumBalanceAmount(SaStudentAccount account);

    BigDecimal sumSurplusAmount(SaStudentAccount account);

    // check
    boolean hasAccount(SaStudent student);

    boolean hasStatement(SaStudentAccount account);

    boolean hasTransaction(SaStudentAccount account);

    boolean hasSurplus(SaStudentAccount account);

    // ====================================================================================================
    // CRUD
    // ====================================================================================================

    void addStatement(SaStudentAccount account, SaStudentAccountStatement statement, SaUser user);

    void updateStatement(SaStudentAccount account, SaStudentAccountStatement statement, SaUser user);

    void removeStatement(SaStudentAccount account, SaStudentAccountStatement statement, SaUser user);

    void addTransaction(SaStudentAccount account, SaStudentAccountTransaction transaction, SaUser user);

    void updateTransaction(SaStudentAccount account, SaStudentAccountTransaction transaction, SaUser user);

    void removeTransaction(SaStudentAccount account, SaStudentAccountTransaction transaction, SaUser user);

    void deleteTransaction(SaStudentAccount account, String sourceNo, SaUser user);
}
