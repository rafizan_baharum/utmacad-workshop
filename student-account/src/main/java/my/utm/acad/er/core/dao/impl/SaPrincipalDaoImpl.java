package my.utm.acad.er.core.dao.impl;

import my.utm.acad.er.core.dao.GenericDaoSupport;
import my.utm.acad.er.core.dao.SaPrincipalDao;
import my.utm.acad.er.core.model.SaPrincipal;
import my.utm.acad.er.core.model.SaPrincipalType;
import my.utm.acad.er.core.model.impl.SaPrincipalImpl;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Repository("saPrincipalDao")
public class SaPrincipalDaoImpl extends GenericDaoSupport<Long, SaPrincipal> implements SaPrincipalDao {

    private static final Logger LOG = Logger.getLogger(SaPrincipalDaoImpl.class);

    public SaPrincipalDaoImpl() {
        super(SaPrincipalImpl.class);
    }

    @Override
    public List<SaPrincipal> findAllPrincipals() {
        Session session = sessionFactory.getCurrentSession();
        List<SaPrincipal> results = new ArrayList<SaPrincipal>();
        Query query = session.createQuery("select p from SaUser p order by p.name");
        results.addAll((List<SaPrincipal>) query.list());

        Query queryGroup = session.createQuery("select p from UsGroup p order by p.name ");
        results.addAll((List<SaPrincipal>) queryGroup.list());

        return results;
    }

    @Override
    public List<SaPrincipal> find(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select p from SaPrincipal p where p.name like :filter order by p.name");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        return query.list();
    }

    @Override
    public List<SaPrincipal> find(String filter, SaPrincipalType type) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select p from SaPrincipal p where " +
                "p.name like :filter " +
                "and p.principalType = :principalType " +
                "order by p.name");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setInteger("principalType", type.ordinal());
        return query.list();
    }

    @Override
    public List<SaPrincipal> find(String filter, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select p from SaPrincipal p where " +
                "p.id in (" +
                "select u.id from InUser u where " +
                "(upper(u.name) like upper(:filter)" +
                "or upper(u.name) like upper(:filter))" +
                ") " +
                "or p.id in (select g.id from InGroup g  where upper(g.name) like upper(:filter)) " +
                "order by p.name");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return query.list();
    }

    @Override
    public Integer count(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(u) from SaPrincipal u where " +
                "u.name like :filter ");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public SaPrincipal findByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select p from SaPrincipal p where " +
                "upper(p.name) = upper(:name) ");
        query.setString("name", name);
        return (SaPrincipal) query.uniqueResult();
    }
}
