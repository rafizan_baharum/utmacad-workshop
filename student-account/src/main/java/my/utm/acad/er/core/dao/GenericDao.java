package my.utm.acad.er.core.dao;


import my.utm.acad.er.core.model.SaUser;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface GenericDao<K, I> {

    I newInstance();

    I refresh(I i);

    I findById(K k);

    List<I> find(Integer offset, Integer limit);

    Integer count();

    void save(I entity, SaUser user);

    void saveOrUpdate(I entity, SaUser user);

    void update(I entity, SaUser user);

    void deactivate(I entity, SaUser user);

    void remove(I entity, SaUser user);

    void delete(I entity, SaUser user);

//    NOTE: available after workshop Spring Security
//    List<I> findAuthorized(Set<String> sids);
//
//    List<I> findAuthorized(Set<String> sids, Integer offset, Integer limit);
//
//    List<Long> findAuthorizedIds(Set<String> sids);

//    Integer countAuthorized(Set<String> sids);


}
