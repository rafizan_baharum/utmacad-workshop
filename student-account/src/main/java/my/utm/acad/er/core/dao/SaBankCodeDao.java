package my.utm.acad.er.core.dao;


import my.utm.acad.er.core.model.SaBankCode;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface SaBankCodeDao extends GenericDao<Long, SaBankCode> {

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    SaBankCode findByCode(String code);

    List<SaBankCode> find();

    List<SaBankCode> find(String filter, Integer offset, Integer limit);


    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(String filter);

    boolean isExists(String code);
}
