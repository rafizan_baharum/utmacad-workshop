package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaBankCode;
import my.utm.acad.er.core.model.SaMetadata;

import javax.persistence.*;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
@Entity(name = "SaBankCode")
@Table(name = "SA_BANK_CODE")
public class SaBankCodeImpl implements SaBankCode {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_SA_BANK_CODE")
    @SequenceGenerator(name = "SEQ_SA_BANK_CODE", sequenceName = "SEQ_SA_BANK_CODE", allocationSize = 1)
    private Long id;

    @Column(name = "CODE", unique = true, nullable = false)
    private String code;

    @Column(name = "SWIFT_CODE", unique = true, nullable = false)
    private String swiftCode;

    @Column(name = "IBG_CODE", unique = true, nullable = false)
    private String ibgCode;

    @Column(name = "DESCRIPTION")
    private String description;

    @Embedded
    private SaMetadata metadata;


    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getSwiftCode() {
        return swiftCode;
    }

    @Override
    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    @Override
    public String getIbgCode() {
        return ibgCode;
    }

    @Override
    public void setIbgCode(String ibgCode) {
        this.ibgCode = ibgCode;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    public SaMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(SaMetadata metadata) {
        this.metadata = metadata;
    }
}
