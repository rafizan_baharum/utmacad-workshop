package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class SaReceiptPaymentImpl implements SaReceiptPayment {

    private Long id;
    private BigDecimal amount;
    private SaReceiptPaymentType paymentType;

    private SaReceipt receipt;
    private SaStudentAccount account;
    private SaAcademicSession session;
    private List<SaReceiptPaymentItem> items;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public SaReceiptPaymentType getPaymentType() {
        return paymentType;
    }

    @Override
    public void setPaymentType(SaReceiptPaymentType paymentType) {
        this.paymentType = paymentType;
    }

    @Override
    public SaReceipt getReceipt() {
        return receipt;
    }

    @Override
    public void setReceipt(SaReceipt receipt) {
        this.receipt = receipt;
    }

    @Override
    public SaStudentAccount getAccount() {
        return account;
    }

    @Override
    public void setAccount(SaStudentAccount account) {
        this.account = account;
    }

    @Override
    public SaAcademicSession getSession() {
        return session;
    }

    @Override
    public void setSession(SaAcademicSession session) {
        this.session = session;
    }

    @Override
    public List<SaReceiptPaymentItem> getItems() {
        return items;
    }

    @Override
    public void setItems(List<SaReceiptPaymentItem> items) {
        this.items = items;
    }
}
