package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaAcademicSession;
import my.utm.acad.er.core.model.SaStudentAccount;
import my.utm.acad.er.core.model.SaStudentAccountTransaction;
import my.utm.acad.er.core.model.SaStudentAccountTransactionCode;

import java.math.BigDecimal;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class SaStudentAccountTransactionImpl implements SaStudentAccountTransaction {

    private Long id;
    private String sourceNo;
    private BigDecimal amount;
    private SaStudentAccountTransactionCode transactionCode;
    private SaAcademicSession session;
    private SaStudentAccount account;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getSourceNo() {
        return sourceNo;
    }

    @Override
    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public SaStudentAccountTransactionCode getTransactionCode() {
        return transactionCode;
    }

    @Override
    public void setTransactionCode(SaStudentAccountTransactionCode transactionCode) {
        this.transactionCode = transactionCode;
    }

    @Override
    public SaAcademicSession getSession() {
        return session;
    }

    @Override
    public void setSession(SaAcademicSession academicSession) {
        this.session = academicSession;
    }

    public SaStudentAccount getAccount() {
        return account;
    }

    public void setAccount(SaStudentAccount account) {
        this.account = account;
    }
}
