package my.utm.acad.er.core.model;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface SaStudent extends SaObject {

    String getMatricNo();

    void setMatricNo(String matricNo);

    String getNricNo();

    void setNricNo(String nricNo);

    String getPassportNo();

    void setPassportNo(String passportNo);

    String getName();

    void setName(String name);

    String getEmail();

    void setEmail(String email);

    String getAccountNo();

    void setAccountNo(String accountNo);

    SaBankCode getBankCode();

    void setBankCode(SaBankCode bankCode);
}
