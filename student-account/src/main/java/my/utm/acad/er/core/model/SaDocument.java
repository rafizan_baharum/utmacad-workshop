package my.utm.acad.er.core.model;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface SaDocument extends SaMetaObject {

    String getReferenceNo();

    void setReferenceNo(String referenceNo);

    String getSourceNo();

    void setSourceNo(String sourceNo);

    String getDescription();

    void setDescription(String description);

}
