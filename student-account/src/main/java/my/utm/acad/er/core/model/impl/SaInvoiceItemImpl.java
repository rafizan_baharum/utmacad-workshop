package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaInvoice;
import my.utm.acad.er.core.model.SaInvoiceItem;
import my.utm.acad.er.core.model.SaMetadata;

import java.math.BigDecimal;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public class SaInvoiceItemImpl implements SaInvoiceItem {

    private Long id;
    private String description;
    private BigDecimal unit;
    private BigDecimal price;
    private BigDecimal amount;
    private SaInvoice invoice;

    private SaMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public BigDecimal getUnit() {
        return unit;
    }

    @Override
    public void setUnit(BigDecimal unit) {
        this.unit = unit;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public SaInvoice getInvoice() {
        return invoice;
    }

    @Override
    public void setInvoice(SaInvoice invoice) {
        this.invoice = invoice;
    }

    @Override
    public SaMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SaMetadata metadata) {
        this.metadata = metadata;
    }
}
