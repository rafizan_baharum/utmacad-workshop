package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaMetadata;
import my.utm.acad.er.core.model.SaPrincipal;
import my.utm.acad.er.core.model.SaPrincipalType;

import javax.persistence.*;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Entity(name = "SaPrincipal")
@Table(name = "SA_PCPL")
@Inheritance(strategy = InheritanceType.JOINED)
public class SaPrincipalImpl implements SaPrincipal {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "SEQ_SA_PCPL")
    @SequenceGenerator(name = "SEQ_SA_PCPL", sequenceName = "SEQ_SA_PCPL", allocationSize = 1)
    private Long id;

    @Column(name = "NAME", unique = true, nullable = false)
    private String name;

    @Column(name = "PRINCIPAL_TYPE")
    private SaPrincipalType principalType;

    @Embedded
    private SaMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public SaPrincipalType getPrincipalType() {
        return principalType;
    }

    @Override
    public void setPrincipalType(SaPrincipalType principalType) {
        this.principalType = principalType;
    }

    @Override
    public SaMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SaMetadata metadata) {
        this.metadata = metadata;
    }
}
