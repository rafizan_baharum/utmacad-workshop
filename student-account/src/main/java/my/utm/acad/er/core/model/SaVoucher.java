package my.utm.acad.er.core.model;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author team utmacad
 * @since 8/2/2015.
 */
public interface SaVoucher extends SaDocument{

    BigDecimal getTotalAmount();

    void setTotalAmount(BigDecimal totalAmount);

    List<SaVoucherRecipient> getRecipients();

    void setRecipients(List<SaVoucherRecipient> recipients);

}
