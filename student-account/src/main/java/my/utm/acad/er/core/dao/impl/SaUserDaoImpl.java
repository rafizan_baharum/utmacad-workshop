package my.utm.acad.er.core.dao.impl;

import my.utm.acad.er.core.dao.GenericDaoSupport;
import my.utm.acad.er.core.dao.SaUserDao;
import my.utm.acad.er.core.model.*;
import my.utm.acad.er.core.model.impl.SaUserImpl;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Repository("saUserDao")
public class SaUserDaoImpl extends GenericDaoSupport<Long, SaUser> implements SaUserDao {

    private static final Logger LOG = Logger.getLogger(SaUserDaoImpl.class);

    public SaUserDaoImpl() {
        super(SaUserImpl.class);
    }

    // =============================================================================
    // FINDER METHODS
    // =============================================================================

    @Override
    public List<SaGroup> findGroups(SaUser user) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select r from SaGroup r inner join r.members m where m.id = :id");
        query.setLong("id", user.getId());
        return (List<SaGroup>) query.list();
    }

    @Override
    public SaUser findByUsername(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select u from SaUser u where u.name = :username ");
        query.setString("username", username);
        return (SaUser) query.uniqueResult();
    }

    @Override
    public SaUser findByActor(SaActor actor) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select u from SaUser u where u.actor = :actor ");
        query.setEntity("actor", actor);
        return (SaUser) query.uniqueResult();
    }

    @Override
    public List<SaUser> find(String filter, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from SaUser s where (" +
                "upper(s.name) like upper(:filter) " +
                "or upper(s.name) like upper(:filter)) " +
                "order by s.name, s.name");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return (List<SaUser>) query.list();
    }

    @Override
    public Integer count() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(u) from SaUser u");
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public Integer count(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(s) from SaUser s where " +
                "upper(s.name) like upper(:filter) " +
                "or upper(s.name) like upper(:filter)");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public void save(SaUser user) {
        Validate.notNull(user, "User cannot be null");
        try {
            Session session = sessionFactory.getCurrentSession();
            SaMetadata metadata = new SaMetadata();
            metadata.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            metadata.setCreatorId(0L);
            metadata.setState(SaMetaState.ACTIVE);
            user.setMetadata(metadata);
            session.save(user);
        } catch (HibernateException e) {
            LOG.debug("error occurred", e);
        }
    }

    // =============================================================================
    // HELPER
    // =============================================================================

    @Override
    public boolean isExists(String username) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(*) from SaUser u where " +
                "upper(u.name) = upper(:username) ");
        query.setString("username", username);
        return 0 < ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public boolean hasUser(SaActor actor) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(*) from SaUser u where " +
                "u.actor = :actor");
        query.setEntity("actor", actor);
        return 0 < ((Long) query.uniqueResult()).intValue();
    }

}
