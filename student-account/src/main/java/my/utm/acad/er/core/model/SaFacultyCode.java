package my.utm.acad.er.core.model;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public interface SaFacultyCode extends SaMetaObject {

    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);

}
