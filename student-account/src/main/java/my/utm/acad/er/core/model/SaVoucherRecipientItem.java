package my.utm.acad.er.core.model;

import java.math.BigDecimal;

/**
 * @author team utmacad
 * @since 8/2/2015.
 */
public interface SaVoucherRecipientItem extends SaMetaObject {

    BigDecimal getAmount();

    void setAmount(BigDecimal amount);

    SaItemCode getItemCode();

    void setItemCode(SaItemCode itemCode);

    SaVoucherRecipient getRecipient();

    void setRecipient(SaVoucherRecipient recipient);
}
