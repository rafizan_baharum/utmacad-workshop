package my.utm.acad.er.core.dao;


import my.utm.acad.er.core.model.SaProgramCode;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public interface SaProgramCodeDao extends GenericDao<Long, SaProgramCode> {

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    SaProgramCode findByCode(String code);

    List<SaProgramCode> find();

    List<SaProgramCode> find(String filter, Integer offset, Integer limit);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(String filter);

    boolean isExists(String code);
}
