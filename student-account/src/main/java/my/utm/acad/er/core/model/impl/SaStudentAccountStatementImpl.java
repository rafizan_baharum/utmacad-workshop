package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.*;

import java.util.List;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public class SaStudentAccountStatementImpl implements SaStudentAccountStatement {

    private Long id;
    private String referenceNo;
    private String sourceNo;
    private String description;
    private SaStudentAccount account;
    private SaAcademicSession session;
    private List<SaInvoice> invoices;

    private SaMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getReferenceNo() {
        return referenceNo;
    }

    @Override
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    @Override
    public String getSourceNo() {
        return sourceNo;
    }

    @Override
    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public SaStudentAccount getAccount() {
        return account;
    }

    @Override
    public void setAccount(SaStudentAccount account) {
        this.account = account;
    }

    @Override
    public SaAcademicSession getSession() {
        return session;
    }

    @Override
    public void setSession(SaAcademicSession session) {
        this.session = session;
    }

    public List<SaInvoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<SaInvoice> invoices) {
        this.invoices = invoices;
    }

    @Override
    public SaMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SaMetadata metadata) {
        this.metadata = metadata;
    }
}
