package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaAcademicSession;

import javax.persistence.*;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
@Entity(name = "SaAcademicSessionImpl")
@Table(name = "SA_ACDM_SESN")
public class SaAcademicSessionImpl implements SaAcademicSession {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ACDM_SESN")
    @SequenceGenerator(name = "SEQ_ACDM_SESN", sequenceName = "SEQ_ACDM_SESN", allocationSize = 1)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "CODE", nullable = false)
    private String code;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }
}
