package my.utm.acad.er.core.model;

public interface SaUser extends SaPrincipal {

    String getRealName();

    void setRealName(String realName);

    String getPassword();

    void setPassword(String password);

    SaActor getActor();

    void setActor(SaActor actor);
}
