package my.utm.acad.er.core.model;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface SaReceiptPayment {

    BigDecimal getAmount();

    void setAmount(BigDecimal amount);

    SaReceiptPaymentType getPaymentType();

    void setPaymentType(SaReceiptPaymentType paymentType);

    SaReceipt getReceipt();

    void setReceipt(SaReceipt receipt);

    SaStudentAccount getAccount();

    void setAccount(SaStudentAccount account);

    SaAcademicSession getSession();

    void setSession(SaAcademicSession session);

    List<SaReceiptPaymentItem> getItems();

    void setItems(List<SaReceiptPaymentItem> items);
}
