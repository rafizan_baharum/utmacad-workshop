package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaSponsorshipCode;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class SaSponsorshipCodeImpl implements SaSponsorshipCode {

    private String code;
    private String description;

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }
}
