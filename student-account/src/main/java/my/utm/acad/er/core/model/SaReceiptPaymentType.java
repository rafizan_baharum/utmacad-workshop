package my.utm.acad.er.core.model;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public enum SaReceiptPaymentType {

    CASH,
    CREDIT_CARD,
    CHEQUE,
    E_PAYMENT;

}
