package my.utm.acad.er.core.model;

import java.math.BigDecimal;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface SaStudentAccountTransaction extends SaObject{

    String getSourceNo();

    void setSourceNo(String sourceNo);

    BigDecimal getAmount();

    void setAmount(BigDecimal amount);

    SaStudentAccountTransactionCode getTransactionCode();

    void setTransactionCode(SaStudentAccountTransactionCode transactionCode);

    SaStudentAccount getAccount();

    void setAccount(SaStudentAccount account);

    SaAcademicSession getSession();

    void setSession(SaAcademicSession academicSession);


}
