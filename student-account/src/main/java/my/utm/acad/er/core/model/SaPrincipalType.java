package my.utm.acad.er.core.model;

public enum SaPrincipalType {
    USER,
    GROUP,
}
