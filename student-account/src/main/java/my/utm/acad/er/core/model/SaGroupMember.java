package my.utm.acad.er.core.model;

public interface SaGroupMember extends SaMetaObject {

    SaPrincipal getPrincipal();

    void setPrincipal(SaPrincipal principal);

    SaGroup getGroup();

    void setGroup(SaGroup group);
}
