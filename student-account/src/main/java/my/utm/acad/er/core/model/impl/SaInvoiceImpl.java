package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public class SaInvoiceImpl implements SaInvoice {

    private Long id;
    private String referenceNo;
    private String sourceNo;
    private String description;
    private boolean paid = false;
    private Date issuedDate;

    private BigDecimal totalAmount = new BigDecimal(0.00);
    private BigDecimal amendedAmount = new BigDecimal(0.00);
    private BigDecimal balanceAmount = new BigDecimal(0.00);

    private SaFacultyCode facultyCode;
    private SaAcademicSession session;
    private SaStudentAccountStatement statement;
    private List<SaInvoiceTransaction> transactions;
    private List<SaInvoiceItem> items;

    private SaMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getReferenceNo() {
        return referenceNo;
    }

    @Override
    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    @Override
    public String getSourceNo() {
        return sourceNo;
    }

    @Override
    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean isPaid() {
        return paid;
    }

    @Override
    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    @Override
    public Date getIssuedDate() {
        return issuedDate;
    }

    @Override
    public void setIssuedDate(Date issuedDate) {
        this.issuedDate = issuedDate;
    }

    @Override
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    @Override
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public BigDecimal getAmendedAmount() {
        return amendedAmount;
    }

    @Override
    public void setAmendedAmount(BigDecimal amendedAmount) {
        this.amendedAmount = amendedAmount;
    }

    @Override
    public BigDecimal getBalanceAmount() {
        return balanceAmount;
    }

    @Override
    public void setBalanceAmount(BigDecimal balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    @Override
    public SaFacultyCode getFacultyCode() {
        return facultyCode;
    }

    @Override
    public void setFacultyCode(SaFacultyCode facultyCode) {
        this.facultyCode = facultyCode;
    }

    @Override
    public SaAcademicSession getSession() {
        return session;
    }

    @Override
    public void setSession(SaAcademicSession session) {
        this.session = session;
    }

    @Override
    public SaStudentAccountStatement getStatement() {
        return statement;
    }

    @Override
    public void setStatement(SaStudentAccountStatement statement) {
        this.statement = statement;
    }

    @Override
    public List<SaInvoiceTransaction> getTransactions() {
        return transactions;
    }

    @Override
    public void setTransactions(List<SaInvoiceTransaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public List<SaInvoiceItem> getItems() {
        return items;
    }

    @Override
    public void setItems(List<SaInvoiceItem> items) {
        this.items = items;
    }

    @Override
    public SaMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SaMetadata metadata) {
        this.metadata = metadata;
    }
}
