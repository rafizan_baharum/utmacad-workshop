package my.utm.acad.er.core.dao.impl;

import my.utm.acad.er.core.dao.GenericDaoSupport;
import my.utm.acad.er.core.dao.SaProgramCodeDao;
import my.utm.acad.er.core.model.SaMetaState;
import my.utm.acad.er.core.model.SaProgramCode;
import my.utm.acad.er.core.model.impl.SaProgramCodeImpl;
import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Repository("saProgramCode")
public class SaProgramCodeDaoImpl extends GenericDaoSupport<Long, SaProgramCode> implements SaProgramCodeDao {

    private static final Logger LOG = LoggerFactory.getLogger(SaProgramCodeDaoImpl.class);

    public SaProgramCodeDaoImpl() {
        super(SaProgramCodeImpl.class);
    }

    @Override
    public SaProgramCode findByCode(String code) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from SaProgramCode s where s.code = :code and  " +
                " s.metadata.state = :state");
        query.setString("code", code);
        query.setCacheable(true);
        query.setInteger("state", SaMetaState.ACTIVE.ordinal());
        return (SaProgramCode) query.uniqueResult();
    }

    @Override
    public List<SaProgramCode> find() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from SaProgramCode s where " +
                " s.metadata.state = :state ");
        query.setInteger("state", SaMetaState.ACTIVE.ordinal());
        query.setCacheable(true);
        return (List<SaProgramCode>) query.list();
    }

    @Override
    public List<SaProgramCode> find(String filter, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select s from SaProgramCode s where " +
                "(upper(s.code) like upper(:filter) " +
                "or upper(s.description) like upper(:filter)) " +
                "and s.metadata.state = :state ");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setInteger("state", SaMetaState.ACTIVE.ordinal());
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        query.setCacheable(true);
        return (List<SaProgramCode>) query.list();
    }

    @Override
    public Integer count(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(s) from SaProgramCode s where " +
                "(upper(s.code) like upper(:filter) " +
                "or upper(s.description) like upper(:filter)) " +
                "and s.metadata.state = :state ");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setInteger("state", SaMetaState.ACTIVE.ordinal());
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public boolean isExists(String code) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(*) from SaProgramCode s where " +
                "s.code = :code " +
                "and s.metadata.state = :state ");
        query.setString("code", code);
        query.setInteger("state", SaMetaState.ACTIVE.ordinal());
        return 0 < ((Long) query.uniqueResult()).intValue();
    }
}
