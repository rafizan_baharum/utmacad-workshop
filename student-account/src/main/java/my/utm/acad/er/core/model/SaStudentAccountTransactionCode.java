package my.utm.acad.er.core.model;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public enum SaStudentAccountTransactionCode {
    RECEIPT,
    VOUCHER;
}
