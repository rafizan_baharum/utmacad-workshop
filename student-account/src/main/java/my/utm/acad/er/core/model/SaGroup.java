package my.utm.acad.er.core.model;

import java.util.Set;

public interface SaGroup extends SaPrincipal {

    Set<SaGroupMember> getMembers();

    void setMembers(Set<SaGroupMember> members);
}
