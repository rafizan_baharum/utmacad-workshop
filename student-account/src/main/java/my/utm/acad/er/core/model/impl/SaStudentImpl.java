package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaBankCode;
import my.utm.acad.er.core.model.SaStudent;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class SaStudentImpl implements SaStudent {

    private Long id;
    private String name;
    private String email;
    private String matricNo;
    private String passportNo;
    private String nricNo;
    private String accountNo;

    private SaBankCode bankCode;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getMatricNo() {
        return matricNo;
    }

    @Override
    public void setMatricNo(String matricNo) {
        this.matricNo = matricNo;
    }

    @Override
    public String getPassportNo() {
        return passportNo;
    }

    @Override
    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    @Override
    public String getNricNo() {
        return nricNo;
    }

    @Override
    public void setNricNo(String nricNo) {
        this.nricNo = nricNo;
    }

    @Override
    public String getAccountNo() {
        return accountNo;
    }

    @Override
    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    @Override
    public SaBankCode getBankCode() {
        return bankCode;
    }

    @Override
    public void setBankCode(SaBankCode bankCode) {
        this.bankCode = bankCode;
    }
}