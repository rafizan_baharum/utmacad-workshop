package my.utm.acad.er.core.dao.impl;

import my.utm.acad.er.core.dao.GenericDaoSupport;
import my.utm.acad.er.core.dao.SaGroupDao;
import my.utm.acad.er.core.model.*;
import my.utm.acad.er.core.model.impl.SaGroupImpl;
import my.utm.acad.er.core.model.impl.SaGroupMemberImpl;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static my.utm.acad.er.core.model.SaMetaState.ACTIVE;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
@Repository("saGroupDao")
public class SaGroupDaoImpl extends GenericDaoSupport<Long, SaGroup> implements SaGroupDao {

    private static final Logger LOG = Logger.getLogger(SaGroupDaoImpl.class);

    public SaGroupDaoImpl() {
        super(SaGroupImpl.class);
    }
    // =============================================================================
    // FINDER METHODS
    // =============================================================================

    @Override
    public List<SaGroup> findAll() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select g from SaGroup g order by g.name");
        return (List<SaGroup>) query.list();
    }

    @Override
    public SaGroup findByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select g from SaGroup g where g.name = :name");
        query.setString("name", name);
        return (SaGroup) query.uniqueResult();
    }


    @Override
    public List<SaGroup> findImmediate(SaPrincipal principal) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select gm.group from SaGroupMember gm inner join gm.principal where " +
                "gm.principal = :principal");
        query.setEntity("principal", principal);
        return (List<SaGroup>) query.list();
    }


    @Override
    public List<SaGroup> findImmediate(SaPrincipal principal, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select gm.group from SaGroupMember gm inner join gm.principal where " +
                "gm.principal = :principal");
        query.setEntity("principal", principal);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return (List<SaGroup>) query.list();
    }

//    @Override
//    public Set<SaGroup> findEffectiveAsNative(SaPrincipal principal) {
//        Set<SaGroup> groups = new HashSet<SaGroup>();
//        Session session = sessionFactory.getCurrentSession();
//        SQLQuery query = session.createSQLQuery("WITH RECURSIVE " +
//                "Q AS " +
//                "( SELECT  GROUP_ID, PRINCIPAL_ID " +
//                "    FROM    CNG_GROP_MMBR " +
//                "    WHERE   PRINCIPAL_ID = " + principal.getId() +
//                "    UNION ALL " +
//                "    SELECT  GM.GROUP_ID, GM.PRINCIPAL_ID " +
//                "    FROM    CNG_GROP_MMBR GM " +
//                "    JOIN    Q " +
//                "    ON      GM.PRINCIPAL_ID = Q.GROUP_ID " +
//                ") " +
//                "SELECT  GROUP_ID FROM  Q");
//        query.addScalar("GROUP_ID", LongType.INSTANCE);
//        List<Long> results = (List<Long>) query.list();
//        for (Long result : results) {
//            groups.add(findById(result));
//        }
//        return groups;
//    }

    @Override
    public List<SaGroup> findAvailableGroups(SaUser user) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select p from SaGroup p where " +
                "p not in (select gm.group from SaGroupMember gm where gm.principal = :user) " +
                "and p <> :user " +
                "and p.metadata.state = :state " +
                "order by p.name asc");
        query.setInteger("state", ACTIVE.ordinal());
        query.setEntity("user", user);
        return (List<SaGroup>) query.list();
    }

    @Override
    public List<SaPrincipal> findAvailableMembers(SaGroup group) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select p from SaPrincipal p where " +
                "p not in (select gm.principal from SaGroupMember gm where gm.group = :group) " +
                "and p <> :group " +
                "and p.metadata.state = :state " +
                "order by p.name asc");
        query.setInteger("state", ACTIVE.ordinal());
        query.setEntity("group", group);
        return (List<SaPrincipal>) query.list();
    }

    @Override
    public List<SaPrincipal> findMembers(SaGroup group, SaPrincipalType principalType) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select gm.group from SaGroupMember gm inner join gm.principal where " +
                "gm.group = :group " +
                "and gm.principal.principalType= :principalType " +
                "and gm.principal.metadata.state = :state ");
        query.setEntity("group", group);
        query.setInteger("principalType", principalType.ordinal());
        return (List<SaPrincipal>) query.list();
    }

    @Override
    public List<SaPrincipal> findMembers(SaGroup group) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select gm.principal from SaGroupMember gm where " +
                "gm.group = :group " +
                "and gm.principal.metadata.state = :state " +
                "order by gm.principal.name");
        query.setEntity("group", group);
        query.setInteger("state", ACTIVE.ordinal());
        return (List<SaPrincipal>) query.list();
    }

    @Override
    public List<SaPrincipal> findMembers(SaGroup group, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select gm.principal from SaGroupMember gm where " +
                "gm.group = :group " +
                "and gm.principal.metadata.state = :state " +
                "order by gm.principal.name");
        query.setEntity("group", group);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return (List<SaPrincipal>) query.list();
    }

    @Override
    public List<SaGroup> find(String filter, Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select distinct g from SaGroup g where " +
                "g.name like upper(:filter) ");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return (List<SaGroup>) query.list();
    }

    @Override
    public List<SaGroup> findMemberships(SaPrincipal principal) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select distinct gm.group from SaGroupMember gm where " +
                "gm.principal = :principal ");
        query.setEntity("principal", principal);
        return (List<SaGroup>) query.list();
    }

    @Override
    public Integer count() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(g) from SaGroup g");
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public Integer count(String filter) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(g) from SaGroup g where " +
                "g.name like upper(:filter)");
        query.setString("filter", WILDCARD + filter + WILDCARD);
        return ((Long) query.uniqueResult()).intValue();
    }

    @Override
    public boolean isExists(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(g) from SaGroup g where " +
                "g.name = :name");
        query.setString("name", name);
        return ((Long) query.uniqueResult()).intValue() >= 1;
    }

    @Override
    public boolean hasMembership(SaGroup group, SaPrincipal principal) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(gm) from SaGroupMember gm where " +
                "gm.group = :group " +
                "and gm.principal = :principal");
        query.setEntity("group", group);
        query.setEntity("principal", principal);
        return ((Long) query.uniqueResult()).intValue() >= 1;
    }

// =============================================================================
    // CRUD METHODS
    // =============================================================================

    @Override
    public void addMember(SaGroup group, SaPrincipal member, SaUser user) {
//            throws RecursiveGroupException {
        Validate.notNull(group, "Group should not be null");
        Validate.notNull(member, "Group member should not be null");

//        if (member instanceof SaGroup) {
//            if (checkRecursive(group, (SaGroup) member))
//                throw new RecursiveGroupException("Recursive user group " + group.getName() + " > " + member.getName());
//        }

        Session session = sessionFactory.getCurrentSession();
        SaGroupMember groupMember = new SaGroupMemberImpl();
        groupMember.setGroup(group);
        groupMember.setPrincipal(member);
        session.save(groupMember);
    }

    @Override
    public void addMembers(SaGroup group, List<SaPrincipal> members, SaUser user) {
        // throws RecursiveGroupException {
        List<SaPrincipal> currentMembers = findMembers(group);
        List<SaPrincipal> newMembers = new ArrayList<SaPrincipal>();

        for (SaPrincipal currentMember : currentMembers) {
            if (!newMembers.contains(currentMember)) {
                removeMember(group, currentMember);
            }
        }
        for (SaPrincipal newMember : newMembers) {
            if (!currentMembers.contains(newMember)) {
                addMember(group, newMember, user);
            }
        }
    }

    @Override
    public void removeMember(SaGroup group, SaPrincipal member) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select g from SaGroupMember g where " +
                "g.group = :group " +
                "and g.principal = :principal");
        query.setEntity("group", group);
        query.setEntity("principal", member);
        SaGroupMember groupMember = (SaGroupMember) query.uniqueResult();
        session.delete(groupMember);
    }

//    private boolean checkRecursive(SaGroup parent, SaGroup child) {
//        Set<SaGroup> hierarchicalGroup = findEffectiveAsNative(parent);
//        return !hierarchicalGroup.add(child);
//    }
}
