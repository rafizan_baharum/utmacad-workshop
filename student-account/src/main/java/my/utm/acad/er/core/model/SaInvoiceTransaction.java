package my.utm.acad.er.core.model;

import java.math.BigDecimal;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public interface SaInvoiceTransaction extends SaMetaObject {

    SaAcademicSession getSession();

    void setSession(SaAcademicSession session);

    SaItemCode getItemCode();

    void setItemCode(SaItemCode itemCode);

    BigDecimal getAmount();

    void setAmount(BigDecimal amount);

    SaInvoice getInvoice();

    boolean isDebit();

    boolean isCredit();

}
