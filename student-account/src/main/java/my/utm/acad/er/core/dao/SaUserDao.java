package my.utm.acad.er.core.dao;


import my.utm.acad.er.core.model.SaActor;
import my.utm.acad.er.core.model.SaGroup;
import my.utm.acad.er.core.model.SaUser;

import java.util.List;

public interface SaUserDao extends GenericDao<Long, SaUser> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    SaUser findByUsername(String username);

    SaUser findByActor(SaActor actor);

    List<SaUser> find(String filter, Integer offset, Integer limit);

    List<SaGroup> findGroups(SaUser user);

    Integer count(String filter);

    // ====================================================================================================
    // CRUD
    // ====================================================================================================
    void save(SaUser user);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    boolean isExists(String username);

    boolean hasUser(SaActor actor);
}
