package my.utm.acad.er.core.dao;


import my.utm.acad.er.core.model.SaActor;
import my.utm.acad.er.core.model.SaActorType;

import java.util.List;

public interface SaActorDao extends GenericDao<Long, SaActor> {

    // ====================================================================================================
    // HELPER
    // ====================================================================================================


    SaActor findById(Long id);

    SaActor findByCode(String code);

    SaActor findByIdentityNo(String identityNo);

    List<SaActor> find(Integer offset, Integer limit);

    List<SaActor> find(String filter, Integer offset, Integer limit);

    List<SaActor> find(SaActorType type, Integer offset, Integer limit);

    List<SaActor> find(SaActorType type, String filter, Integer offset, Integer limit);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count();

    Integer count(String filter);

    Integer count(SaActorType type);

}
