package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaMetadata;
import my.utm.acad.er.core.model.SaVoucher;
import my.utm.acad.er.core.model.SaVoucherRecipient;
import my.utm.acad.er.core.model.SaVoucherRecipientItem;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author team utmacad
 * @since 14/2/2015.
 */
public class SaVoucherRecipientImpl implements SaVoucherRecipient {

    private Long id;
    private BigDecimal totalAmount;

    private SaVoucher voucher;
    List<SaVoucherRecipientItem> items;
    private SaMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    @Override
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public SaVoucher getVoucher() {
        return voucher;
    }

    @Override
    public void setVoucher(SaVoucher voucher) {
        this.voucher = voucher;
    }

    @Override
    public List<SaVoucherRecipientItem> getItems() {
        return items;
    }

    @Override
    public void setItems(List<SaVoucherRecipientItem> items) {
        this.items = items;
    }

    @Override
    public SaMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SaMetadata metadata) {
        this.metadata = metadata;
    }
}
