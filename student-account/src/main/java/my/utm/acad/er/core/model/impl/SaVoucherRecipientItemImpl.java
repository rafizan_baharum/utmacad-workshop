package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaItemCode;
import my.utm.acad.er.core.model.SaMetadata;
import my.utm.acad.er.core.model.SaVoucherRecipient;
import my.utm.acad.er.core.model.SaVoucherRecipientItem;

import java.math.BigDecimal;

/**
 * @author team utmacad
 * @since 14/2/2015.
 */
public class SaVoucherRecipientItemImpl implements SaVoucherRecipientItem {

    private Long id;
    private BigDecimal amount;
    private SaItemCode itemCode;
    private SaVoucherRecipient recipient;
    private SaMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public SaVoucherRecipient getRecipient() {
        return recipient;
    }

    public void setRecipient(SaVoucherRecipient recipient) {
        this.recipient = recipient;
    }

    @Override
    public SaItemCode getItemCode() {
        return itemCode;
    }

    @Override
    public void setItemCode(SaItemCode itemCode) {
        this.itemCode = itemCode;
    }

    @Override
    public SaMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SaMetadata metadata) {
        this.metadata = metadata;
    }
}
