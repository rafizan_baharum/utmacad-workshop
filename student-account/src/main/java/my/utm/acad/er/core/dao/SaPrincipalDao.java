package my.utm.acad.er.core.dao;

import my.utm.acad.er.core.model.SaPrincipal;
import my.utm.acad.er.core.model.SaPrincipalType;

import java.util.List;

public interface SaPrincipalDao extends GenericDao<Long, SaPrincipal> {

    // ====================================================================================================
    // FINDER
    // ====================================================================================================

    SaPrincipal findByName(String name);

    List<SaPrincipal> findAllPrincipals();

    List<SaPrincipal> find(String filter);

    List<SaPrincipal> find(String filter, SaPrincipalType type);

    List<SaPrincipal> find(String filter, Integer offset, Integer limit);

    // ====================================================================================================
    // HELPER
    // ====================================================================================================

    Integer count(String filter);
}
