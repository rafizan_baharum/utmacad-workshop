package my.utm.acad.er.biz.chain;

import my.utm.acad.er.core.model.SaDocument;

/**
 * @author team utmacad
 * @since 4/2/2015.
 */
public class ChainContext {

    private SaDocument document;

    public SaDocument getDocument() {
        return document;
    }

    public void setDocument(SaDocument document) {
        this.document = document;
    }
}
