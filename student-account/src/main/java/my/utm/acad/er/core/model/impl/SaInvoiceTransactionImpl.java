package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.*;

import java.math.BigDecimal;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public class SaInvoiceTransactionImpl implements SaInvoiceTransaction {

    private Long id;

    private SaAcademicSession session;
    private SaInvoice invoice;
    private SaItemCode itemCode;
    private BigDecimal amount;
    private SaMetadata metadata;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public SaAcademicSession getSession() {
        return session;
    }

    @Override
    public void setSession(SaAcademicSession session) {
        this.session = session;
    }

    @Override
    public SaInvoice getInvoice() {
        return invoice;
    }

    public void setInvoice(SaInvoice invoice) {
        this.invoice = invoice;
    }

    @Override
    public SaItemCode getItemCode() {
        return itemCode;
    }

    @Override
    public void setItemCode(SaItemCode itemCode) {
        this.itemCode = itemCode;
    }

    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public SaMetadata getMetadata() {
        return metadata;
    }

    @Override
    public void setMetadata(SaMetadata metadata) {
        this.metadata = metadata;
    }

    @Override
    public boolean isDebit() {
        return false;
    }

    @Override
    public boolean isCredit() {
        return false;
    }
}
