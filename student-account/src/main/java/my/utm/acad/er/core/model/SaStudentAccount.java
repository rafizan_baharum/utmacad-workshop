package my.utm.acad.er.core.model;

import java.util.List;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface SaStudentAccount {

    String getCode();

    void setCode(String code);

    SaStudent getStudent();

    void setStudent(SaStudent student);

    List<SaStudentAccountTransaction> getTransactions();

    void setTransactions(List<SaStudentAccountTransaction> transactions);
}
