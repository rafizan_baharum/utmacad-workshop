package my.utm.acad.er.core.model;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public enum SaMetaState {
    INACTIVE, // 0
    ACTIVE,   // 1

}
