package my.utm.acad.er.core.model;

import java.math.BigDecimal;

/**
 * @author team utamacad
 * @since 3/2/2015.
 */
public interface SaReceiptPaymentItem {

    String getDescription();

    void setDescription(String description);

    BigDecimal getAmount();

    void setAmount(BigDecimal amount);

    SaItemCode getItemCode();

    void setItemCode(SaItemCode itemCode);

    SaReceiptPayment getPayment();

    void setPayment(SaReceiptPayment payment);
}

