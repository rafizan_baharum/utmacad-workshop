package my.utm.acad.er.core.dao;

import my.utm.acad.er.core.model.SaMetaObject;
import my.utm.acad.er.core.model.SaMetaState;
import my.utm.acad.er.core.model.SaMetadata;
import my.utm.acad.er.core.model.SaUser;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class GenericDaoSupport<K, I> implements GenericDao<K, I>, InitializingBean {

    private static final Logger LOG = Logger.getLogger(GenericDaoSupport.class);

    public static final String WILDCARD = "%";

//    private final String RECURSIVE_ACL_QUERY1 = "select distinct aoi.object_id_identity as id from ACL_OBJECT_IDENTITY AOI  " +
//            "left join ACL_ENTRY AE on AE.ACL_OBJECT_IDENTITY in (" +
//            "with recursive Q as " +
//            " ( select  PARENT_OBJECT, ID " +
//            "     from    ACL_OBJECT_IDENTITY " +
//            "     where   ID = AOI.ID " +
//            "     union all " +
//            "     select  AOI.PARENT_OBJECT, AOI.ID " +
//            "     from    ACL_OBJECT_IDENTITY AOI " +
//            "     join    Q " +
//            "     on      AOI.ID = Q.PARENT_OBJECT " +
//            " )" +
//            " select  ID from  Q " +
//            ") " +
//            "left join ACL_CLASS AC ON AC.ID = AOI.OBJECT_ID_CLASS " +
//            "left join ACL_SID SID ON SID.ID = AE.SID ";

//    private final String RECURSIVE_ACL_QUERY =
//            "with recursive top_buttom as   (" +
//                    "  select parent_object,id from (" +
//                    "  " +
//                    "        with recursive bottom_up(PARENT_OBJECT,ID) as   (" +
//                    "            select aoi.parent_object,aoi.id from AC_ACL_AOID aoi" +
//                    "            where AOI.class = '%s'" +
//                    "            union all" +
//                    "            select  AOI.PARENT_OBJECT, AOI.ID" +
//                    "            from    AC_ACL_AOID AOI" +
//                    "              inner join  bottom_up on AOI.ID = bottom_up.PARENT_OBJECT" +
//                    "          )  " +
//                    "          select distinct aoi.id,aoi.parent_object,aoi.sid,aoi.class from AC_ACL_AOID AOI" +
//                    "            inner join bottom_up q on aoi.id = q.id " +
//                    "            " +
//                    "            ) a" +
//                    "  where sid in (%s)" +
//                    "  union all" +
//                    "  select  p.PARENT_OBJECT, p.ID" +
//                    "  from    (" +
//                    "        with recursive bottom_up(PARENT_OBJECT,ID) as   (" +
//                    "            select aoi.parent_object,aoi.id from AC_ACL_AOID aoi" +
//                    "            where AOI.class = '%s'" +
//                    "            union all" +
//                    "            select  AOI.PARENT_OBJECT, AOI.ID" +
//                    "            from    AC_ACL_AOID AOI" +
//                    "              inner join  bottom_up on AOI.ID = bottom_up.PARENT_OBJECT" +
//                    "          )  " +
//                    "          " +
//                    "          select distinct aoi.id,aoi.parent_object,aoi.sid,aoi.class from AC_ACL_AOID AOI" +
//                    "            inner join bottom_up q on aoi.id = q.id" +
//                    "            ) p" +
//                    "            inner join  top_buttom on p.parent_object = top_buttom.id" +
//                    "        )" +
//                    " select b.object_id_identity as id from top_buttom t" +
//                    " inner join AC_ACL_AOID b on t.id = b.id" +
//                    " where b.class = '%s'";

    @Autowired(required = true)
    protected SessionFactory sessionFactory;

    private Class<I> interfaceClass;
    private Class entityClass;

    public GenericDaoSupport(Class entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        interfaceClass = (Class<I>) genericSuperclass.getActualTypeArguments()[1];
    }

    @Override
    public I newInstance() {
        try {
            return (I) entityClass.newInstance();
        } catch (IllegalAccessException e) {
        } catch (InstantiationException e) {
        }
        return null;
    }

    public I refresh(I i) {
        sessionFactory.getCurrentSession().refresh(i);
        return i;
    }

    public I findById(K k) {
        Session session = sessionFactory.getCurrentSession();
        return (I) session.get(entityClass, (Serializable) k);
    }

    /**
     * @return
     * @throws org.springframework.dao.DataAccessException
     */
    public List<I> find() {
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(entityClass).list();
    }

    public List<I> find(Integer offset, Integer limit) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select a "
                + " from " + entityClass.getName() + " a");
        query.setFirstResult(offset);
        query.setMaxResults(limit);
        return (List<I>) query.list();
    }

    @Override
    public Integer count() {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("select count(a) "
                + " from " + entityClass.getName() + " a");
        return ((Long) query.uniqueResult()).intValue();
    }

    /**
     * @param entity
     * @param user
     * @return
     * @throws org.springframework.dao.DataAccessException
     */
    public void save(I entity, SaUser user) {
        Validate.notNull(user, "User cannot be null");
        Validate.notNull(entity, "Object cannot be null");

        try {
            Session session = sessionFactory.getCurrentSession();
            prepareMetadata(entity, user, true);
//            prepareFlowData(entity, user);
            session.save(entity);
        } catch (HibernateException e) {
            LOG.debug("error occurred", e);
        }
    }

    /**
     * @param entity
     * @param user
     * @return
     * @throws org.springframework.dao.DataAccessException
     */
    public void saveOrUpdate(I entity, SaUser user) {
        Validate.notNull(user, "User cannot be null");
        Validate.notNull(entity, "Object cannot be null");

        try {
            Session session = sessionFactory.getCurrentSession();
            prepareMetadata(entity, user, true);
//            prepareFlowData(entity, user);
            session.saveOrUpdate(entity);
        } catch (HibernateException e) {
            LOG.debug("error occurred", e);
        }
    }

    /**
     * @param entity
     * @param user
     * @return
     * @throws org.springframework.dao.DataAccessException
     */
    public void update(I entity, SaUser user) {
        Validate.notNull(user, "User cannot be null");
        Validate.notNull(entity, "Object cannot be null");

        Session session = sessionFactory.getCurrentSession();
        prepareMetadata(entity, user, true);
        session.update(entity);
    }

    /**
     * @param entity
     * @param user
     * @return
     * @throws org.springframework.dao.DataAccessException
     */
    public void deactivate(I entity, SaUser user) {
        Validate.notNull(user, "User cannot be null");
        Validate.notNull(entity, "Object cannot be null");

        // session
        Session session = sessionFactory.getCurrentSession();
        prepareMetadata(entity, user, false);
        session.update(entity);
    }

    /**
     * @param entity
     * @param user
     */
    public void remove(I entity, SaUser user) {
        Validate.notNull(user, "User cannot be null");
        Validate.notNull(entity, "Object cannot be null");

        Session session = sessionFactory.getCurrentSession();
        prepareMetadata(entity, user, false);
        session.update(entity);
    }

    @Override
    public void delete(I entity, SaUser user) {
        Validate.notNull(user, "User cannot be null");
        Validate.notNull(entity, "Object cannot be null");

        Session session = sessionFactory.getCurrentSession();
        session.delete(entity);
    }

    private void prepareMetadata(I i, SaUser user, boolean active) {
        if (i instanceof SaMetaObject) {
            SaMetadata metadata = null;
            if (((SaMetaObject) i).getMetadata() != null)
                metadata = ((SaMetaObject) i).getMetadata();
            else
                metadata = new SaMetadata();
            metadata.setCreatedDate(new Timestamp(System.currentTimeMillis()));
            metadata.setCreatorId(user.getId());
            metadata.setState(active ? SaMetaState.ACTIVE : SaMetaState.INACTIVE);
            ((SaMetaObject) i).setMetadata(metadata);
        }
    }

//    private void prepareFlowData(I i, SaUser user) {
//        if (i instanceof AcFlowObject) {
//            AcFlowdata flowData = null;
//            if (((AcFlowObject) i).getFlowdata() != null)
//                flowData = ((AcFlowObject) i).getFlowdata();
//            else
//                flowData = new AcFlowdata();
//            flowData.setDraftedDate(new Timestamp(System.currentTimeMillis()));
//            flowData.setDrafterId(user.getId());
//            flowData.setState(AcFlowState.DRAFTED);
//            ((AcFlowObject) i).setFlowdata(flowData);
//        }
//    }


//    @Override
//    public List<I> findAuthorized(Set<String> sids) {
//        Validate.notNull(sids, "Sids cannot be null");
//
//        Session session = sessionFactory.getCurrentSession();
//        Query query = session.createQuery("select a " + " from " + entityClass.getName() + " a " +
//                "where a.id in (" + ACL_QUERY + ")");
//        query.setString("clazz", interfaceClass.getCanonicalName());
//        query.setParameterList("name", sids);
//        return (List<I>) query.list();
//    }
//
//    @Override
//    public List<I> findAuthorized(Set<String> sids, Integer offset, Integer limit) {
//        Validate.notNull(sids, "Sids cannot be null");
//        Session session = sessionFactory.getCurrentSession();
//        Query query = session.createQuery("select a " + " from " + entityClass.getName() + " a " +
//                "where a.id in (" + ACL_QUERY + ")");
//        query.setString("clazz", interfaceClass.getCanonicalName());
//        query.setParameterList("name", sids);
//        query.setFirstResult(offset);
//        query.setMaxResults(limit);
//        return (List<I>) query.list();
//    }
//
//    @Override
//    public List<Long> findAuthorizedIds(Set<String> sids) {
//        Validate.notNull(sids, "Sids cannot be null");
//        Session session = sessionFactory.getCurrentSession();
//        String queryString = String.format(RECURSIVE_ACL_QUERY, interfaceClass.getCanonicalName(), sidsToStringArray(sids), interfaceClass.getCanonicalName(), interfaceClass.getCanonicalName());
//        SQLQuery query = session.createSQLQuery(queryString);
//        query.addScalar("ID", HibernateType.INSTANCE);
//        query.setCacheable(true);

//        List<Long> result = new ArrayList<Long>();

//        try {
//            result = (List<Long>) query.list();
//        } catch (GenericJDBCException e) {
//            Throwable cause = e.getCause();
//            if (!(cause instanceof PSQLException && cause.getMessage().equals("No results were returned by the query."))) {
//                e.printStackTrace();
//                throw e;
//            }
//        }
//        return result;
//    }


//    @Override
//    public Integer countAuthorized(Set<String> sids) {
//        Session session = sessionFactory.getCurrentSession();
//        Query query = session.createQuery("select count(a) "
//                + " from " + entityClass.getName() + " a where a.id in (" + ACL_QUERY + ")");
//        query.setString("clazz", interfaceClass.getCanonicalName());
//        query.setParameterList("name", sids);
//        return ((Long) query.uniqueResult()).intValue();
//    }

}