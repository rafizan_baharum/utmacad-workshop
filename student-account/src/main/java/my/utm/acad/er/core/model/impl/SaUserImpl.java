package my.utm.acad.er.core.model.impl;

import my.utm.acad.er.core.model.SaActor;
import my.utm.acad.er.core.model.SaUser;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author team utmSaAd
 * @since 7/2/2015.
 */
@Entity(name = "SaUser")
@Table(name = "SA_USER")
public class SaUserImpl extends SaPrincipalImpl implements SaUser {

    @NotNull
    @Column(name = "REAL_NAME")
    private String realName;

    @NotNull
    @Column(name = "PASSWORD")
    private String password;

    @OneToOne(targetEntity = SaActorImpl.class)
    @JoinColumn(name = "ACTOR_ID")
    private SaActor actor;

    @Override
    public String getRealName() {
        return realName;
    }

    @Override
    public void setRealName(String realName) {
        this.realName = realName;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public SaActor getActor() {
        return actor;
    }

    @Override
    public void setActor(SaActor actor) {
        this.actor = actor;
    }
}
