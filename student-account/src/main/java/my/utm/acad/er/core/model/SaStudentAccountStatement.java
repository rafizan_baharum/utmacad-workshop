package my.utm.acad.er.core.model;

import java.util.List;

/**
 * @author team utmacad
 * @since 9/2/2015.
 */
public interface SaStudentAccountStatement extends SaMetaObject {

    String getReferenceNo();

    void setReferenceNo(String referenceNo);

    String getSourceNo();

    void setSourceNo(String sourceNo);

    String getDescription();

    void setDescription(String description);

    SaStudentAccount getAccount();

    void setAccount(SaStudentAccount account);

    SaAcademicSession getSession();

    void setSession(SaAcademicSession session);

    List<SaInvoice> getInvoices();

    void setInvoices(List<SaInvoice> invoices);
}
