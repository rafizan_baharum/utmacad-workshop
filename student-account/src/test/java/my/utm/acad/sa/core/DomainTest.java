package my.utm.acad.sa.core;

import my.utm.acad.configuration.AbstractDaoContextBaseTest;
import my.utm.acad.sa.core.model.*;
import my.utm.acad.sa.core.model.impl.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author team utamacad
 * @since 4/2/2015.
 */
public class DomainTest extends AbstractDaoContextBaseTest {

    private SaBankCode maybankCode;
    private SaAcademicSession academicSession;
    private SaStudentAccount studentAccount;
    private SaStudent student;
    private SaItemCode hostelCode;
    private SaItemCodeImpl courseCode;
    private SaFacultyCode facultyCode;
    private SaProgramCode programCode;

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void should_Create_AcademicSession() {

    }

    public void setup() {
        academicSession = new SaAcademicSessionImpl();
        academicSession.setCode("201420151");
        academicSession.setDescription("201420151");

        facultyCode = new SaFacultyCodeImpl();
        facultyCode.setCode("FKM");
        facultyCode.setDescription("FKM");

        programCode = new SaProgramCodeImpl();
        programCode.setCode("FKM");
        programCode.setDescription("FKM");

        maybankCode = new SaBankCodeImpl();
        maybankCode.setCode("035");
        maybankCode.setIbgCode("035");
        maybankCode.setSwiftCode("035");
        maybankCode.setDescription("MALAYAN BANKING");

        // items
        hostelCode = new SaItemCodeImpl();
        hostelCode.setCode("SA22001");
        hostelCode.setDescription("CHARGE HOSTEL");

        courseCode = new SaItemCodeImpl();
        courseCode.setCode("SA22002");
        courseCode.setDescription("CHARGE ENROLLMENT");
    }

    public void testAccount() {

        student = new SaStudentImpl();
        student.setName("RAFIZAN BAHARUm");
        student.setEmail("rafizan.baharum@utm.my");
        student.setMatricNo("INT/SEP/2015/BSC/0001");
        student.setNricNo("760607145591");
        student.setPassportNo("");
        student.setAccountNo("1789289181");
        student.setBankCode(maybankCode);

        // set account
        studentAccount = new SaStudentAccountImpl();
        studentAccount.setCode("SCA2015020101");
        studentAccount.setStudent(student);
    }

    public void testStatement() {
        SaStudentAccountStatement statement = new SaStudentAccountStatementImpl();
        statement.setReferenceNo("STMT/201420151/0001");
        statement.setSourceNo("XXX");
        statement.setDescription("Charges");
        statement.setAccount(studentAccount);
        statement.setSession(academicSession);

        SaInvoice invoice = new SaInvoiceImpl();
        invoice.setReferenceNo("INV201401000001");
        invoice.setSourceNo("XXX");
        invoice.setStatement(statement);
        invoice.setSession(academicSession);
        invoice.setFacultyCode(facultyCode);
        invoice.setBalanceAmount(BigDecimal.ZERO);
        invoice.setAmendedAmount(BigDecimal.ZERO);
        invoice.setTotalAmount(BigDecimal.ZERO);
        invoice.setIssuedDate(new Date());
        invoice.setPaid(false);

        SaInvoiceItem item1 = new SaInvoiceItemImpl();
        item1.setDescription("Payment for Course");
        item1.setAmount(BigDecimal.ZERO);
        item1.setUnit(BigDecimal.ONE);
        item1.setPrice(BigDecimal.ZERO);

        SaInvoiceTransaction transaction = new SaInvoiceTransactionImpl();
        transaction.setSession(academicSession);
        transaction.setItemCode(courseCode);
        transaction.setAmount(BigDecimal.ZERO);
    }

    public void testPosting() {
        SaStudentAccountTransaction transaction = new SaStudentAccountTransactionImpl();
        transaction.setAmount(BigDecimal.valueOf(200.00));
        transaction.setSourceNo("RCPT20150100001");
        transaction.setTransactionCode(SaStudentAccountTransactionCode.RECEIPT);
        transaction.setAccount(studentAccount);
        transaction.setSession(academicSession);
    }


    public void testReceipt() {
        // receipt
        SaReceipt receipt = new SaReceiptImpl();
        receipt.setReferenceNo("RCPT20150100001");
        receipt.setSourceNo("2012010/10001/1000");
        receipt.setDescription("STUDENT PAYMENT");
        receipt.setTotalAmount(BigDecimal.valueOf(200.00));

        // payments per account and session
        List<SaReceiptPaymentItem> items = new ArrayList<SaReceiptPaymentItem>();
        SaReceiptPayment payment = new SaReceiptPaymentImpl();
        payment.setPaymentType(SaReceiptPaymentType.CASH);
        payment.setAmount(BigDecimal.valueOf(200.00));
        payment.setAccount(studentAccount);
        payment.setSession(academicSession);
        payment.setReceipt(receipt);

        SaReceiptPaymentItem hostelItem = new SaReceiptPaymentItemImpl();
        hostelItem.setDescription("HOSTEL");
        hostelItem.setAmount(BigDecimal.valueOf(200.00));
        hostelItem.setItemCode(hostelCode);
        hostelItem.setPayment(payment);
        items.add(hostelItem);

        SaReceiptPaymentItem courseItem = new SaReceiptPaymentItemImpl();
        courseItem.setDescription("PAYMENT FOR SCK201");
        courseItem.setAmount(BigDecimal.valueOf(200.00));
        courseItem.setItemCode(hostelCode);
        courseItem.setPayment(payment);
        items.add(courseItem);
    }
}