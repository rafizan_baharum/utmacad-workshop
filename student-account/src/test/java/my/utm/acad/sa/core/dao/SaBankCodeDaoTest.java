package my.utm.acad.sa.core.dao.impl;

import my.utm.acad.configuration.AbstractDaoContextBaseTest;
import my.utm.acad.sa.core.dao.SaBankCodeDao;
import my.utm.acad.sa.core.dao.SaUserDao;
import my.utm.acad.sa.core.model.SaBankCode;
import my.utm.acad.sa.core.model.SaPrincipalType;
import my.utm.acad.sa.core.model.SaUser;
import my.utm.acad.sa.core.model.impl.SaBankCodeImpl;
import my.utm.acad.sa.core.model.impl.SaUserImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;

/**
 * @author team utmacad
 * @since 7/2/2015.
 */
public class SaBankCodeDaoTest extends AbstractDaoContextBaseTest {

    private static final Logger LOG = LoggerFactory.getLogger(SaBankCodeDaoTest.class);

    @Autowired
    private SaBankCodeDao bankCodeDao;

    @Autowired
    private SaUserDao userDao;
    private SaUser root;

    @Before
    public void setUp() {
        root = new SaUserImpl();
        root.setName("root");
        root.setRealName("ROOT");
        root.setPassword("abc123");
        root.setPrincipalType(SaPrincipalType.USER);
        userDao.save(root);
    }

    @After
    public void tearDown() {
    }

    @Test
    @Rollback(true)
    public void list() {
        SaBankCode bankCode = new SaBankCodeImpl();
        bankCode.setCode("035");
        bankCode.setIbgCode("00235");
        bankCode.setSwiftCode("00235");
        bankCode.setDescription("Maybank Berhad");
        bankCodeDao.save(bankCode, root);

        SaBankCode maybank = bankCodeDao.findByCode("035");
        Assert.assertNotNull(maybank);
        LOG.debug("bank {}", maybank.getDescription());
    }
}
